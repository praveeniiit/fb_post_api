from django.apps import AppConfig


class FbTddAppConfig(AppConfig):
    name = "fb_tdd"

    def ready(self):
        from fb_tdd import signals # pylint: disable=unused-variable
