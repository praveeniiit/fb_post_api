#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
import datetime

from fb_tdd.constants.reactions import ReactionType
from fb_tdd.interactors.storages.storage import (CommentDTO, CommentIdDTO,
                                                 PostCompleteDetailsDTO,
                                                 PostDTO, PostIdDTO,
                                                 PostReactionDTO,
                                                 ReactionMetricDTO,
                                                 ReactionWithUserDTO, ReplyDTO,
                                                 UserDTO)
from fb_tdd.presenters.json_presenter import DataPresenter


def test_get_create_post_response():
    presenter = DataPresenter()
    post_dto = PostIdDTO(post_id=1)
    post_dict = {
        'post_id': 1
    }

    response = presenter.get_create_post_response(post_id_dto=post_dto)
    assert response == post_dict


def test_get_add_comment_response():
    presenter = DataPresenter()
    comment_dto = CommentIdDTO(comment_id=1)
    comment_dict = {
        'comment_id': 1
    }

    response = presenter.get_add_comment_response(comment_id_dto=comment_dto)
    assert response == comment_dict


def test_get_reply_to_comment_response():
    presenter = DataPresenter()

    comment_dto = CommentIdDTO(comment_id=1)
    reply_dict = {
        'reply_comment_id': 1
    }

    response = presenter.get_reply_to_comment_response(comment_dto)
    assert response == reply_dict


def test_get_total_reactions_count_response():
    presenter = DataPresenter()

    reaction_count = 1
    reaction_count_dict = dict(reactions_count=reaction_count)

    response = presenter.get_total_reactions_count_response(reaction_count)
    assert reaction_count_dict == response


def test_get_replies_for_comment_response():
    presenter = DataPresenter()

    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url='None')
    comment_dto = CommentDTO(
        comment_id=2, comment_content='Reply for the comment.',
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988)
    )
    reply_dto = ReplyDTO(
        commenter=user_dto, comment_id=2,
        comment_description='Reply for the comment.',
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988))
    reply_dto_dict = [
        {
            'comment_id': 2,
            'commented_at': '09/03/2019, 17:00:30',
            'comment_description': 'Reply for the comment.',
            'commenter': {
                'user_id': 1, 'name': 'Praveen', 'profile_pic': 'None'
            }
        }
    ]

    response = presenter.get_replies_for_comment_response([reply_dto])
    assert response == reply_dto_dict


def test_get_user_reacted_posts_response():
    presenter = DataPresenter()

    post1_dto = PostIdDTO(post_id=1)
    post2_dto = PostIdDTO(post_id=2)

    reacted_posts_dto = [post1_dto, post2_dto]
    reacted_posts_dict = [
        {
            'post_id': 1
        },
        {
            'post_id': 2
        }
    ]

    response = presenter.get_user_reacted_posts_response(
        reacted_posts_dto=reacted_posts_dto)
    assert response == reacted_posts_dict


def test_get_positive_posts_response():
    presenter = DataPresenter()

    positive_post_dto = [
        PostIdDTO(post_id=4),
        PostIdDTO(post_id=3)
    ]
    positive_post_dict = [dict(post_id=4), dict(post_id=3)]

    response = presenter.get_positive_posts_response(
        post_id_dtos=positive_post_dto
    )

    assert positive_post_dict == response


def test_get_reaction_metrics_response():
    presenter = DataPresenter()
    reaction_dto_list = [
        ReactionMetricDTO(reaction_type=ReactionType.LIKE.value, count=23),
        ReactionMetricDTO(reaction_type=ReactionType.WOW.value, count=2)
    ]
    reaction_dict = [
        {
            'reaction_type': reaction.reaction_type,
            'count': reaction.count
        } for reaction in reaction_dto_list
    ]

    response = presenter.get_reaction_metrics_response(
        reaction_metrics=reaction_dto_list)
    assert response == reaction_dict


def test_get_post_reactions_response():
    presenter = DataPresenter()

    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url='None')
    reaction_dto = PostReactionDTO(
        user_dto=user_dto, reaction_type=ReactionType.LIKE.value)
    reaction_dto_dict = [
        {
            'user_id': user_dto.user_id,
            'name': user_dto.name,
            'profile_pic': user_dto.profile_pic_url,
            'reaction': reaction_dto.reaction_type
        }
    ]

    response = presenter.get_post_reactions_response(
        post_reactions_dto=[reaction_dto])
    assert response == reaction_dto_dict


def test_get_post_with_user():
    presenter = DataPresenter()

    post_dto = PostDTO(
        post_id=1, post_content='This is the first post from.',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url=None)
    post_complete_details_dto = PostCompleteDetailsDTO(
        post=[post_dto], users=[user_dto], reactions=[], comments=[]
    )
    get_post_dict = {
        'post_id': 1, 'post_content': 'This is the first post from.',
        'posted_at': '09/03/2019, 17:00:30',
        'posted_by': {
            'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
        },
        'reactions': {
            'count': 0, 'type': []
        },
        'comments': []
    }

    get_post_response = presenter.get_post_response(
        get_post_dto=post_complete_details_dto)

    assert get_post_dict == get_post_response


def test_get_post_with_user_and_reactions():
    presenter = DataPresenter()

    post_dto = PostDTO(
        post_id=1, post_content='This is the first post from.',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url=None)
    reaction_dto = ReactionWithUserDTO(reaction_type="LIKE", user_id=1,
                                       post_id=1)
    reaction_dto2 = ReactionWithUserDTO(reaction_type="LOVE", user_id=1,
                                        post_id=1)
    post_complete_details_dto = PostCompleteDetailsDTO(
        post=[post_dto], users=[user_dto],
        reactions=[reaction_dto, reaction_dto2], comments=[]
    )
    get_post_dict = {
        'post_id': 1, 'post_content': 'This is the first post from.',
        'posted_at': '09/03/2019, 17:00:30',
        'reactions': {
            'type': ['LIKE', 'LOVE'],
            'count': 2
        },
        'posted_by': {
            'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
        },
        'comments': []
    }

    get_post_response = presenter.get_post_response(
        get_post_dto=post_complete_details_dto)

    assert get_post_dict == get_post_response


def test_get_post_with_user_and_reactions_and_comments():
    presenter = DataPresenter()

    post_dto = PostDTO(
        post_id=1, post_content='This is the first post from.',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url=None)
    reaction_dto = ReactionWithUserDTO(reaction_type="LIKE", user_id=1,
                                       post_id=1)
    reaction_dto2 = ReactionWithUserDTO(reaction_type="LOVE", user_id=1,
                                        post_id=1)
    comment_dto = CommentDTO(
        comment_id=1, comment_content="Comment from",
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988),
        post_id=1, user_id=1)
    post_complete_details_dto = PostCompleteDetailsDTO(
        post=[post_dto], users=[user_dto],
        reactions=[reaction_dto, reaction_dto2], comments=[comment_dto]
    )
    get_post_dict = {
        'post_id': 1, 'post_content': 'This is the first post from.',
        'posted_at': '09/03/2019, 17:00:30',
        'reactions': {
            'type': ['LIKE', 'LOVE'],
            'count': 2
        },
        'posted_by': {
            'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
        },
        'comments': [
            {
                'comment_content': 'Comment from',
                'commented_at': '09/03/2019, 17:00:30',
                'commenter':
                    {'name': 'Praveen', 'user_id': 1, 'profile_pic_url': None},
                'reactions': {
                    'count': 0,
                    'type': []
                },
                'replies': []
            }
        ]
    }

    get_post_response = presenter.get_post_response(
        get_post_dto=post_complete_details_dto)

    assert get_post_dict == get_post_response


def test_get_post_with_user_and_reactions_and_comments_and_reactions():
    presenter = DataPresenter()

    post_dto = PostDTO(
        post_id=1, post_content='This is the first post from.',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url=None)
    reaction_dto = ReactionWithUserDTO(reaction_type="LIKE", user_id=1,
                                       post_id=1)
    reaction_dto2 = ReactionWithUserDTO(reaction_type="LOVE", user_id=1,
                                        post_id=1)
    comment_dto = CommentDTO(
        comment_id=1, comment_content="Comment from",
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988),
        post_id=1, user_id=1)
    comment_reaction = ReactionWithUserDTO(reaction_type="LOVE", user_id=1,
                                           comment_id=1)
    post_complete_details_dto = PostCompleteDetailsDTO(
        post=[post_dto], users=[user_dto],
        reactions=[reaction_dto, reaction_dto2, comment_reaction],
        comments=[comment_dto]
    )
    get_post_dict = {
        'post_id': 1, 'post_content': 'This is the first post from.',
        'posted_at': '09/03/2019, 17:00:30',
        'reactions': {
            'type': ['LIKE', 'LOVE'],
            'count': 2
        },
        'posted_by': {
            'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
        },
        'comments': [
            {
                'comment_content': 'Comment from',
                'commented_at': '09/03/2019, 17:00:30',
                'commenter': {'name': 'Praveen', 'user_id': 1,
                              'profile_pic_url': None},
                'reactions': {'type': ['LOVE'], 'count': 1},
                'replies': []
            }
        ]
    }

    get_post_response = presenter.get_post_response(
        get_post_dto=post_complete_details_dto)

    assert get_post_dict == get_post_response


def test_get_post_with_user_and_reactions_and_comments_and_reactions_replies():
    presenter = DataPresenter()

    post_dto = PostDTO(
        post_id=1, post_content='This is the first post from.',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url=None)
    reaction_dto = ReactionWithUserDTO(reaction_type="LIKE", user_id=1,
                                       post_id=1)
    reaction_dto2 = ReactionWithUserDTO(reaction_type="LOVE", user_id=1,
                                        post_id=1)
    comment_dto = CommentDTO(
        comment_id=1, comment_content="Comment from",
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988),
        post_id=1, user_id=1)
    comment_reaction = ReactionWithUserDTO(reaction_type="LOVE", user_id=1,
                                           comment_id=1)
    reply_dto = CommentDTO(
        comment_id=2, comment_content='Reply to comment',
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988),
        parent_id=1, user_id=1)
    reply_reaction = ReactionWithUserDTO(reaction_type="WOW", user_id=1,
                                         comment_id=2)
    post_complete_details_dto = PostCompleteDetailsDTO(
        post=[post_dto], users=[user_dto],
        reactions=[reaction_dto, reaction_dto2, comment_reaction,
                   reply_reaction],
        comments=[comment_dto, reply_dto]
    )
    get_post_dict = {
        'post_id': 1, 'post_content': 'This is the first post from.',
        'posted_at': '09/03/2019, 17:00:30',
        'reactions': {
            'type': ['LIKE', 'LOVE'],
            'count': 2
        },
        'posted_by': {
            'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
        },
        'comments': [
            {
                'comment_content': 'Comment from',
                'commented_at': '09/03/2019, 17:00:30',
                'commenter': {'name': 'Praveen', 'user_id': 1,
                              'profile_pic_url': None},
                'replies': [
                    {
                        'comment_content': 'Reply to comment',
                        'commented_at': '09/03/2019, 17:00:30',
                        'commenter': {
                            'name': 'Praveen', 'user_id': 1,
                            'profile_pic_url': None
                        },
                        'reactions': {'type': ['WOW'], 'count': 1}
                    }
                ],
                'reactions': {'type': ['LOVE'], 'count': 1}
            }
        ]
    }

    get_post_response = presenter.get_post_response(
        get_post_dto=post_complete_details_dto)

    assert get_post_dict == get_post_response


def test_get_user_posts_with_user():
    presenter = DataPresenter()

    post_dto = PostDTO(
        post_id=1, post_content='This is the first post from.',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    second_post_dto = PostDTO(
        post_id=2, post_content='This is the Second post from Praveen',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url=None)
    post_complete_details_dto = PostCompleteDetailsDTO(
        post=[post_dto, second_post_dto], users=[user_dto], reactions=[],
        comments=[]
    )
    user_posts = [
        {
            'post_id': 1, 'post_content': 'This is the first post from.',
            'posted_at': '09/03/2019, 17:00:30',
            'posted_by': {
                'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
            },
            'reactions': {
                'type': [],
                'count': 0
            },
            'comments': []
        },
        {
            'post_id': 2, 'posted_at': '09/03/2019, 17:00:30',
            'post_content': 'This is the Second post from Praveen',
            'posted_by': {
                'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
            },
            'reactions': {
                'type': [],
                'count': 0
            },
            'comments': []
        }
    ]

    user_posts_response = presenter.get_user_posts_response(
        user_posts_dto=post_complete_details_dto
    )

    assert user_posts == user_posts_response


def test_get_user_posts_with_user_and_reactions():
    presenter = DataPresenter()

    post_dto = PostDTO(
        post_id=1, post_content='This is the first post from.',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    second_post_dto = PostDTO(
        post_id=2, post_content='This is the Second post from Praveen',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    reaction_dto = ReactionWithUserDTO(
        reaction_type="LIKE", user_id=1, post_id=1)
    reaction_dto2 = ReactionWithUserDTO(
        reaction_type="LOVE", user_id=1, post_id=1)
    reaction_dto3 = ReactionWithUserDTO(
        reaction_type="LIKE", user_id=1, post_id=2)

    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url=None)
    post_complete_details_dto = PostCompleteDetailsDTO(
        post=[post_dto, second_post_dto], users=[user_dto],
        reactions=[reaction_dto, reaction_dto2, reaction_dto3],
        comments=[]
    )
    user_posts = [
        {
            'post_id': 1, 'post_content': 'This is the first post from.',
            'posted_at': '09/03/2019, 17:00:30',
            'reactions': {
                'type': ['LIKE', 'LOVE'],
                'count': 2
            },
            'posted_by': {
                'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
            },
            'comments': []
        },
        {
            'post_id': 2, 'posted_at': '09/03/2019, 17:00:30',
            'post_content': 'This is the Second post from Praveen',
            'reactions': {
                'type': ['LIKE'],
                'count': 1
            },
            'posted_by': {
                'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
            },
            'comments': []
        }
    ]

    user_posts_response = presenter.get_user_posts_response(
        user_posts_dto=post_complete_details_dto
    )

    assert user_posts == user_posts_response


def test_get_user_posts_with_user_and_reactions_comments():
    presenter = DataPresenter()

    post_dto = PostDTO(
        post_id=1, post_content='This is the first post from.',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    second_post_dto = PostDTO(
        post_id=2, post_content='This is the Second post from Praveen',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    reaction_dto = ReactionWithUserDTO(
        reaction_type="LIKE", user_id=1, post_id=1)
    reaction_dto2 = ReactionWithUserDTO(
        reaction_type="LOVE", user_id=1, post_id=1)
    reaction_dto3 = ReactionWithUserDTO(
        reaction_type="LIKE", user_id=1, post_id=2)

    comment_dto = CommentDTO(
        comment_id=1, comment_content="Comment post 1",
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988),
        post_id=1, user_id=1)
    comment_dto2 = CommentDTO(
        comment_id=2, comment_content="Comment for posts 2",
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988),
        post_id=2, user_id=1)

    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url=None)
    post_complete_details_dto = PostCompleteDetailsDTO(
        post=[post_dto, second_post_dto], users=[user_dto],
        reactions=[reaction_dto, reaction_dto2, reaction_dto3],
        comments=[comment_dto, comment_dto2]
    )
    user_posts = [
        {
            'post_id': 1, 'post_content': 'This is the first post from.',
            'posted_at': '09/03/2019, 17:00:30',
            'reactions': {
                'type': ['LIKE', 'LOVE'],
                'count': 2
            },
            'posted_by': {
                'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
            },
            'comments': [
                {
                    'comment_content': 'Comment post 1',
                    'commented_at': '09/03/2019, 17:00:30',
                    'commenter':
                        {'name': 'Praveen', 'user_id': 1,
                         'profile_pic_url': None},
                    'reactions': {
                        'type': [],
                        'count': 0
                    },
                    'replies': []
                }
            ]
        },
        {
            'post_id': 2, 'posted_at': '09/03/2019, 17:00:30',
            'post_content': 'This is the Second post from Praveen',
            'reactions': {
                'type': ['LIKE'],
                'count': 1
            },
            'posted_by': {
                'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
            },
            'comments': [
                {
                    'comment_content': 'Comment for posts 2',
                    'commented_at': '09/03/2019, 17:00:30',
                    'commenter':
                        {'name': 'Praveen', 'user_id': 1,
                         'profile_pic_url': None},
                    'reactions': {
                        'type': [],
                        'count': 0
                    },
                    'replies': []
                }
            ]
        }
    ]

    user_posts_response = presenter.get_user_posts_response(
        user_posts_dto=post_complete_details_dto
    )

    assert user_posts == user_posts_response


def test_get_user_posts_with_user_and_reactions_comments_and_reactions():
    presenter = DataPresenter()

    post_dto = PostDTO(
        post_id=1, post_content='This is the first post from.',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    second_post_dto = PostDTO(
        post_id=2, post_content='This is the Second post from Praveen',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    reaction_dto = ReactionWithUserDTO(
        reaction_type=ReactionType.LIKE.value, user_id=1, post_id=1)
    reaction_dto2 = ReactionWithUserDTO(
        reaction_type=ReactionType.LOVE.value, user_id=1, post_id=1)
    reaction_dto3 = ReactionWithUserDTO(
        reaction_type=ReactionType.LIKE.value, user_id=1, post_id=2)
    reaction_comment = ReactionWithUserDTO(
        reaction_type=ReactionType.LIKE.value, user_id=1, comment_id=1
    )

    comment_dto = CommentDTO(
        comment_id=1, comment_content="Comment post 1",
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988),
        post_id=1, user_id=1)
    comment_dto2 = CommentDTO(
        comment_id=2, comment_content="Comment for posts 2",
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988),
        post_id=2, user_id=1)

    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url=None)
    post_complete_details_dto = PostCompleteDetailsDTO(
        post=[post_dto, second_post_dto], users=[user_dto],
        reactions=[reaction_dto, reaction_dto2, reaction_dto3,
                   reaction_comment],
        comments=[comment_dto, comment_dto2]
    )
    user_posts = [
        {
            'post_id': 1, 'post_content': 'This is the first post from.',
            'posted_at': '09/03/2019, 17:00:30',
            'reactions': {
                'type': ['LIKE', 'LOVE'],
                'count': 2
            },
            'posted_by': {
                'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
            },
            'comments': [
                {
                    'comment_content': 'Comment post 1',
                    'commented_at': '09/03/2019, 17:00:30',
                    'commenter':
                        {'name': 'Praveen', 'user_id': 1,
                         'profile_pic_url': None},
                    'reactions': {
                        'type': ['LIKE'],
                        'count': 1
                    },
                    'replies': []
                }
            ]
        },
        {
            'post_id': 2, 'posted_at': '09/03/2019, 17:00:30',
            'post_content': 'This is the Second post from Praveen',
            'reactions': {
                'type': ['LIKE'],
                'count': 1
            },
            'posted_by': {
                'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
            },
            'comments': [
                {
                    'comment_content': 'Comment for posts 2',
                    'commented_at': '09/03/2019, 17:00:30',
                    'commenter':
                        {'name': 'Praveen', 'user_id': 1,
                         'profile_pic_url': None},
                    'reactions': {
                        'type': [],
                        'count': 0
                    },
                    'replies': []
                }
            ]
        }
    ]

    user_posts_response = presenter.get_user_posts_response(
        user_posts_dto=post_complete_details_dto
    )

    assert user_posts == user_posts_response


def test_get_user_posts_with_user_and_reactions_comments_reactions_replies():
    presenter = DataPresenter()

    post_dto = PostDTO(
        post_id=1, post_content='This is the first post from.',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    second_post_dto = PostDTO(
        post_id=2, post_content='This is the Second post from Praveen',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    reaction_dto = ReactionWithUserDTO(
        reaction_type=ReactionType.LIKE.value, user_id=1, post_id=1)
    reaction_dto2 = ReactionWithUserDTO(
        reaction_type=ReactionType.LOVE.value, user_id=1, post_id=1)
    reaction_dto3 = ReactionWithUserDTO(
        reaction_type=ReactionType.LIKE.value, user_id=1, post_id=2)
    reaction_comment = ReactionWithUserDTO(
        reaction_type=ReactionType.LIKE.value, user_id=1, comment_id=1
    )

    comment_dto = CommentDTO(
        comment_id=1, comment_content="Comment post 1",
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988),
        post_id=1, user_id=1)
    comment_dto2 = CommentDTO(
        comment_id=2, comment_content="Comment for posts 2",
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988),
        post_id=2, user_id=1)

    reply_dto = CommentDTO(
        comment_id=3, comment_content='Reply to comment',
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988),
        parent_id=1, user_id=1)
    reply_reaction = ReactionWithUserDTO(reaction_type=ReactionType.WOW.value,
                                         user_id=1, comment_id=3)

    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url=None)
    post_complete_details_dto = PostCompleteDetailsDTO(
        post=[post_dto, second_post_dto], users=[user_dto],
        reactions=[reaction_dto, reaction_dto2, reaction_dto3,
                   reaction_comment, reply_reaction],
        comments=[comment_dto, comment_dto2, reply_dto]
    )
    user_posts = [
        {
            'post_id': 1, 'post_content': 'This is the first post from.',
            'posted_at': '09/03/2019, 17:00:30',
            'reactions': {
                'type': ['LIKE', 'LOVE'],
                'count': 2
            },
            'posted_by': {
                'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
            },
            'comments': [
                {
                    'comment_content': 'Comment post 1',
                    'commented_at': '09/03/2019, 17:00:30',
                    'commenter':
                        {'name': 'Praveen', 'user_id': 1,
                         'profile_pic_url': None},
                    'reactions': {
                        'type': ['LIKE'],
                        'count': 1
                    },
                    'replies': [
                        {
                            'comment_content': 'Reply to comment',
                            'commented_at': '09/03/2019, 17:00:30',
                            'commenter': {
                                'name': 'Praveen', 'user_id': 1,
                                'profile_pic_url': None
                            },
                            'reactions': {'type': ['WOW'], 'count': 1}
                        }
                    ]
                }
            ]
        },
        {
            'post_id': 2, 'posted_at': '09/03/2019, 17:00:30',
            'post_content': 'This is the Second post from Praveen',
            'reactions': {
                'type': ['LIKE'],
                'count': 1
            },
            'posted_by': {
                'user_id': 1, 'name': 'Praveen', 'profile_pic_url': None
            },
            'comments': [
                {
                    'comment_content': 'Comment for posts 2',
                    'commented_at': '09/03/2019, 17:00:30',
                    'commenter':
                        {'name': 'Praveen', 'user_id': 1,
                         'profile_pic_url': None},
                    'reactions': {
                        'type': [],
                        'count': 0
                    },
                    'replies': []
                }
            ]
        }
    ]

    user_posts_response = presenter.get_user_posts_response(
        user_posts_dto=post_complete_details_dto
    )

    assert user_posts == user_posts_response
