#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from unittest.mock import create_autospec

from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.reply_to_comment_interactor import \
    ReplyToCommentInteractor
from fb_tdd.interactors.storages.storage import CommentIdDTO, Storage
from fb_tdd.presenters.json_presenter import DataPresenter

NEW_COMMENT = {
    'user_id': 1, 'comment_id': 2,
    'comment_content': 'Reply to the Comment 1'
}


def test_reply_to_comment_on_valid_input_get_response_reply_id():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    comment_id = 2
    comment_id_dto = CommentIdDTO(comment_id=comment_id)
    comment_id_dict = {
        'comment_id': comment_id
    }

    storage_mock.is_comment_exists.return_value = True
    storage_mock.get_comment_parent_id.return_value = None
    storage_mock.reply_to_comment.return_value = comment_id_dto
    presenter_mock.get_reply_to_comment_response.return_value = comment_id_dict

    reply_to_comment_interactor = ReplyToCommentInteractor(
        storage_mock, presenter_mock
    )
    result = reply_to_comment_interactor.reply_to_comment(
        user_id=NEW_COMMENT['user_id'], comment_id=NEW_COMMENT['comment_id'],
        description=NEW_COMMENT['comment_content']
    )

    storage_mock.reply_to_comment.assert_called_once_with(
        user_id=NEW_COMMENT['user_id'], comment_id=NEW_COMMENT['comment_id'],
        description=NEW_COMMENT['comment_content']
    )
    assert result['comment_id'] == comment_id


def test_reply_to_comment_on_valid_input_get_response_reply_id_triangulation():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    comment_id = 3
    comment_id_dto = CommentIdDTO(comment_id=comment_id)
    comment_id_dict = {
        'comment_id': comment_id
    }

    storage_mock.is_comment_exists.return_value = True
    storage_mock.get_comment_parent_id.return_value = None
    storage_mock.reply_to_comment.return_value = comment_id_dto
    presenter_mock.get_reply_to_comment_response.return_value = comment_id_dict

    reply_to_comment_interactor = ReplyToCommentInteractor(
        storage_mock, presenter_mock)
    result = reply_to_comment_interactor.reply_to_comment(
        user_id=NEW_COMMENT['user_id'], comment_id=NEW_COMMENT['comment_id'],
        description=NEW_COMMENT['comment_content']
    )

    assert result['comment_id'] == comment_id
    storage_mock.reply_to_comment.assert_called_once_with(
        user_id=NEW_COMMENT['user_id'], comment_id=NEW_COMMENT['comment_id'],
        description=NEW_COMMENT['comment_content']
    )


def test_reply_to_comment_on_reply_to_reply_get_response_reply_id():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    comment_id = 2
    parent_comment_id = 1
    comment_id_dto = CommentIdDTO(comment_id=comment_id)
    comment_id_dict = {
        'comment_id': comment_id
    }

    storage_mock.is_comment_exists.return_value = True
    storage_mock.get_comment_parent_id.return_value = parent_comment_id
    storage_mock.reply_to_comment.return_value = comment_id_dto
    presenter_mock.get_reply_to_comment_response.return_value = comment_id_dict

    reply_to_comment_interactor = ReplyToCommentInteractor(
        storage_mock, presenter_mock)
    reply_to_comment_interactor.reply_to_comment(
        user_id=NEW_COMMENT['user_id'], comment_id=NEW_COMMENT['comment_id'],
        description=NEW_COMMENT['comment_content']
    )

    storage_mock.get_comment_parent_id.assert_called_once_with(comment_id)
    storage_mock.reply_to_comment.assert_called_once_with(
        user_id=NEW_COMMENT['user_id'], comment_id=parent_comment_id,
        description=NEW_COMMENT['comment_content']
    )
    presenter_mock.get_reply_to_comment_response.assert_called_once_with(
        comment_id_dto
    )


def test_reply_to_comment_invalid_comment_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(DataPresenter)

    storage_mock.is_comment_exists.return_value = False
    reply_to_comment_interactor = ReplyToCommentInteractor(
        storage_mock, presenter_mock)

    reply_to_comment_interactor.reply_to_comment(
        user_id=NEW_COMMENT['user_id'],
        comment_id=NEW_COMMENT['comment_id'],
        description=NEW_COMMENT['comment_content']
    )

    storage_mock.is_comment_exists.assert_called_once()
    presenter_mock.raise_invalid_comment_id_exception.assert_called_once()
