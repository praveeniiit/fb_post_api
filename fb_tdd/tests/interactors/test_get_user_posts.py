#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from unittest.mock import create_autospec

from fb_tdd.constants.get_post_util import get_comments
from fb_tdd.constants.reactions import ReactionType
from fb_tdd.interactors.get_user_posts_interactor import GetUserPostsInteractor
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import (CommentDTO,
                                                 PostCompleteDetailsDTO,
                                                 PostDTO, ReactionWithUserDTO,
                                                 Storage, UserDTO)


def test_get_user_posts_on_invalid_user_id_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    user_id = 1
    offset = 0
    limit = 3
    storage_mock.is_user_exists.return_value = False

    get_user_posts_interactor = GetUserPostsInteractor(
        storage_mock, presenter_mock)
    get_user_posts_interactor.get_user_posts(
        user_id=user_id, offset=offset, limit=limit)

    storage_mock.is_user_exists.assert_called_once_with(user_id=user_id)
    presenter_mock.raise_invalid_user_id_exception.assert_called_once()


def test_get_user_posts_on_valid_input_get_user_posts():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    user_id = 1
    offset = 0
    limit = 3
    POST = {
        'post_id': 1, 'post_content': 'Hello World!',
        'posted_at': '2019-05-21 20:21:46.810366'
    }
    USER = {
        'user_id': 1, 'user_name': 'Praveen', 'profile_pic': None
    }
    COMMENT = {
        'comment_id': 2, 'comment_content': 'Nothing',
        'commented_at': '2019-05-21 20:21:46.810366'
    }

    post_dto = PostDTO(
        post_id=POST['post_id'], post_content=POST['post_content'],
        posted_at=POST['posted_at'])
    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url=None)
    comment_dto = CommentDTO(
        comment_id=COMMENT['comment_id'], commented_at=COMMENT['commented_at'],
        comment_content=COMMENT['comment_content']
    )
    reaction_dto = ReactionWithUserDTO(
        reaction_type=ReactionType.LIKE.value, user_id=USER['user_id'],
        comment_id=COMMENT['comment_id'])

    post_complete_dto = PostCompleteDetailsDTO(
        post=[post_dto], users=[user_dto], comments=[comment_dto],
        reactions=[reaction_dto])
    post_complete_dict = [{
        "post_id": 1,
        "posted_by": {
            "name": "Praveen",
            "user_id": 1,
            "profile_pic_url": None
        },
        "posted_at": "2019-05-21 20:21:46.810366",
        "post_content": "Hello World!",
        "reactions": {
            "count": 0,
            "type": []
        },
        "comments": get_comments(),
        "comments_count": 1,
    }]

    storage_mock.is_user_exists.return_value = True
    storage_mock.get_user_posts.return_value = post_complete_dto
    presenter_mock.get_user_posts_response.return_value = post_complete_dict

    get_user_posts_interactor = GetUserPostsInteractor(
        storage_mock, presenter_mock)
    result = get_user_posts_interactor.get_user_posts(
        user_id=user_id, offset=offset, limit=limit)

    assert result == post_complete_dict
    storage_mock.is_user_exists.assert_called_once_with(user_id=user_id)
    storage_mock.get_user_posts.assert_called_once_with(
        user_id=user_id, offset=offset, limit=limit)
    presenter_mock.get_user_posts_response.assert_called_once_with(
        post_complete_dto)
