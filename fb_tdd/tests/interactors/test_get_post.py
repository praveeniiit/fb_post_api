#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from unittest.mock import create_autospec, Mock

import pytest
from django_swagger_utils.drf_server.exceptions import NotFound

from fb_tdd.constants.get_post_util import get_comments
from fb_tdd.interactors.get_post_interactor import GetPostInteractor
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import (
    CommentDTO, PostCompleteDetailsDTO, PostDTO, ReactionDTO, Storage, UserDTO,
    ReactionWithUserDTO)


def test_get_post_on_invalid_post_raise_exception_triangulation():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id = 1

    storage_mock.is_post_exists.return_value = False

    get_post_interactor = GetPostInteractor(storage_mock, presenter_mock)

    with pytest.raises(NotFound) as PostNotFound:
        get_post_interactor.get_post(post_id=post_id)
    storage_mock.is_post_exists.assert_called_once()


def test_get_post_on_valid_input_get_response_introduce_presenter():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id = 1
    post_dto = PostDTO(post_id=2, post_content='Hello World!',
                       posted_at='2019-05-21 20:21:46.810366')
    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url=None)
    comment_dto = CommentDTO(comment_id=2, comment_content="Nothing",
                             commented_at='2019-05-21 20:21:46.810366')
    reaction_dto = ReactionWithUserDTO(reaction_type="LIKE", user_id=1,
                                       comment_id=2)

    post_complete_dto = PostCompleteDetailsDTO(
        post=[post_dto], users=[user_dto], comments=[comment_dto],
        reactions=[reaction_dto])
    post_complete_dto_dict = {
        "post_id": 1,
        "posted_by": {
            "name": "Praveen",
            "user_id": 1,
            "profile_pic_url": None
        },
        "posted_at": "2019-05-21 20:21:46.810366",
        "post_content": "Hello World!",
        "reactions": {
            "count": 0,
            "type": []
        },
        "comments": get_comments(),
        "comments_count": 1,
    }

    storage_mock.is_post_exists.return_value = True
    storage_mock.get_post.return_value = post_complete_dto
    presenter_mock.get_post_response.return_value = post_complete_dto_dict

    get_post_interactor = GetPostInteractor(storage_mock, presenter_mock)
    result = get_post_interactor.get_post(post_id=post_id)

    assert result == post_complete_dto_dict
    storage_mock.is_post_exists.assert_called_once_with(post_id=post_id)
    storage_mock.get_post.assert_called_once_with(post_id=post_id)
    presenter_mock.get_post_response.assert_called_once_with(post_complete_dto)
