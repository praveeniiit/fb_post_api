#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from unittest.mock import create_autospec

import pytest
from django_swagger_utils.drf_server.exceptions import Forbidden, NotFound

from fb_tdd.interactors.delete_post_interactor import DeletePostInteractor
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import Storage


def test_delete_post_raise_error_on_invalid_post_triangulation():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)
    post_id = 1
    user_id = 1

    storage_mock.is_post_exists.return_value = False

    delete_post_interactor = DeletePostInteractor(
        storage_mock, presenter_mock)
    delete_post_interactor.delete_post(user_id=user_id, post_id=post_id)

    storage_mock.is_post_exists.assert_called_once_with(post_id=post_id)
    presenter_mock.raise_invalid_post_id_exception.assert_called_once()


def test_delete_post_raise_error_unauthorized_user_tries_to_delete_post():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)
    post_id = 1
    user_id = 1

    storage_mock.is_post_exists.return_value = True
    storage_mock.is_un_authorized_to_delete_post.return_value = True

    delete_post_interactor = DeletePostInteractor(
        storage_mock, presenter_mock)
    delete_post_interactor.delete_post(user_id=user_id, post_id=post_id)

    storage_mock.is_post_exists.assert_called_once()
    storage_mock.is_un_authorized_to_delete_post.assert_called_once()


def test_delete_post_on_valid_input():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)
    post_id = 1
    user_id = 1

    storage_mock.is_post_exists.return_value = True
    storage_mock.is_un_authorized_to_delete_post.return_value = False

    delete_post_interactor = DeletePostInteractor(
        storage_mock, presenter_mock)
    delete_post_interactor.delete_post(user_id=user_id, post_id=post_id)

    storage_mock.is_un_authorized_to_delete_post.assert_called_once()
    storage_mock.is_post_exists.assert_called_once()
    storage_mock.delete_post.assert_called_once_with(
        post_id=post_id
    )
