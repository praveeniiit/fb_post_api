#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from unittest.mock import create_autospec

from fb_tdd.interactors.get_posts_with_more_positive_reactions_interactor \
    import GetPositivePostsInteractor
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import PostIdDTO, Storage


def test_get_posts_with_more_positive_reactions_get_response():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id_1 = 1
    post_id_3 = 3
    post_id_dtos = [PostIdDTO(post_id=post_id_1), PostIdDTO(post_id=post_id_3)]
    posts_dict = [
        {'post_id': post_id_1},
        {'post_id': post_id_3}
    ]

    storage_mock.get_positive_posts.return_value = post_id_dtos
    presenter_mock.get_positive_posts_response.return_value = posts_dict

    positive_posts_interactor = GetPositivePostsInteractor(
        storage_mock, presenter_mock)
    positive_posts = positive_posts_interactor.get_positive_posts()

    assert positive_posts == posts_dict
    storage_mock.get_positive_posts.assert_called_once()
    presenter_mock.get_positive_posts_response.assert_called_once()
