#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from unittest.mock import create_autospec

from fb_tdd.interactors.get_replies_for_comment_interactor import \
    GetRepliesForCommentInteractor
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import ReplyDTO, Storage, UserDTO


def test_get_replies_for_comment_on_invalid_comment_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    comment_id = 1

    storage_mock.is_comment_exists.return_value = False

    get_replies_for_comment_interactor = GetRepliesForCommentInteractor(
        storage_mock, presenter_mock)

    get_replies_for_comment_interactor.get_comment_replies(comment_id)
    storage_mock.is_comment_exists.assert_called_once()
    presenter_mock.raise_invalid_comment_id_exception.assert_called_once()


def test_get_replies_for_comment_on_valid_input_get_replies():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    comment_id = 1
    reply_id = 2
    user_dto = UserDTO(
        user_id=1, name='Praveen', profile_pic_url=None
    )
    reply_dto = ReplyDTO(
        commenter=user_dto, commented_at='2019-08-29 00:00:01',
        comment_description='New reply for comment.', comment_id=reply_id
    )
    replies_dict = [
        {
            'commenter': {
                'commenter_id': 2,
                'name': 'praveen',
            },
            'comment_id': 2,
            'commented_at': '2019-08-29 00:00:01',
            'comment_description': 'New reply for comment.'
        }
    ]

    storage_mock.is_comment_exists.return_value = True
    storage_mock.get_replies_for_comment.return_value = [reply_dto]
    presenter_mock.get_replies_for_comment_response.return_value = replies_dict

    get_replies_for_comment_interactor = GetRepliesForCommentInteractor(
        storage_mock, presenter_mock)
    result = get_replies_for_comment_interactor.get_comment_replies(comment_id)

    storage_mock.is_comment_exists.assert_called_once()
    storage_mock.get_replies_for_comment.assert_called_once()
    presenter_mock.get_replies_for_comment_response.assert_called_once_with(
        replies_dto=[reply_dto]
    )
    assert result == replies_dict
