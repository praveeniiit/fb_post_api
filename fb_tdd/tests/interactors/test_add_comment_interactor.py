#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from unittest.mock import create_autospec

from fb_tdd.interactors.add_comment_interactor import AddCommentInteractor
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import CommentIdDTO, Storage

NEW_COMMENT = {
    'user_id': 1, 'post_id': 1,
    'comment_content': 'Comment to the post 1'
}


def test_add_comment_on_valid_input_get_response_comment_id():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    comment_id = 1
    comment_id_dto = CommentIdDTO(comment_id=comment_id)
    comment_id_dict = {
        'comment_id': comment_id
    }

    storage_mock.add_comment_get_id.return_value = comment_id_dto
    presenter_mock.get_add_comment_response.return_value = comment_id_dict

    add_comment_interactor = AddCommentInteractor(storage_mock, presenter_mock)
    result = add_comment_interactor.add_comment(
        user_id=NEW_COMMENT['user_id'], post_id=NEW_COMMENT['post_id'],
        description=NEW_COMMENT['comment_content']
    )

    assert result['comment_id'] == comment_id


def test_add_comment_on_valid_input_get_response_comment_id_triangulation():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    comment_id = 1
    comment_id_dto = CommentIdDTO(comment_id=comment_id)
    comment_id_dict = {
        'comment_id': comment_id
    }

    storage_mock.add_comment_get_id.return_value = comment_id_dto
    presenter_mock.get_add_comment_response.return_value = comment_id_dict

    add_comment_interactor = AddCommentInteractor(storage_mock, presenter_mock)
    result = add_comment_interactor.add_comment(
        user_id=NEW_COMMENT['user_id'], post_id=NEW_COMMENT['post_id'],
        description=NEW_COMMENT['comment_content']
    )

    assert result['comment_id'] == comment_id


def test_add_comment_on_valid_input_storage_triangulation():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    comment_id = 1
    comment_id_dto = CommentIdDTO(comment_id=comment_id)
    comment_id_dict = {
        'comment_id': comment_id
    }

    storage_mock.add_comment_get_id.return_value = comment_id_dto
    presenter_mock.get_add_comment_response.return_value = comment_id_dict

    add_comment_interactor = AddCommentInteractor(storage_mock, presenter_mock)
    result = add_comment_interactor.add_comment(
        user_id=NEW_COMMENT['user_id'], post_id=NEW_COMMENT['post_id'],
        description=NEW_COMMENT['comment_content']
    )

    assert result['comment_id'] == comment_id


def test_add_comment_on_invalid_post_id_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock.is_post_exists.return_value = False

    add_comment_interactor = AddCommentInteractor(storage_mock, presenter_mock)

    add_comment_interactor.add_comment(
        user_id=NEW_COMMENT['user_id'], post_id=NEW_COMMENT['post_id'],
        description=NEW_COMMENT['comment_content']
    )

    presenter_mock.raise_invalid_post_id_exception.assert_called_once()
