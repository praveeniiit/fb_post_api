#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.

from mock import create_autospec

from fb_tdd.interactors.get_user_reacted_posts_interactor import \
    GetUserReactedPostsInteractor
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import PostDTO, Storage


def test_get_user_reacted_posts_raise_exception_on_invalid_user_triangulation():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    user_id = 1
    offset = 0
    limit = 3

    storage_mock.is_user_exists.return_value = False

    get_user_reacted_posts_interactor = GetUserReactedPostsInteractor(
        storage_mock, presenter_mock)

    get_user_reacted_posts_interactor.get_user_reacted_posts(
        user_id=user_id, offset=offset, limit=limit)

    storage_mock.is_user_exists.assert_called_once_with(user_id=user_id)
    presenter_mock.raise_invalid_user_id_exception.assert_called_once()


def test_get_user_reacted_posts_on_valid_input_introduce_presenters():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    user_id = 1
    offset = 0
    limit = 3
    POST = {
        'post_id': 1, 'content': 'Welcome to the world',
        'posted_at': '2019-09-08 10:13:12'
    }
    post_dto = PostDTO(post_id=POST['post_id'], post_content=POST['content'],
                       posted_at=POST['posted_at'])
    post_dto_dict = [
        {
            'post_id': 1,
            'post_content': 'Welcome to the world',
            'posted_at': '2019-09-08 10:13:12'
        }
    ]

    storage_mock.is_user_exists.return_value = True
    storage_mock.get_user_reacted_posts.return_value = [post_dto]
    presenter_mock.get_user_reacted_posts_response.return_value = post_dto_dict

    get_user_reacted_posts_interactor = GetUserReactedPostsInteractor(
        storage_mock, presenter_mock)
    result = get_user_reacted_posts_interactor.get_user_reacted_posts(
        user_id=user_id, offset=offset, limit=limit)

    assert result == post_dto_dict
    storage_mock.is_user_exists.assert_called_once_with(user_id=user_id)
    storage_mock.get_user_reacted_posts.assert_called_once_with(
        user_id=user_id, offset=offset, limit=limit)
    presenter_mock.get_user_reacted_posts_response.assert_called_once_with(
        [post_dto]
    )
