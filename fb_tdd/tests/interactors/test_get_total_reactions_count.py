#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from unittest.mock import create_autospec

from fb_tdd.interactors.get_total_reactions_count_interactor import \
    GetTotalReactionsInteractor
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import Storage


def test_get_total_reactions_count_returns_reactions_count():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    reactions_count = 10
    reactions_count_dict = {
        'count': reactions_count
    }

    storage_mock.get_total_reactions_count.return_value = reactions_count
    presenter_mock.get_total_reactions_count_response.return_value = \
        reactions_count_dict

    get_total_reactions_count_interactor = GetTotalReactionsInteractor(
        storage_mock, presenter_mock)
    result = get_total_reactions_count_interactor.total_reactions_count()

    assert result['count'] == reactions_count
    storage_mock.get_total_reactions_count.assert_called_once()
    presenter_mock.get_total_reactions_count_response.assert_called_once()
