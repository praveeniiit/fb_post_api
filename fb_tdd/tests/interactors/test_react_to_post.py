#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from unittest.mock import create_autospec

from fb_tdd.constants.reactions import ReactionType
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.react_to_post_interactor import ReactToPostInteractor
from fb_tdd.interactors.storages.storage import ReactionDTO, Storage

NEW_REACTION = {
    'user_id': 1, 'post_id': 1,
    'reaction_type': ReactionType.LIKE.value
}
INVALID_REACTION_TYPE = "RUN"


def test_react_to_post_check_post_existence_triangulation():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)
    post_id = 1

    storage_mock.is_post_exists.return_value = True

    react_to_post_interactor = ReactToPostInteractor(
        storage_mock, presenter_mock)
    react_to_post_interactor.is_post_do_not_exist(post_id=post_id)

    storage_mock.is_post_exists.assert_called_once_with(post_id=post_id)


def test_react_to_post_on_valid_input_create_reaction_returns_nothing():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock.is_post_exists.return_value = True
    storage_mock.get_post_reaction.return_value = False

    react_to_post_interactor = ReactToPostInteractor(
        storage_mock, presenter_mock)
    react_to_post_interactor.react_to_post(
        user_id=NEW_REACTION['user_id'], post_id=NEW_REACTION['post_id'],
        reaction_type=NEW_REACTION['reaction_type']
    )

    storage_mock.react_to_post.assert_called_once_with(
        user_id=NEW_REACTION['user_id'], post_id=NEW_REACTION['post_id'],
        reaction_type=NEW_REACTION['reaction_type']
    )


def test_react_to_post_on_invalid_post_id_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock.is_post_exists.return_value = False

    react_to_post_interactor = ReactToPostInteractor(
        storage_mock, presenter_mock)

    react_to_post_interactor.react_to_post(
        user_id=NEW_REACTION['user_id'], post_id=NEW_REACTION['post_id'],
        reaction_type=NEW_REACTION['reaction_type']
    )

    storage_mock.is_post_exists.assert_called_once()
    presenter_mock.raise_invalid_post_id_exception.assert_called_once()


def test_react_to_post_on_update_action_returns_none():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    reaction_dto = ReactionDTO(
        reaction_type=ReactionType.LOVE.value, user_id=NEW_REACTION['user_id'],
        post_id=NEW_REACTION['post_id'])

    storage_mock.get_post_reaction.return_value = reaction_dto

    react_to_post_interactor = ReactToPostInteractor(
        storage_mock, presenter_mock)
    react_to_post_interactor.react_to_post(
        user_id=NEW_REACTION['user_id'], post_id=NEW_REACTION['post_id'],
        reaction_type=NEW_REACTION['reaction_type']
    )

    storage_mock.update_post_reaction.assert_called_once()


def test_react_to_post_on_same_reaction_perform_undo_action():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    reaction_dto = ReactionDTO(
        reaction_type=ReactionType.LIKE.value, user_id=NEW_REACTION['user_id'],
        post_id=NEW_REACTION['post_id'])

    storage_mock.get_post_reaction.return_value = reaction_dto

    react_to_post_interactor = ReactToPostInteractor(
        storage_mock, presenter_mock)
    react_to_post_interactor.react_to_post(
        user_id=NEW_REACTION['user_id'], post_id=NEW_REACTION['post_id'],
        reaction_type=NEW_REACTION['reaction_type']
    )

    storage_mock.undo_post_reaction.assert_called_once()
    storage_mock.get_post_reaction.assert_called_once()


def test_react_to_post_on_invalid_reaction_type_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    react_to_post_interactor = ReactToPostInteractor(storage_mock,
                                                     presenter_mock)

    react_to_post_interactor.react_to_post(
        user_id=NEW_REACTION['user_id'], post_id=NEW_REACTION['post_id'],
        reaction_type=INVALID_REACTION_TYPE
    )

    presenter_mock.raise_invalid_reaction_exception.assert_called_once()
