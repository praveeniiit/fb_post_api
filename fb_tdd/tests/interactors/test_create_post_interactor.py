#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from unittest.mock import create_autospec

from fb_tdd.interactors.create_post_interactor import CreatePostInteractor
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import PostIdDTO, Storage

NEW_POST = {
    'user_id': 1,
    'post_content': 'This is the post content.'
}


def test_create_post_on_valid_input_create_post_get_response_post_id():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id = 3
    post_id_dto = PostIdDTO(post_id=post_id)
    post_id_dict = {'post_id': post_id}

    storage_mock.create_post_and_get_post_id.return_value = post_id_dto
    presenter_mock.get_create_post_response.return_value = post_id_dict

    create_post_interactor = CreatePostInteractor(storage_mock, presenter_mock)
    result = create_post_interactor.create_post(
        user_id=NEW_POST['user_id'], description=NEW_POST['post_content'])

    assert result['post_id'] == post_id
