#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from unittest.mock import create_autospec

from fb_tdd.constants.reactions import ReactionType
from fb_tdd.interactors.get_reactions_to_post import \
    GetReactionsToPostInteractor
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import (PostReactionDTO, Storage,
                                                 UserDTO)


def test_get_reactions_to_post_on_invalid_post_id_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id = 1
    offset = 0
    limit = 12

    storage_mock.is_post_exists.return_value = False

    get_reactions_to_post_interactor = GetReactionsToPostInteractor(
        storage_mock, presenter_mock
    )
    get_reactions_to_post_interactor.get_reactions_to_post(
        post_id, offset=offset, limit=limit)

    storage_mock.is_post_exists.assert_called_once()
    presenter_mock.raise_invalid_post_id_exception.assert_called_once()


def test_get_reaction_to_post_on_valid_input_returns_response():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id = 1
    offset = 0
    limit = 12
    user_id = 1
    user_name = 'iB Cricket'
    profile_pic = 'None'

    user_dto = UserDTO(user_id=user_id, name=user_name,
                       profile_pic_url=profile_pic)
    post_reaction_dto = PostReactionDTO(user_dto=user_dto,
                                        reaction_type=ReactionType.LIKE.value)
    post_reaction_dto_dict = [
        {
            'user_id': user_id, 'name': user_name, 'profile_pic': profile_pic,
            'reaction_type': ReactionType.LIKE.value
        }
    ]

    storage_mock.is_post_exists.return_value = True
    storage_mock.get_post_reactions.return_value = [post_reaction_dto]
    presenter_mock.get_post_reactions_response.return_value = \
        post_reaction_dto_dict

    get_reactions_to_post_interactor = GetReactionsToPostInteractor(
        storage_mock, presenter_mock
    )
    result = get_reactions_to_post_interactor.get_reactions_to_post(
        post_id=post_id, offset=offset, limit=limit)

    storage_mock.is_post_exists.assert_called_once_with(post_id=post_id)
    storage_mock.get_post_reactions.assert_called_once_with(
        post_id=post_id, offset=offset, limit=limit)
    presenter_mock.get_post_reactions_response.assert_called_once_with(
        [post_reaction_dto]
    )
    assert result == post_reaction_dto_dict
