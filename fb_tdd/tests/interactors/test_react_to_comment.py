#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from unittest.mock import create_autospec

from fb_tdd.constants.reactions import ReactionType
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.react_to_comment_interactor import \
    ReactToCommentInteractor
from fb_tdd.interactors.storages.storage import ReactionDTO, Storage

NEW_REACTION = {
    'user_id': 1, 'comment_id': 1, 'reaction_type': ReactionType.LIKE.value
}

INVALID_REACTION_TYPE = 'JUMP'


def test_react_to_comment_on_valid_input_create_reaction():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock.is_comment_exists.return_value = True
    storage_mock.get_comment_reaction.return_value = False

    react_to_comment_interactor = ReactToCommentInteractor(
        storage_mock, presenter_mock)
    react_to_comment_interactor.react_to_comment(
        user_id=NEW_REACTION['user_id'],
        comment_id=NEW_REACTION['comment_id'],
        reaction_type=NEW_REACTION['reaction_type']
    )

    storage_mock.is_comment_exists.assert_called_once()
    storage_mock.get_comment_reaction.assert_called_once()
    storage_mock.react_to_comment.assert_called_once_with(
        user_id=NEW_REACTION['user_id'],
        comment_id=NEW_REACTION['comment_id'],
        reaction_type=NEW_REACTION['reaction_type']
    )


def test_react_to_comment_on_invalid_comment_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)
    comment_id = 1

    storage_mock.is_comment_exists.return_value = False

    react_to_comment_interactor = ReactToCommentInteractor(
        storage_mock, presenter_mock)

    react_to_comment_interactor.react_to_comment(
        user_id=NEW_REACTION['user_id'],
        comment_id=NEW_REACTION['comment_id'],
        reaction_type=NEW_REACTION['reaction_type']
    )

    storage_mock.is_comment_exists.assert_called_once_with(comment_id)
    presenter_mock.raise_invalid_comment_id_exception.assert_called_once()


def test_react_to_comment_update_reaction_returns_nothing():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)
    comment_id = 1

    reaction_dto = ReactionDTO(
        reaction_type=ReactionType.LOVE.value, user_id=NEW_REACTION['user_id'],
        comment_id=NEW_REACTION['comment_id'])

    storage_mock.get_comment_reaction.return_value = reaction_dto
    storage_mock.is_comment_exists.return_value = True

    react_to_comment_interactor = ReactToCommentInteractor(
        storage_mock, presenter_mock)
    react_to_comment_interactor.react_to_comment(
        user_id=NEW_REACTION['user_id'],
        comment_id=NEW_REACTION['comment_id'],
        reaction_type=NEW_REACTION['reaction_type']
    )

    storage_mock.get_comment_reaction.assert_called_once()
    storage_mock.is_comment_exists.assert_called_once_with(comment_id)
    storage_mock.update_comment_reaction.assert_called_once_with(
        user_id=NEW_REACTION['user_id'],
        comment_id=NEW_REACTION['comment_id'],
        reaction_type=NEW_REACTION['reaction_type']
    )


def test_react_to_comment_undo_reaction_returns_nothing():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)
    comment_id = 1

    reaction_dto = ReactionDTO(
        reaction_type=ReactionType.LIKE.value, user_id=NEW_REACTION['user_id'],
        post_id=NEW_REACTION['comment_id'])

    storage_mock.get_comment_reaction.return_value = reaction_dto
    storage_mock.is_comment_exists.return_value = True
    storage_mock.is_comment_reaction_exists.return_value = True

    react_to_comment_interactor = ReactToCommentInteractor(
        storage_mock, presenter_mock)
    react_to_comment_interactor.react_to_comment(
        user_id=NEW_REACTION['user_id'],
        comment_id=NEW_REACTION['comment_id'],
        reaction_type=NEW_REACTION['reaction_type']
    )

    storage_mock.get_comment_reaction.assert_called_once()
    storage_mock.is_comment_exists.assert_called_once_with(comment_id)
    storage_mock.undo_comment_reaction.assert_called_once_with(
        user_id=NEW_REACTION['user_id'], comment_id=NEW_REACTION['comment_id']
    )


def test_react_to_comment_on_invalid_reaction_type_raise_error():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    react_to_comment_interactor = ReactToCommentInteractor(
        storage_mock, presenter_mock)

    react_to_comment_interactor.react_to_comment(
        user_id=NEW_REACTION['user_id'],
        comment_id=NEW_REACTION['comment_id'],
        reaction_type=INVALID_REACTION_TYPE
    )

    presenter_mock.raise_invalid_reaction_exception.assert_called_once()
