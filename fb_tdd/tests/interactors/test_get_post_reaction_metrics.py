#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from unittest.mock import Mock, create_autospec

from fb_tdd.constants.reactions import ReactionType
from fb_tdd.interactors.get_post_reaction_metrics_interactor import \
    GetPostReactionMetricsInteractor
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import ReactionMetricDTO, Storage


def test_get_post_reaction_metrics_on_invalid_post_id_raise_exception():
    storage_mock = Mock()
    presenter_mock = Mock()

    post_id = 1
    storage_mock.is_post_exists.return_value = False

    get_post_reaction_metrics_interactor = GetPostReactionMetricsInteractor(
        storage_mock, presenter_mock
    )
    get_post_reaction_metrics_interactor.get_post_reaction_metrics(post_id)

    storage_mock.is_post_exists.assert_called_once()
    presenter_mock.raise_invalid_post_id_exception.assert_called_once()


def test_get_post_reaction_metrics_on_valid_input_get_response():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id = 1
    count = 3
    reaction_metric_dto = ReactionMetricDTO(reaction_type="Like", count=count)
    reaction_metric_list = {
        "reaction_type": ReactionType.LIKE.value,
        "count": count
    }

    storage_mock.is_post_exists.return_value = True
    storage_mock.get_post_reaction_metrics.return_value = [reaction_metric_dto]
    presenter_mock.get_reaction_metrics_response.return_value = [
        reaction_metric_list
    ]

    get_post_reaction_metrics_interactor = GetPostReactionMetricsInteractor(
        storage_mock, presenter_mock
    )
    result = get_post_reaction_metrics_interactor.get_post_reaction_metrics(
        post_id
    )

    assert result == [reaction_metric_list]
    storage_mock.is_post_exists.assert_called_once()
    storage_mock.get_post_reaction_metrics.assert_called_once()
    presenter_mock.get_reaction_metrics_response.assert_called_once_with(
        [reaction_metric_dto]
    )
