#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
import pytest

from fb_tdd.constants.get_post_util import populate_get_post_dto
from fb_tdd.constants.reactions import ReactionType
from fb_tdd.models.comment import Comment
from fb_tdd.models.post import Post
from fb_tdd.models.reaction import Reaction
from fb_tdd.storages.tdd_storage import SQLStorage


class TestStorage:
    @pytest.mark.django_db
    def test_get_total_reactions_count(self, post_reaction_data):
        storage = SQLStorage()
        reaction_count = storage.get_total_reactions_count()
        assert reaction_count == 1

    @pytest.mark.django_db
    def test_get_posts_with_more_positive_reactions(
            self, posts_with_more_positive_reactions):
        storage = SQLStorage()

        posts_dto_list = storage.get_positive_posts()

        assert len(posts_dto_list) == len(posts_with_more_positive_reactions)
        for (post_dto, post_fixture) in zip(
                posts_dto_list, posts_with_more_positive_reactions):
            assert post_dto.post_id == post_fixture ['id']

    @pytest.mark.django_db
    def test_delete_post(self, post_data):
        storage = SQLStorage()

        storage.delete_post(post_id=post_data.id)

        with pytest.raises(Post.DoesNotExist) as PostNotExist:
            Post.objects.get(id=post_data.id)

    @pytest.mark.django_db
    def test_is_post_exists(self, post_data):
        storage = SQLStorage()

        is_post_exists = storage.is_post_exists(post_id=post_data.id)

        assert is_post_exists is True

    @pytest.mark.django_db
    def test_is_user_exists(self, user_data):
        storage = SQLStorage()

        is_user_exists = storage.is_user_exists(user_id=user_data.id)

        assert is_user_exists

    @pytest.mark.django_db
    def test_is_authorized(self, post_data, user_data):
        storage = SQLStorage()

        is_authorized = storage.is_un_authorized_to_delete_post(
            user_id=user_data.id, post_id=post_data.id)

        assert is_authorized is True

    @pytest.mark.django_db
    def test_is_comment_exists(self, comment_data):
        storage = SQLStorage()

        is_comment_exists = storage.is_comment_exists(
            comment_id=comment_data.id)

        assert is_comment_exists is True

    @pytest.mark.django_db
    def test_is_comment_parent_exists(self, comment_replies):
        storage = SQLStorage()

        is_comment_parent_exists = storage.is_comment_parent_exists(
            comment_id=comment_replies [0].id)

        assert is_comment_parent_exists is True

    @pytest.mark.django_db
    def test_is_reaction_exists(self, post_reaction_data, post_data, user_data):
        storage = SQLStorage()

        is_post_reaction_exists = storage.is_post_reaction_exists(
            post_id=post_data.id, user_id=user_data.id)

        assert is_post_reaction_exists

    @pytest.mark.django_db
    def test_is_comment_reaction_exists(self, comment_reaction_data,
                                        comment_data, user_data):
        storage = SQLStorage()

        is_comment_reaction_exists = storage.is_comment_reaction_exists(
            comment_id=comment_data.id, user_id=user_data.id
        )

        assert is_comment_reaction_exists

    @pytest.mark.django_db
    def test_create_post_and_get_post_id(self, user_data):
        storage = SQLStorage()
        description = "THis is my first post."

        create_post = storage.create_post_and_get_post_id(
            user_id=user_data.id, description=description
        )

        db_post = Post.objects.get(user_id=user_data.id)

        assert create_post.post_id == db_post.id
        assert db_post.description == description
        assert db_post.user_id == user_data.id

    @pytest.mark.django_db
    def test_user_reacted_posts(self, user_reacted_posts, user_data):
        storage = SQLStorage()

        user_posts_dto_list = storage.get_user_reacted_posts(
            user_id=user_data.id)

        assert len(user_posts_dto_list) == len(user_reacted_posts)
        for (post_dto, post_fixture) in zip(
                user_posts_dto_list, user_reacted_posts):
            assert post_dto.post_id == post_fixture ['id']

    @pytest.mark.django_db
    def test_get_replies_for_comment(self, comment_replies, comment_data):
        storage = SQLStorage()

        replies_dto_list = storage.get_replies_for_comment(
            comment_id=comment_data.id)

        assert len(replies_dto_list) == len(comment_replies)
        for (reply_dto, reply_fixture) in zip(
                replies_dto_list, comment_replies):
            assert reply_dto.commenter.user_id == reply_fixture.user_id
            assert reply_dto.commenter.name == reply_fixture.user.name
            assert reply_dto.comment_id == reply_fixture.id
            description = reply_fixture.description
            assert reply_dto.comment_description == description

    @pytest.mark.django_db
    def test_undo_comment_reaction(self, comment_reaction_data, comment_data,
                                   user_data):
        storage = SQLStorage()
        storage.undo_comment_reaction(comment_id=comment_data.id,
                                      user_id=user_data.id)

        with pytest.raises(Reaction.DoesNotExist) as ReactionNotExist:
            Reaction.objects.get(
                user_id=user_data.id, comment_id=comment_data.id)

    @pytest.mark.django_db
    def test_update_comment_reaction(self, comment_reaction_data, comment_data,
                                     user_data):
        storage = SQLStorage()

        storage.update_comment_reaction(
            comment_id=comment_data.id, user_id=user_data.id,
            reaction_type=ReactionType.HAHA.value
        )
        is_reaction_updated = Reaction.objects.filter(
            comment_id=comment_data.id, user_id=user_data.id,
            reaction_type=ReactionType.HAHA.value
        ).exists()

        assert is_reaction_updated

    @pytest.mark.django_db
    def test_get_comment_reaction(self, comment_reaction_data, comment_data,
                                  user_data):
        storage = SQLStorage()

        comment_reaction_dto = storage.get_comment_reaction(
            user_id=user_data.id, comment_id=comment_data.id
        )

        assert comment_reaction_dto == ReactionType.LIKE.value

    @pytest.mark.django_db
    def test_react_to_comment(self, user_data, comment_data):
        storage = SQLStorage()

        storage.react_to_comment(
            user_id=user_data.id, comment_id=comment_data.id,
            reaction_type=ReactionType.LIKE.value)
        is_reaction_exists_in_db = Reaction.objects.filter(
            user_id=user_data.id, comment_id=comment_data.id,
            reaction_type=ReactionType.LIKE.value
        ).exists()

        assert is_reaction_exists_in_db

    @pytest.mark.django_db
    def test_get_post_reactions(self, post_reactions, post_data):
        storage = SQLStorage()

        reactions_dto_list = storage.get_post_reactions(post_id=post_data.id)

        assert len(reactions_dto_list) == len(post_reactions)
        for (reaction_dto, reaction_fixture) in zip(
                reactions_dto_list, post_reactions):
            assert reaction_dto.user_dto.user_id == reaction_fixture.user_id
            assert reaction_dto.user_dto.name == reaction_fixture.user.name
            assert reaction_dto.reaction_type == reaction_fixture.reaction_type

    @pytest.mark.django_db
    def test_get_post_reaction_metrics(self, reaction_metrics, post_data):
        storage = SQLStorage()

        reaction_metrics_dto_list = storage.get_post_reaction_metrics(
            post_id=post_data.id)

        for (reaction_dto, reaction_fixture) in zip(
                reaction_metrics_dto_list, reaction_metrics):
            dto_reaction_type = reaction_dto.reaction_type
            assert reaction_dto.count == reaction_fixture ['count']
            assert dto_reaction_type == reaction_fixture ['reaction_type']

    @pytest.mark.django_db
    def test_undo_post_reaction(self, post_reaction_data, post_data, user_data):
        storage = SQLStorage()

        storage.undo_post_reaction(post_id=post_data.id, user_id=user_data.id)

        with pytest.raises(Reaction.DoesNotExist) as ReactionNotExist:
            Reaction.objects.get(user_id=user_data.id, post_id=post_data.id)

    @pytest.mark.django_db
    def test_update_post_reaction(self, post_reaction_data, post_data,
                                  user_data):
        storage = SQLStorage()

        storage.update_post_reaction(
            post_id=post_data.id, user_id=user_data.id,
            reaction_type=ReactionType.HAHA.value
        )
        is_reaction_updated = Reaction.objects.filter(
            post_id=post_data.id, user_id=user_data.id,
            reaction_type=ReactionType.HAHA.value
        ).exists()

        assert is_reaction_updated

    @pytest.mark.django_db
    def test_get_post_reaction(self, post_reaction_data, post_data, user_data):
        storage = SQLStorage()

        post_reaction = storage.get_post_reaction(
            post_id=post_data.id, user_id=user_data.id
        )

        assert post_reaction_data.reaction_type == post_reaction

    @pytest.mark.django_db
    def test_react_to_post(self, user_data, post_data):
        storage = SQLStorage()

        storage.react_to_post(
            user_id=user_data.id, post_id=post_data.id,
            reaction_type=ReactionType.LIKE.value)
        is_reaction_exists_in_db = Reaction.objects.filter(
            user_id=user_data.id, post_id=post_data.id,
            reaction_type=ReactionType.LIKE.value
        ).exists()

        assert is_reaction_exists_in_db

    @pytest.mark.django_db
    def test_get_comment_parent_id(self, comment_replies, comment_data):
        storage = SQLStorage()

        parent_id = storage.get_comment_parent_id(
            comment_id=comment_replies [0].id)

        assert comment_data.id == parent_id

    @pytest.mark.django_db
    def test_reply_to_comment(self, user_data, comment_data):
        storage = SQLStorage()

        reply = storage.reply_to_comment(
            user_id=user_data.id, comment_id=comment_data.id,
            description="Reply to comment"
        )
        db_reply = Comment.objects.get(
            user_id=user_data.id, parent_id=comment_data.id)

        assert db_reply.id == reply.comment_id

    @pytest.mark.django_db
    def test_add_comment_get_comment_id(self, user_data, post_data):
        storage = SQLStorage()

        comment = storage.add_comment_get_id(
            user_id=user_data.id, post_id=post_data.id,
            description="Comment"
        )
        db_comment = Comment.objects.get(
            user_id=user_data.id, post_id=post_data.id)

        assert db_comment.id == comment.comment_id

    @pytest.mark.django_db
    def test_get_post(self, populate_get_post):
        storage = SQLStorage()
        get_post_dto = storage.get_post(post_id=1)
        custom_dto = populate_get_post_dto()

        for (post_dto, custom_post) in zip(get_post_dto.post, custom_dto.post):
            assert post_dto.post_id == custom_post.post_id
            assert post_dto.post_content == custom_post.post_content
        for (comment_dto, custom_comment) in zip(
                get_post_dto.comments, custom_dto.comments):
            assert comment_dto.comment_id == custom_comment.comment_id
        for (user_dto, custom_user) in zip(
                get_post_dto.users, custom_dto.users):
            assert user_dto.user_id == custom_user.user_id
            assert user_dto.name == custom_user.name
        for (reaction_dto, custom_reaction) in zip(
                get_post_dto.reactions, custom_dto.reactions):
            assert reaction_dto.reaction_type == custom_reaction.reaction_type
            assert reaction_dto.user_id == custom_reaction.user_id

    @pytest.mark.django_db
    def test_get_user_posts(self, populate_get_post):
        storage = SQLStorage()
        get_user_posts_dto = storage.get_user_posts(user_id=1)
        custom_dto = populate_get_post_dto()

        for (post_dto, custom_post) in zip(
                get_user_posts_dto.post, custom_dto.post):
            assert post_dto.post_id == custom_post.post_id
            assert post_dto.post_content == custom_post.post_content
        for (comment_dto, custom_comment) in zip(
                get_user_posts_dto.comments, custom_dto.comments):
            assert comment_dto.comment_id == custom_comment.comment_id
        for (user_dto, custom_user) in zip(
                get_user_posts_dto.users, custom_dto.users):
            assert user_dto.user_id == custom_user.user_id
            assert user_dto.name == custom_user.name
        for (reaction_dto, custom_reaction) in zip(
                get_user_posts_dto.reactions, custom_dto.reactions):
            assert reaction_dto.reaction_type == custom_reaction.reaction_type
            assert reaction_dto.user_id == custom_reaction.user_id
