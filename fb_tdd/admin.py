# your django admin
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from fb_tdd.models.comment import Comment
from fb_tdd.models.post import Post
from fb_tdd.models.reaction import Reaction
from fb_tdd.models.user import User

admin.site.register(User, UserAdmin)
admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Reaction)