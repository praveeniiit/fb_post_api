#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from typing import List

from django.db.models import Count, F, Prefetch, Q

from fb_tdd.constants.get_post_util import convert_full_post_to_dto
from fb_tdd.constants.reactions import (NEGATIVE_REACTIONS_LIST,
                                        POSITIVE_REACTIONS_LIST)
from fb_tdd.interactors.storages.storage import (CommentIdDTO,
                                                 PostCompleteDetailsDTO,
                                                 PostCompleteDetailsDTOWithTotal,
                                                 PostIdDTO, PostIdDTOWithTotal,
                                                 PostReactionDTO, ReactionDTO,
                                                 ReactionDTOWithTotal,
                                                 ReactionMetricDTO, ReplyDTO,
                                                 ReplyDTOWithCount, Storage,
                                                 UserDTO)
from fb_tdd.models.comment import Comment
from fb_tdd.models.post import Post
from fb_tdd.models.reaction import Reaction
from fb_tdd.models.user import User


class SQLStorage(Storage):
    def get_user_posts(self, user_id: int, offset: int,
                       limit: int) -> PostCompleteDetailsDTOWithTotal:
        posts = Post.objects.filter(user_id=user_id).select_related(
            'user').prefetch_related(
            Prefetch("reactions", to_attr="post_reactions"),
            Prefetch("comments",
                     queryset=Comment.objects.select_related('user'),
                     to_attr="post_comments"),
            Prefetch("post_comments__reactions", to_attr="comment_reactions"),
            Prefetch("post_comments__replies",
                     queryset=Comment.objects.select_related(
                         "user").prefetch_related(
                         Prefetch('reactions', to_attr="reply_reactions")),
                     to_attr="comment_replies"),
        )[offset:offset + limit]
        posts_count = Post.objects.filter(user_id=user_id).count()

        return PostCompleteDetailsDTOWithTotal(
            total=posts_count,
            user_post_dtos=convert_full_post_to_dto(posts)
        )

    def get_post(self, post_id: int) -> PostCompleteDetailsDTO:
        post = Post.objects.filter(id=post_id).select_related(
            'user').prefetch_related(
            Prefetch("reactions", to_attr="post_reactions"),
            Prefetch("comments",
                     queryset=Comment.objects.select_related('user'),
                     to_attr="post_comments"),
            Prefetch("post_comments__reactions", to_attr="comment_reactions"),
            Prefetch("post_comments__replies",
                     queryset=Comment.objects.select_related(
                         "user").prefetch_related(
                         Prefetch('reactions', to_attr="reply_reactions")),
                     to_attr="comment_replies"),
        )[0]
        return convert_full_post_to_dto([post])

    def add_comment_get_id(self, user_id: int, post_id: int,
                           description: str) -> CommentIdDTO:
        comment = Comment.objects.create(
            user_id=user_id, post_id=post_id, description=description
        )
        return CommentIdDTO(comment_id=comment.id)

    def reply_to_comment(self, user_id: int, comment_id: int,
                         description: str) -> CommentIdDTO:
        reply = Comment.objects.create(
            user_id=user_id, parent_id=comment_id, description=description
        )
        reply_id_dto = CommentIdDTO(comment_id=reply.id)

        return reply_id_dto

    def get_comment_parent_id(self, comment_id: int) -> int:
        return Comment.objects.get(id=comment_id).parent_id

    def react_to_post(self, user_id: int, post_id: int, reaction_type: str):
        Reaction.objects.create(
            user_id=user_id, post_id=post_id, reaction_type=reaction_type
        )

    def get_post_reaction(self, user_id: int, post_id: int) -> ReactionDTO:
        reaction = Reaction.objects.filter(post_id=post_id, user_id=user_id)
        if len(reaction) is not 0:
            return ReactionDTO(
                reaction_type=reaction.reaction_type, post_id=reaction.post_id,
                user_id=reaction.user_id, comment_id=reaction.comment_id)
        return False

    def update_post_reaction(self, user_id: int, post_id: int,
                             reaction_type: str):
        post_reaction = Reaction.objects.get(
            post_id=post_id, user_id=user_id
        )
        post_reaction.reaction_type = reaction_type
        post_reaction.save()

    def undo_post_reaction(self, user_id: int, post_id: int):
        Reaction.objects.filter(post_id=post_id, user_id=user_id).delete()

    def get_post_reaction_metrics(
            self, post_id: int) -> List[ReactionMetricDTO]:
        reaction_metrics = Reaction.objects.filter(post_id=post_id).values(
            "reaction_type").annotate(count=Count('reaction_type'))

        reaction_metrics_dto_list = []
        for reaction in reaction_metrics:
            reaction_dto = ReactionMetricDTO(
                reaction_type=reaction['reaction_type'],
                count=reaction['count'])
            reaction_metrics_dto_list.append(reaction_dto)

        return reaction_metrics_dto_list

    def get_post_reactions(self, post_id: int, offset=0,
                           limit=15) -> ReactionDTOWithTotal:
        reaction_with_user = Reaction.objects.filter(
            post_id=post_id).select_related('user')[offset:offset + limit]
        reaction_with_user_count = Reaction.objects.filter(
            post_id=post_id).count()

        reaction_with_user_dto_list = []
        for reaction in reaction_with_user:
            reacted_user = UserDTO(
                user_id=reaction.user_id, name=reaction.user.name,
                profile_pic_url=reaction.user.profile_pic_url
            )
            reaction_dto = PostReactionDTO(
                user_dto=reacted_user, reaction_type=reaction.reaction_type
            )
            reaction_with_user_dto_list.append(reaction_dto)

        return ReactionDTOWithTotal(
            total=reaction_with_user_count,
            reaction_dto_list=reaction_with_user_dto_list
        )

    def react_to_comment(self, user_id: int, comment_id: int,
                         reaction_type: str):
        Reaction.objects.create(
            user_id=user_id, comment_id=comment_id, reaction_type=reaction_type
        )

    def get_comment_reaction(
            self, user_id: int, comment_id: int) -> ReactionDTO:
        reaction = Reaction.objects.filter(comment_id=comment_id,
                                           user_id=user_id)
        if len(reaction) is not 0:
            return ReactionDTO(
                reaction_type=reaction.reaction_type, post_id=reaction.post_id,
                user_id=reaction.user_id, comment_id=reaction.comment_id)
        return False

    def update_comment_reaction(self, user_id: int, comment_id: int,
                                reaction_type: str):
        post_reaction = Reaction.objects.get(
            comment_id=comment_id, user_id=user_id
        )
        post_reaction.reaction_type = reaction_type
        post_reaction.save()

    def undo_comment_reaction(self, user_id: int, comment_id: int):
        Reaction.objects.filter(
            comment_id=comment_id, user_id=user_id).delete()

    def get_replies_for_comment(self, comment_id: int, offset=0,
                                limit=15) -> ReplyDTOWithCount:
        replies = Comment.objects.get(id=comment_id).replies.select_related(
            "user")[offset:offset + limit]
        replies_count = Comment.objects.get(id=comment_id).replies.count()
        replies_dto_list = []

        for reply in replies:
            commenter_dto = UserDTO(
                name=reply.user.name, user_id=reply.user_id,
                profile_pic_url=reply.user.profile_pic_url
            )
            replies_dto_list.append(
                ReplyDTO(comment_id=reply.id,
                         comment_description=reply.description,
                         commented_at=reply.pub_date, commenter=commenter_dto)
            )
        return ReplyDTOWithCount(total=replies_count,
                                 replies_dto=replies_dto_list)

    def get_user_reacted_posts(
            self, user_id, offset=0, limit=15) -> PostIdDTOWithTotal:
        user_posts = Post.objects.filter(reactions__user_id=user_id).values(
            'id').distinct()[offset:offset + limit]
        user_posts_count = Post.objects.filter(
            reactions__user_id=user_id).values('id').distinct().count()

        user_posts_dto_list = []
        for post in user_posts:
            post_id_dto = PostIdDTO(post_id=post['id'])
            user_posts_dto_list.append(post_id_dto)

        return PostIdDTOWithTotal(
            total=user_posts_count,
            post_id_dtos=user_posts_dto_list
        )

    def is_comment_exists(self, comment_id: int) -> bool:
        return Comment.objects.filter(id=comment_id).exists()

    def is_comment_reaction_exists(self, user_id: int, comment_id: int):
        return Reaction.objects.filter(
            comment_id=comment_id, user_id=user_id).exists()

    def is_comment_parent_exists(self, comment_id: int) -> bool:
        is_parent_exists = Comment.objects.get(id=comment_id).parent_id
        if is_parent_exists:
            return True
        else:
            return False

    def is_post_reaction_exists(self, user_id: int, post_id: int) -> bool:
        return Reaction.objects.filter(
            post_id=post_id, user_id=user_id).exists()

    def is_un_authorized_to_delete_post(self, user_id: int,
                                        post_id: int) -> bool:
        return not Post.objects.filter(user_id=user_id, id=post_id).exists()

    def delete_post(self, post_id: int):
        Post.objects.get(id=post_id).delete()

    def get_total_reactions_count(self) -> int:
        return Reaction.objects.count()

    def is_post_exists(self, post_id: int) -> bool:
        return Post.objects.filter(id=post_id).exists()

    def is_user_exists(self, user_id: int) -> bool:
        return User.objects.filter(id=user_id).exists()

    def get_positive_posts(self, offset=0, limit=15) -> PostIdDTOWithTotal:
        positive_exp = Count(
            'reactions',
            filter=Q(reactions__reaction_type__in=POSITIVE_REACTIONS_LIST))
        negative_exp = Count(
            'reactions',
            filter=Q(reactions__reaction_type__in=NEGATIVE_REACTIONS_LIST))

        positive_posts = Post.objects.annotate(
            positive_count=positive_exp, negative_count=negative_exp).filter(
            positive_count__gt=F('negative_count')).values(
            'id').distinct()[offset:offset + limit]

        positive_posts_count = Post.objects.annotate(
            positive_count=positive_exp, negative_count=negative_exp).filter(
            positive_count__gt=F('negative_count')).values(
            'id').distinct().count()

        positive_post_dto_list = []
        for post in positive_posts:
            post_id_dto = PostIdDTO(post_id=post['id'])
            positive_post_dto_list.append(post_id_dto)

        return PostIdDTOWithTotal(total=positive_posts_count,
                                  post_id_dtos=positive_post_dto_list)

    def create_post_and_get_post_id(self, user_id: int,
                                    description: str) -> PostIdDTO:
        post = Post.objects.create(
            description=description, user_id=user_id
        )
        post_id_dto = PostIdDTO(post_id=post.id)
        return post_id_dto
