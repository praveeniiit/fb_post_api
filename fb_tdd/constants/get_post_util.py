#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from fb_tdd.interactors.storages.storage import (PostDTO, ReactionWithUserDTO,
                                                 UserDTO, CommentDTO,
                                                 PostCompleteDetailsDTO)


def get_comments():
    return [
        {
            "comment_id": 1,
            "commenter": {
                "user_id": 1,
                "name": "Praveen",
                "profile_pic_url": None
            },
            "commented_at": "2019-05-21 20:22:46.810366",
            "comment_content": "Nothing",
            "reactions": {
                "count": 1,
                "type": ["LIKE"]
            },
            "replies_count": 1,
            "replies": []
        },
    ]


def populate_get_post_dto():
    post = [
        PostDTO(
            post_id=1, user_id=1, post_content="This is my first post.",
            posted_at="2019-09-08 12:12:23"
        )
    ]
    users = []
    comments = []
    reactions = [
        ReactionWithUserDTO(
            post_id=1, user_id=1, reaction_type="LIKE"
        )
    ]
    user = UserDTO(
        user_id=1, name="Praveen", profile_pic_url="None"
    )
    users.append(user)
    comment = CommentDTO(
        comment_id=1, comment_content="Waste comment",
        commented_at="2019-09-08 12:12:23", user_id=1
    )
    reaction = ReactionWithUserDTO(
        comment_id=1, user_id=1, reaction_type="LIKE"
    )
    reactions.append(reaction)
    comments.append(comment)
    reply = CommentDTO(
        comment_id=2, parent_id=1, comment_content="Reply",
        commented_at="2019-09-08 12:12:23"
    )
    reaction = ReactionWithUserDTO(
        comment_id=2, user_id=1, reaction_type="LIKE"
    )
    reactions.append(reaction)
    comments.append(reply)
    return PostCompleteDetailsDTO(
        post=post, users=users, comments=comments, reactions=reactions
    )


def convert_comment_to_dto(comment) -> CommentDTO:
    return CommentDTO(
        user_id=comment.user_id, post_id=comment.post_id,
        comment_id=comment.id, comment_content=comment.description,
        commented_at=comment.pub_date
    )


def convert_reply_to_dto(reply) -> CommentDTO:
    return CommentDTO(
        user_id=reply.user_id, parent_id=reply.post_id,
        comment_id=reply.id, comment_content=reply.description,
        commented_at=reply.pub_date
    )


def convert_post_reaction_to_dto(reaction) -> ReactionWithUserDTO:
    return ReactionWithUserDTO(
        user_id=reaction.user_id, post_id=reaction.post_id,
        reaction_type=reaction.reaction_type
    )


def convert_comment_reaction_to_dto(reaction) -> ReactionWithUserDTO:
    return ReactionWithUserDTO(
        user_id=reaction.user_id, comment_id=reaction.comment_id,
        reaction_type=reaction.reaction_type
    )


def convert_user_to_dto(user) -> UserDTO:
    return UserDTO(
        user_id=user.id, name=user.name, profile_pic_url=user.profile_pic_url
    )


def convert_full_post_to_dto(posts):
    posts_dto, users_dto = [], []
    reaction_dto_list, comments_dto_list = [], []
    for post in posts:
        post_dto = PostDTO(
            post_id=post.id, post_content=post.description,
            posted_at=post.pub_date, user_id=post.user_id
        )
        posts_dto.append(post_dto)
        users_dto.append(UserDTO(
            user_id=post.user_id, name=post.user.name,
            profile_pic_url=post.user.profile_pic_url
        ))

        for post_reaction in post.reactions.all():
            reaction_dto_list.append(
                convert_post_reaction_to_dto(post_reaction)
            )

        for comment in post.comments.all():
            comments_dto_list.append(convert_comment_to_dto(comment=comment))
            users_dto.append(convert_user_to_dto(user=comment.user))
            for comment_reaction in comment.reactions.all():
                reaction_dto_list.append(
                    convert_comment_reaction_to_dto(comment_reaction)
                )

            replies = comment.replies.all()
            for reply in replies:
                comments_dto_list.append(convert_reply_to_dto(reply=reply))
                users_dto.append(convert_user_to_dto(user=reply.user))
                for comment_reaction in reply.reactions.all():
                    reaction_dto_list.append(
                        convert_comment_reaction_to_dto(comment_reaction)
                    )
    post_complete_dto = PostCompleteDetailsDTO(
        post=posts_dto, users=users_dto, comments=comments_dto_list,
        reactions=reaction_dto_list
    )
    return post_complete_dto
