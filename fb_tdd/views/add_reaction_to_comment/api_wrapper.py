from django.http import HttpResponse
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_tdd.interactors.react_to_comment_interactor import \
    ReactToCommentInteractor
from fb_tdd.storages.tdd_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    storage = SQLStorage()

    comment_id = kwargs ['id']
    reaction_type = kwargs ['request_data'] ['reaction_type']
    user_id = kwargs ['user'].id

    react_to_comment_interactor = ReactToCommentInteractor(
        storage
    )
    react_to_comment_interactor.react_to_comment(
        user_id=user_id, comment_id=comment_id, reaction_type=reaction_type
    )

    return HttpResponse(status=202)
