# pylint: disable=wrong-import-position

APP_NAME = "fb_tdd"
OPERATION_NAME = "add_reaction_to_comment"
REQUEST_METHOD = "post"
URL_SUFFIX = "comment/{id}/react/v1/"

from .test_case_01 import TestCase01AddReactionToCommentAPITestCase

__all__ = [
    "TestCase01AddReactionToCommentAPITestCase"
]
