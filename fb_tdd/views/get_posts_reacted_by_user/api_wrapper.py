import json

from django.http import HttpResponse
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_tdd.interactors.get_user_reacted_posts_interactor import \
    GetUserReactedPostsInteractor
from fb_tdd.presenters.json_presenter import DataPresenter
from fb_tdd.storages.tdd_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    storage = SQLStorage()
    presenter = DataPresenter()

    user_id = kwargs ['user'].id
    query_parameters = kwargs ['request_query_params']
    offset = query_parameters.offset
    limit = query_parameters.limit

    get_posts_reacted_by_user_interactor = GetUserReactedPostsInteractor(
        storage, presenter
    )
    reacted_posts_response = \
        get_posts_reacted_by_user_interactor.get_user_reacted_posts(
            user_id=user_id, offset=offset, limit=limit)

    response_data = json.dumps(reacted_posts_response)
    return HttpResponse(response_data, status=200)
