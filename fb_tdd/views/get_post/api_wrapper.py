from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_tdd.interactors.get_post_interactor import GetPostInteractor
from fb_tdd.presenters.json_presenter import DataPresenter
from fb_tdd.storages.tdd_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    storage = SQLStorage()
    presenter = DataPresenter()

    post_id = kwargs ['id']

    get_post_interactor = GetPostInteractor(
        storage, presenter
    )
    post_response = get_post_interactor.get_post(post_id=post_id)

    import json
    from django.http.response import HttpResponse

    response_data = json.dumps(post_response)

    return HttpResponse(response_data, status=200)
