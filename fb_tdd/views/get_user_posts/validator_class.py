from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import ValidatorAbstractClass

from fb_tdd.utils.validate_query import ValidateQuery


class ValidatorClass(ValidatorAbstractClass, ValidateQuery):
    def __init__(self, *args, **kwargs):
        self.request_data = kwargs['request_data']
        self.user = kwargs['user']
        self.user_dto = kwargs['user_dto']
        self.access_token = kwargs['access_token']
        self.query_parameters = kwargs ['request_query_params']

    def validate(self):
        dict_ = dict()
        self.validate_offset(self.query_parameters.offset)
        self.validate_limit(self.query_parameters.limit)
        return dict_
