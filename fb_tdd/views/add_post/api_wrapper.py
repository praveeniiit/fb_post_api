from django.http import HttpResponse
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_tdd.interactors.create_post_interactor import CreatePostInteractor
from fb_tdd.presenters.json_presenter import DataPresenter
from fb_tdd.storages.tdd_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    user_id = kwargs ["user"].id
    post_content = kwargs ["request_data"] ["post_content"]
    storage = SQLStorage()
    presenter = DataPresenter()

    create_post_interactor = CreatePostInteractor(storage, presenter)
    create_post_get_id = create_post_interactor.create_post(
        user_id=user_id, description=post_content
    )
    import json
    response_data = json.dumps(create_post_get_id)
    return HttpResponse(response_data, status=202)
