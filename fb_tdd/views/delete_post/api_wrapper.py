from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_tdd.interactors.delete_post_interactor import DeletePostInteractor
from fb_tdd.storages.tdd_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    post_id = kwargs ['id']
    user_id = kwargs ['user'].id
    storage = SQLStorage()

    delete_post_interactor = DeletePostInteractor(
        storage
    )
    delete_post_interactor.delete_post(post_id=post_id, user_id=user_id)
