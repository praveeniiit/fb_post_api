from django.http import HttpResponse
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_tdd.interactors.get_total_reactions_count_interactor import \
    GetTotalReactionsInteractor
from fb_tdd.presenters.json_presenter import DataPresenter
from fb_tdd.storages.tdd_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    storage = SQLStorage()
    presenter = DataPresenter()

    get_total_reactions_count = GetTotalReactionsInteractor(storage, presenter)
    total_reaction_count = get_total_reactions_count.total_reactions_count()

    import json
    response_data = json.dumps(total_reaction_count)

    return HttpResponse(response_data, status=200)
