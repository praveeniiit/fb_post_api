# pylint: disable=wrong-import-position

APP_NAME = "fb_tdd"
OPERATION_NAME = "get_comment_replies"
REQUEST_METHOD = "get"
URL_SUFFIX = "comment/{id}/reply/v1/"

from .test_case_01 import TestCase01GetCommentRepliesAPITestCase

__all__ = [
    "TestCase01GetCommentRepliesAPITestCase"
]
