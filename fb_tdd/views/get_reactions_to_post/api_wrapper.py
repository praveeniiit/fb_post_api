from django.http import HttpResponse
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_tdd.interactors.get_reactions_to_post import \
    GetReactionsToPostInteractor
from fb_tdd.presenters.json_presenter import DataPresenter
from fb_tdd.storages.tdd_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    storage = SQLStorage()
    presenter = DataPresenter()

    post_id = kwargs ['id']
    query_parameters = kwargs ['request_query_params']
    offset = query_parameters.offset
    limit = query_parameters.limit

    get_reactions_to_post = GetReactionsToPostInteractor(
        storage, presenter
    )
    reactions_to_post_response = get_reactions_to_post.get_reactions_to_post(
        post_id=post_id, offset=offset, limit=limit)

    import json
    response_data = json.dumps(reactions_to_post_response)

    return HttpResponse(response_data, status=200)
