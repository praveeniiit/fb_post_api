# pylint: disable=wrong-import-position

APP_NAME = "fb_tdd"
OPERATION_NAME = "get_reactions_to_post"
REQUEST_METHOD = "get"
URL_SUFFIX = "post/{id}/reactions/v1/"

from .test_case_01 import TestCase01GetReactionsToPostAPITestCase

__all__ = [
    "TestCase01GetReactionsToPostAPITestCase"
]
