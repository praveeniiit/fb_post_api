from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_tdd.interactors.get_posts_with_more_positive_reactions_interactor \
    import \
    GetPositivePostsInteractor
from fb_tdd.presenters.json_presenter import DataPresenter
from fb_tdd.storages.tdd_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    storage = SQLStorage()
    presenter = DataPresenter()

    query_parameters = kwargs ['request_query_params']
    offset = query_parameters.offset
    limit = query_parameters.limit

    posts_with_more_positive_reaction = \
        GetPositivePostsInteractor(storage, presenter)
    positive_posts_response = \
        posts_with_more_positive_reaction.get_positive_posts(
            offset=offset, limit=limit)

    import json
    from django.http import HttpResponse

    response_data = json.dumps(positive_posts_response)
    return HttpResponse(response_data, status=200)
