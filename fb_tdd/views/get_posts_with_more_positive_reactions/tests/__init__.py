# pylint: disable=wrong-import-position

APP_NAME = "fb_tdd"
OPERATION_NAME = "get_posts_with_more_positive_reactions"
REQUEST_METHOD = "get"
URL_SUFFIX = "post/positive/reactions/v1/"

from .test_case_01 import TestCase01GetPostsWithMorePositiveReactionsAPITestCase

__all__ = [
    "TestCase01GetPostsWithMorePositiveReactionsAPITestCase"
]
