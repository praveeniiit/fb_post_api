import json

from django.http import HttpResponse
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_tdd.interactors.reply_to_comment_interactor import \
    ReplyToCommentInteractor
from fb_tdd.presenters.json_presenter import DataPresenter
from fb_tdd.storages.tdd_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    storage = SQLStorage()
    presenter = DataPresenter()

    comment_id = kwargs ['id']
    comment_description = kwargs ['request_data'] ['comment_description']
    user_id = kwargs ['user'].id

    reply_to_comment_interactor = ReplyToCommentInteractor(
        storage, presenter
    )
    reply_and_get_comment_id = reply_to_comment_interactor.reply_to_comment(
        comment_id=comment_id, user_id=user_id, description=comment_description
    )

    response_data = json.dumps(reply_and_get_comment_id)
    return HttpResponse(response_data, status=202)
