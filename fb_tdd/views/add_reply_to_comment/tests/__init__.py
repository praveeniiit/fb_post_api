# pylint: disable=wrong-import-position

APP_NAME = "fb_tdd"
OPERATION_NAME = "add_reply_to_comment"
REQUEST_METHOD = "post"
URL_SUFFIX = "comment/{id}/reply/v1/"

from .test_case_01 import TestCase01AddReplyToCommentAPITestCase

__all__ = [
    "TestCase01AddReplyToCommentAPITestCase"
]
