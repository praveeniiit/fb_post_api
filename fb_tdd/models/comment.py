#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.

from django.db import models

from fb_tdd.models.post import Post
from fb_tdd.models.user import User


class Comment(models.Model):
    description = models.CharField(max_length=146)
    pub_date = models.DateTimeField(auto_now=True)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True,
                               default=None,
                               blank=True, related_name="replies")
    post = models.ForeignKey(Post, on_delete=models.CASCADE,
                             related_name="comments",
                             null=True, default=None, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name="comments")

    def __str__(self):
        return self.description
