#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import Storage


class PostNotFound(Exception):
    pass


class AddCommentInteractor:
    def __init__(self, storage: Storage, presenter: Presenter):
        self.storage = storage
        self.presenter = presenter

    def add_comment(self, user_id: int, post_id: int, description: str):
        is_post_not_exist = not self.is_post_exists(post_id)
        if is_post_not_exist:
            raise Post
        
        new_comment_dto = self.storage.add_comment_get_id(
            user_id=user_id, post_id=post_id, description=description)
        return self.presenter.get_add_comment_response(new_comment_dto)

    def is_post_exists(self, post_id: int):
        is_post_exists = self.storage.is_post_exists(post_id=post_id)
        return is_post_exists
