#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from django_swagger_utils.drf_server.exceptions import NotFound

from fb_tdd.presenters.json_presenter import DataPresenter
from fb_tdd.storages.tdd_storage import SQLStorage


class GetPostInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def get_post(self, post_id):
        is_post_not_exists = not self.storage.is_post_exists(post_id=post_id)
        if is_post_not_exists:
            raise NotFound

        get_post_dto = self.storage.get_post(post_id=post_id)
        return self.presenter.get_post_response(get_post_dto)
