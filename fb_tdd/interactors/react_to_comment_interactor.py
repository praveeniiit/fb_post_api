#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.

from fb_tdd.constants.reactions import REACTION_LIST
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.storages.tdd_storage import SQLStorage


class ReactToCommentInteractor:
    def __init__(self, storage: SQLStorage, presenter: Presenter):
        self.storage = storage
        self.presenter = presenter

    def react_to_comment(self, user_id, comment_id, reaction_type):
        is_reaction_invalid = reaction_type not in REACTION_LIST
        if is_reaction_invalid:
            self.presenter.raise_invalid_reaction_exception()

        is_comment_do_not_exist = self._is_comment_do_not_exist(
            comment_id=comment_id)
        if is_comment_do_not_exist:
            self.presenter.raise_invalid_comment_id_exception()

        reaction = self.storage.get_comment_reaction(
            user_id=user_id, comment_id=comment_id
        )
        is_reaction_exists = reaction is not False

        if is_reaction_exists:
            is_undo_action = reaction.reaction_type == reaction_type
            self._update_or_undo_the_reaction(
                undo_action=is_undo_action, user_id=user_id,
                comment_id=comment_id, reaction_type=reaction_type)
        else:
            self.storage.react_to_comment(
                user_id=user_id, comment_id=comment_id,
                reaction_type=reaction_type
            )

    def _is_comment_do_not_exist(self, comment_id):
        return not self.storage.is_comment_exists(comment_id)

    def _update_or_undo_the_reaction(
            self, undo_action: bool, user_id: int, comment_id: int,
            reaction_type: str):
        if undo_action:
            self.storage.undo_comment_reaction(
                user_id=user_id, comment_id=comment_id,
            )
        else:
            self.storage.update_comment_reaction(
                user_id=user_id, comment_id=comment_id,
                reaction_type=reaction_type
            )
