#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.

from fb_tdd.presenters.json_presenter import DataPresenter
from fb_tdd.storages.tdd_storage import SQLStorage


class ReplyToCommentInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def _is_comment_do_not_exist(self, comment_id):
        return not self.storage.is_comment_exists(comment_id=comment_id)

    def reply_to_comment(self, user_id: int, comment_id: int, description: str):
        is_comment_do_not_exist = self._is_comment_do_not_exist(
            comment_id=comment_id)

        if is_comment_do_not_exist:
            self.presenter.raise_invalid_comment_id_exception()

        parent_comment_id = self.storage.get_comment_parent_id(
            comment_id=comment_id)
        if parent_comment_id is None:
            parent_comment_id = comment_id

        reply_id_dto = self.storage.reply_to_comment(
            user_id=user_id, comment_id=parent_comment_id,
            description=description
        )

        return self.presenter.get_reply_to_comment_response(reply_id_dto)
