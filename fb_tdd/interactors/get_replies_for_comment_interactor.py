#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.

from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import Storage


class GetRepliesForCommentInteractor:
    def __init__(self, storage: Storage, presenter: Presenter):
        self.storage = storage
        self.presenter = presenter

    def get_comment_replies(self, comment_id, offset=0, limit=15):
        is_comment_do_not_exist = not self.storage.is_comment_exists(comment_id)
        if is_comment_do_not_exist:
            self.presenter.raise_invalid_comment_id_exception()

        replies_dto = self.storage.get_replies_for_comment(
            comment_id=comment_id, offset=offset, limit=limit)
        return self.presenter.get_replies_for_comment_response(replies_dto)
