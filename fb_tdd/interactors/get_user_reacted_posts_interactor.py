#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.

from fb_tdd.presenters.json_presenter import DataPresenter
from fb_tdd.storages.tdd_storage import SQLStorage


class GetUserReactedPostsInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def get_user_reacted_posts(self, user_id, offset=0, limit=15):
        is_user_not_exists = not self.storage.is_user_exists(user_id=user_id)
        if is_user_not_exists:
            self.presenter.raise_invalid_user_id_exception()

        reacted_posts_dto = self.storage.get_user_reacted_posts(
            user_id=user_id, offset=offset, limit=limit)
        return self.presenter.get_user_reacted_posts_response(reacted_posts_dto)
