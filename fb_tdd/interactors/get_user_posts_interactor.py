#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.

from fb_tdd.presenters.json_presenter import DataPresenter
from fb_tdd.storages.tdd_storage import SQLStorage


class GetUserPostsInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def get_user_posts(self, user_id: int, offset: int, limit: int = 15):
        is_user_not_exists = not self.storage.is_user_exists(user_id=user_id)
        if is_user_not_exists:
            self.presenter.raise_invalid_user_id_exception()

        user_posts_dto = self.storage.get_user_posts(user_id=user_id,
                                                     offset=offset, limit=limit)
        return self.presenter.get_user_posts_response(user_posts_dto)
