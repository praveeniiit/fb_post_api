#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.

from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import Storage


class DeletePostInteractor:
    def __init__(self, storage: Storage, presenter: Presenter):
        self.storage = storage
        self.presenter = presenter

    def delete_post(self, user_id: int, post_id: int):
        is_post_not_exists = not self.storage.is_post_exists(post_id=post_id)
        if is_post_not_exists:
            self.presenter.raise_invalid_post_id_exception()

        is_unauthorized = self.storage.is_un_authorized_to_delete_post(
            user_id=user_id, post_id=post_id)
        if is_unauthorized:
            self.presenter.raise_un_authorized_access()

        self.storage.delete_post(post_id=post_id)
