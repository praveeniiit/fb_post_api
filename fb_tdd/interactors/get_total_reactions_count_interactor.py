#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import Storage


class GetTotalReactionsInteractor:
    def __init__(self, storage: Storage, presenter: Presenter):
        self.storage = storage
        self.presenter = presenter

    def total_reactions_count(self):
        count = self.storage.get_total_reactions_count()
        return self.presenter.get_total_reactions_count_response(count)
