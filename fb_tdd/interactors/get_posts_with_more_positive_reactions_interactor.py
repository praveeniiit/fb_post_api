#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from fb_tdd.presenters.json_presenter import DataPresenter
from fb_tdd.storages.tdd_storage import SQLStorage


class GetPositivePostsInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def get_positive_posts(self, offset=0, limit=15):
        positive_post_dtos = self.storage.get_positive_posts(
            offset=offset, limit=limit
        )
        return self.presenter.get_positive_posts_response(positive_post_dtos)
