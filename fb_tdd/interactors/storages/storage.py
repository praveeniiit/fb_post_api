#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from typing import Any, List


@dataclass
class PostIdDTO:
    post_id: int


@dataclass
class PostIdDTOWithTotal:
    total: int
    post_id_dtos: List[PostIdDTO]


@dataclass
class PostDTO:
    post_id: int
    posted_at: Any
    post_content: str
    user_id: int = None


@dataclass
class ReactionMetricDTO:
    reaction_type: str
    count: int


@dataclass
class CommentIdDTO:
    comment_id: int


@dataclass
class CommentDTO:
    comment_id: int
    comment_content: str
    commented_at: Any
    post_id: int = None
    user_id: int = None
    parent_id: int = None


@dataclass
class UserDTO:
    user_id: int
    name: str
    profile_pic_url: Any


@dataclass
class PostReactionDTO:
    user_dto: UserDTO
    reaction_type: str


@dataclass
class ReplyDTO:
    commenter: UserDTO
    comment_id: int
    commented_at: str
    comment_description: str


@dataclass
class ReplyDTOWithCount:
    replies_dto: List[ReplyDTO]
    total: int


@dataclass
class ReactionDTO:
    user_id: int
    reaction_type: str
    post_id: int = None
    comment_id: int = None


@dataclass
class ReactionDTOWithTotal:
    total: int
    reaction_dto_list: List[PostReactionDTO]


@dataclass
class ReactionWithUserDTO:
    reaction_type: str
    user_id: int = None
    comment_id: int = None
    post_id: int = None


@dataclass
class PostCompleteDetailsDTO:
    post: List[PostDTO]
    users: List[UserDTO]
    comments: List[CommentDTO] = None
    reactions: List[ReactionWithUserDTO] = None


@dataclass
class PostCompleteDetailsDTOWithTotal:
    total: int
    user_post_dtos: List[PostCompleteDetailsDTO]


class Storage:
    __metaclass__ = ABCMeta

    @abstractmethod
    def create_post_and_get_post_id(
            self, user_id: int, description: str) -> PostIdDTO:
        pass

    @abstractmethod
    def add_comment_get_id(
            self, user_id: int, post_id: int, description: str) -> CommentIdDTO:
        pass

    @abstractmethod
    def is_post_exists(self, post_id: int) -> bool:
        pass

    @abstractmethod
    def is_comment_exists(self, comment_id: int) -> bool:
        pass

    @abstractmethod
    def reply_to_comment(
            self, user_id: int, comment_id: int, description: str
    ) -> CommentIdDTO:
        pass

    @abstractmethod
    def is_comment_parent_exists(self, comment_id: int) -> bool:
        pass

    @abstractmethod
    def get_comment_parent_id(self, comment_id: int) -> bool:
        pass

    @abstractmethod
    def is_post_reaction_exists(self, user_id: int, post_id: int) -> bool:
        pass

    @abstractmethod
    def react_to_post(self, user_id: int, post_id: int, reaction_type: str):
        pass

    @abstractmethod
    def get_post_reaction(self, user_id: int, post_id: int) -> str:
        pass

    @abstractmethod
    def update_post_reaction(self, user_id: int, post_id: int,
                             reaction_type: str):
        pass

    @abstractmethod
    def undo_post_reaction(self, user_id: int, post_id: int):
        pass

    @abstractmethod
    def react_to_comment(self, user_id: int, comment_id: int,
                         reaction_type: str):
        pass

    @abstractmethod
    def get_comment_reaction(self, user_id: int, comment_id: int) -> str:
        pass

    @abstractmethod
    def is_comment_reaction_exists(self, user_id: int, comment_id: int):
        pass

    @abstractmethod
    def update_comment_reaction(self, user_id: int, comment_id: int,
                                reaction_type: str):
        pass

    @abstractmethod
    def undo_comment_reaction(self, user_id: int, comment_id: int):
        pass

    @abstractmethod
    def is_un_authorized_to_delete_post(self, user_id: int, post_id: int):
        pass

    @abstractmethod
    def delete_post(self, post_id: int):
        pass

    @abstractmethod
    def get_total_reactions_count(self) -> int:
        pass

    @abstractmethod
    def get_replies_for_comment(self, comment_id: int, offset=0,
                                limit=15) -> ReplyDTO:
        pass

    @abstractmethod
    def is_user_exists(self, user_id: int) -> bool:
        pass

    @abstractmethod
    def get_user_reacted_posts(self, user_id, offset=0, limit=15) -> List[
        PostDTO]:
        pass

    @abstractmethod
    def get_positive_posts(self, offset=0, limit=15) -> List[PostIdDTO]:
        pass

    @abstractmethod
    def get_post_reaction_metrics(
            self, post_id: int) -> List[ReactionMetricDTO]:
        pass

    @abstractmethod
    def get_post_reactions(
            self, post_id: int, offset=0, limit=15) -> List[PostReactionDTO]:
        pass

    @abstractmethod
    def get_post(self, post_id: int) -> PostCompleteDetailsDTO:
        pass

    @abstractmethod
    def get_user_posts(self, user_id: int, offset: int,
                       limit: int) -> PostCompleteDetailsDTOWithTotal:
        pass
