#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.

from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import Storage


class GetPostReactionMetricsInteractor:
    def __init__(self, storage: Storage, presenter: Presenter):
        self.storage = storage
        self.presenter = presenter

    def get_post_reaction_metrics(self, post_id):
        is_post_not_exists = not self.storage.is_post_exists(post_id=post_id)
        if is_post_not_exists:
            self.presenter.raise_invalid_post_id_exception()

        reaction_metrics = self.storage.get_post_reaction_metrics(post_id)
        return self.presenter.get_reaction_metrics_response(reaction_metrics)
