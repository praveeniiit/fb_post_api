#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import Storage


class CreatePostInteractor:
    def __init__(self, storage: Storage, presenter: Presenter):
        self.storage = storage
        self.presenter = presenter

    def create_post(self, user_id: int, description: str):
        post_id_dto = self.storage.create_post_and_get_post_id(
            user_id=user_id, description=description
        )
        return self.presenter.get_create_post_response(post_id_dto)
