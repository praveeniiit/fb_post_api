#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.

from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import Storage


class AddCommentInteractor:
    def __init__(self, storage: Storage, presenter: Presenter):
        self.storage = storage
        self.presenter = presenter

    def add_comment(self, user_id: int, post_id: int, description: str):
        is_post_do_not_exist = self._is_post_do_not_exists(post_id=post_id)
        if is_post_do_not_exist:
            self.presenter.raise_invalid_post_id_exception()

        new_comment_id_dto = self.storage.add_comment_get_id(
            user_id=user_id, post_id=post_id, description=description)
        return self.presenter.get_add_comment_response(new_comment_id_dto)

    def _is_post_do_not_exists(self, post_id: int):
        return not self.storage.is_post_exists(post_id=post_id)
