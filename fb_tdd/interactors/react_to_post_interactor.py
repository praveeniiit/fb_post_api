#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.

from fb_tdd.constants.reactions import REACTION_LIST
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.storages.tdd_storage import SQLStorage


class ReactToPostInteractor:
    def __init__(self, storage: SQLStorage, presenter: Presenter):
        self.storage = storage
        self.presenter = presenter

    def is_post_do_not_exist(self, post_id):
        return not self.storage.is_post_exists(post_id=post_id)

    def react_to_post(self, user_id: int, post_id: int, reaction_type: str):
        is_invalid_reaction_type = reaction_type not in REACTION_LIST
        if is_invalid_reaction_type:
            self.presenter.raise_invalid_reaction_exception()

        is_post_not_exists = self.is_post_do_not_exist(post_id=post_id)
        if is_post_not_exists:
            self.presenter.raise_invalid_post_id_exception()

        reaction = self.storage.get_post_reaction(user_id=user_id,
                                                  post_id=post_id)
        is_reaction_exists = reaction is not False

        if is_reaction_exists:
            is_undo_action = reaction.reaction_type == reaction_type
            self._update_or_undo_the_reaction(
                undo_action=is_undo_action, user_id=user_id, post_id=post_id,
                reaction_type=reaction_type)
        else:
            self.storage.react_to_post(
                user_id=user_id, post_id=post_id, reaction_type=reaction_type
            )

    def _update_or_undo_the_reaction(
            self, undo_action: bool, user_id: int, post_id: int,
            reaction_type: str):
        if undo_action:
            self.storage.undo_post_reaction(
                user_id=user_id, post_id=post_id
            )
        else:
            self.storage.update_post_reaction(
                user_id=user_id, post_id=post_id,
                reaction_type=reaction_type
            )
