#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.

from fb_tdd.presenters.json_presenter import DataPresenter
from fb_tdd.storages.tdd_storage import SQLStorage


class GetReactionsToPostInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def get_reactions_to_post(self, post_id, offset=0, limit=15):
        is_post_do_not_exist = not self.storage.is_post_exists(post_id=post_id)
        if is_post_do_not_exist:
            self.presenter.raise_invalid_post_id_exception()

        post_reactions_dto = self.storage.get_post_reactions(
            post_id=post_id, offset=offset, limit=limit)
        return self.presenter.get_post_reactions_response(post_reactions_dto)
