#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from abc import ABCMeta, abstractmethod
from typing import List

from fb_tdd.constants.exception_messages import INVALID_REACTION
from fb_tdd.interactors.storages.storage import (CommentIdDTO, PostDTO,
                                                 PostIdDTO, PostReactionDTO,
                                                 ReactionMetricDTO, ReplyDTO,
                                                 PostCompleteDetailsDTO)


class Presenter:
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_create_post_response(self, post_id_dto: PostIdDTO):
        pass

    @abstractmethod
    def get_add_comment_response(self, comment_id_dto: CommentIdDTO):
        pass

    @abstractmethod
    def get_reply_to_comment_response(self, reply_id_dto: CommentIdDTO):
        pass

    @abstractmethod
    def get_total_reactions_count_response(self, count: int):
        pass

    @abstractmethod
    def get_replies_for_comment_response(self, replies_dto: List [ReplyDTO]):
        pass

    @abstractmethod
    def get_user_reacted_posts_response(self,
                                        reacted_posts_dto: List [PostDTO]):
        pass

    @abstractmethod
    def get_positive_posts_response(self, post_id_dtos: List [PostIdDTO]):
        pass

    @abstractmethod
    def get_reaction_metrics_response(
            self, reaction_metrics: List [ReactionMetricDTO]):
        pass

    @abstractmethod
    def get_post_reactions_response(self,
                                    post_reactions_dto: List [PostReactionDTO]):
        pass

    @abstractmethod
    def get_post_response(self, get_post_dto: PostCompleteDetailsDTO):
        pass

    @abstractmethod
    def get_user_posts_response(self, user_posts_dto: PostCompleteDetailsDTO):
        pass

    @abstractmethod
    def raise_invalid_post_id_exception(self):
        pass

    @abstractmethod
    def raise_invalid_comment_id_exception(self):
        pass

    @abstractmethod
    def raise_invalid_user_id_exception(self):
        pass

    @abstractmethod
    def raise_invalid_reaction_exception(self):
        pass

    @abstractmethod
    def raise_un_authorized_access(self):
        pass
