#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from typing import List

from django_swagger_utils.drf_server.exceptions import (BadRequest, NotFound,
                                                        Forbidden)

from fb_tdd.constants.exception_messages import (INVALID_COMMENT_ID,
                                                 INVALID_POST_ID,
                                                 INVALID_REACTION,
                                                 INVALID_USER_ID,
                                                 ACCESS_FORBIDDEN)
from fb_tdd.interactors.presenters.presenter import Presenter
from fb_tdd.interactors.storages.storage import (
    CommentIdDTO, PostCompleteDetailsDTO, PostCompleteDetailsDTOWithTotal,
    PostIdDTO, PostIdDTOWithTotal, ReactionDTOWithTotal, ReactionMetricDTO,
    ReplyDTOWithCount)


def convert_user_dto_to_dict(user_dto):
    return {
        'user_id': user_dto.user_id,
        'name': user_dto.name,
        'profile_pic': user_dto.profile_pic_url
    }


class DataPresenter(Presenter):
    def get_create_post_response(self, post_id_dto: PostIdDTO):
        return {
            'post_id': post_id_dto.post_id
        }

    def get_add_comment_response(self, comment_id_dto: CommentIdDTO):
        return {
            'comment_id': comment_id_dto.comment_id
        }

    def get_reply_to_comment_response(self, reply_id_dto: CommentIdDTO):
        return {
            'reply_comment_id': reply_id_dto.comment_id
        }

    def get_total_reactions_count_response(self, count: int):
        return {
            'reactions_count': count
        }

    def get_replies_for_comment_response(self, reply_total: ReplyDTOWithCount):
        replies_dto = reply_total.replies_dto
        replies = []
        for reply in replies_dto:
            user_dto = reply.commenter
            replies.append(
                {
                    'comment_id': reply.comment_id,
                    'commented_at': reply.commented_at.strftime(
                        "%m/%d/%Y, %H:%M:%S"),
                    'comment_description': reply.comment_description,
                    'commenter': convert_user_dto_to_dict(user_dto)
                }
            )
        return {
            'total': reply_total.total,
            'result': replies
        }

    def get_user_reacted_posts_response(self,
                                        post_with_total: PostIdDTOWithTotal):
        reacted_posts_dto = post_with_total.post_id_dtos
        return {
            'total': post_with_total.total,
            'result': [
                {
                    'post_id': post.post_id
                } for post in reacted_posts_dto
            ]
        }

    def get_positive_posts_response(self, post_with_total: PostIdDTOWithTotal):
        post_id_dtos = post_with_total.post_id_dtos
        return {
            'total': post_with_total.total,
            'result': [
                {
                    'post_id': post.post_id
                } for post in post_id_dtos
            ]
        }

    def get_reaction_metrics_response(
            self, reaction_metrics: List[ReactionMetricDTO]):
        return [
            {
                'reaction_type': reaction.reaction_type,
                'count': reaction.count
            } for reaction in reaction_metrics
        ]

    def get_post_reactions_response(
            self, post_reactions_with_total: ReactionDTOWithTotal):
        post_reactions_dto = post_reactions_with_total.reaction_dto_list
        post_reactions = []
        for reaction in post_reactions_dto:
            user_dto_list = convert_user_dto_to_dict(reaction.user_dto)
            user_dto_list.update(
                {'reaction': reaction.reaction_type}
            )
            post_reactions.append(user_dto_list)

        return {
            'total': post_reactions_with_total.total,
            'result': post_reactions
        }

    def get_post_response(self, get_post_dto: PostCompleteDetailsDTO):
        post = get_post_dto.post[0]

        return self._convert_single_post_dto_to_dict(
            complete_dto=get_post_dto, post=post)

    def get_user_posts_response(
            self, posts_with_total: PostCompleteDetailsDTOWithTotal):
        user_posts_dto = posts_with_total.user_post_dtos
        posts = user_posts_dto.post
        posts_dict_list = []
        for post in posts:
            posts_dict_list.append(
                self._convert_single_post_dto_to_dict(
                    complete_dto=user_posts_dto, post=post
                )
            )

        return {
            'total': posts_with_total.total,
            'result': posts_dict_list
        }

    def _convert_single_post_dto_to_dict(self, complete_dto, post):
        users_dto = complete_dto.users
        comments_dto = filter(
            lambda x: x.post_id == post.post_id, complete_dto.comments)

        post_replies = filter(lambda x: x.post_id is None,
                              complete_dto.comments)

        from collections import defaultdict
        comment_replies_map = defaultdict(lambda: [])
        for reply in post_replies:
            comment_replies_map[reply.parent_id].append(reply)

        post_dict = self._convert_post_dto_to_dict(
            post=post, users_dto=users_dto)
        post_reactions_dto = filter(
            lambda x: x.post_id == post.post_id,
            complete_dto.reactions)

        post_dict['reactions'] = self._convert_reaction_dto_to_dict(
            post_reactions_dto=post_reactions_dto)
        post_dict['comments'] = self._convert_comments_dto_to_dict(
            comments_dto=comments_dto, users_dto=users_dto,
            get_post_dto=complete_dto,
            comment_replies_map=comment_replies_map
        )

        return post_dict

    def _convert_comments_dto_to_dict(self, comments_dto, users_dto,
                                      get_post_dto, comment_replies_map):
        comment_list = []
        for comment in comments_dto:
            comment_dict = self._convert_single_comment_dto_to_dict(
                comment=comment, users_dto=users_dto,
                get_post_dto=get_post_dto
            )
            comment_dict['replies'] = self._convert_reply_dto_to_dict(
                comment=comment, users_dto=users_dto, get_post_dto=get_post_dto,
                comment_replies_map=comment_replies_map
            )
            comment_list.append(
                comment_dict
            )
        return comment_list

    def _convert_reply_dto_to_dict(
            self, comment, users_dto, get_post_dto, comment_replies_map):
        replies = comment_replies_map[comment.comment_id]

        replies_list = []
        for reply in replies:
            reply_dict = self._convert_single_comment_dto_to_dict(
                reply, users_dto, get_post_dto=get_post_dto)
            replies_list.append(reply_dict)

        return replies_list

    def _convert_single_comment_dto_to_dict(self, comment, users_dto,
                                            get_post_dto):
        users = filter(lambda u: u.user_id == comment.user_id, users_dto)
        user = [user for user in users][0]
        comment_dict = {
            'comment_content': comment.comment_content,
            'commented_at': comment.commented_at.strftime(
                "%m/%d/%Y, %H:%M:%S"),
            'commenter': self._convert_user_dto_to_dict(user)
        }

        comment_reactions = filter(
            lambda x: x.comment_id == comment.comment_id,
            get_post_dto.reactions)
        comment_dict['reactions'] = self._convert_reaction_dto_to_dict(
            comment_reactions)

        return comment_dict

    def _convert_post_dto_to_dict(self, post, users_dto):
        users = filter(lambda u: u.user_id == post.user_id, users_dto)
        user_dto = [user for user in users][0]
        user_dict = self._convert_user_dto_to_dict(user_dto)

        return {
            'post_id': post.post_id,
            'post_content': post.post_content,
            'posted_by': user_dict,
            'posted_at': post.posted_at.strftime("%m/%d/%Y, %H:%M:%S")
        }

    @staticmethod
    def _convert_user_dto_to_dict(user_dto):
        return {
            'name': user_dto.name,
            'user_id': user_dto.user_id,
            'profile_pic_url': user_dto.profile_pic_url
        }

    @staticmethod
    def _convert_reaction_dto_to_dict(post_reactions_dto):
        reactions = [p.reaction_type for p in post_reactions_dto]
        reaction_types = sorted(list(set(reactions)))

        return {
            'type': reaction_types,
            'count': len(reactions)
        }

    def raise_invalid_user_id_exception(self):
        raise NotFound(*INVALID_USER_ID)

    def raise_invalid_post_id_exception(self):
        raise NotFound(*INVALID_POST_ID)

    def raise_invalid_comment_id_exception(self):
        raise NotFound(*INVALID_COMMENT_ID)

    def raise_invalid_reaction_exception(self):
        raise BadRequest(*INVALID_REACTION)

    def raise_un_authorized_access(self):
        raise Forbidden(*ACCESS_FORBIDDEN)
