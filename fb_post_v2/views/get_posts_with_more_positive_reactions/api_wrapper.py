from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post_v2.interactors.get_posts_with_more_positive_reactions_interactor import \
    GetPostsWithMorePositiveReactionsInteractor
from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    storage = SQLStorage()
    presenter = DataPresenter()

    query_parameters = kwargs['request_query_params']
    offset = query_parameters.offset
    limit = query_parameters.limit

    posts_with_more_positive_reaction = \
        GetPostsWithMorePositiveReactionsInteractor(storage, presenter)
    positive_posts_response = \
        posts_with_more_positive_reaction.posts_with_more_positive_reactions(
            offset=offset, limit=limit
        )

    import json
    from django.http import HttpResponse

    response_data = json.dumps(positive_posts_response)
    return HttpResponse(response_data, status=200)
