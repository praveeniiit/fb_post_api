from django.http import HttpResponse
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post_v2.interactors.get_replies_for_comment_interactor import \
    GetRepliesForCommentInteractor
from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    storage = SQLStorage()
    presenter = DataPresenter()

    comment_id = kwargs['id']
    query_parameters = kwargs['request_query_params']
    offset = query_parameters.offset
    limit = query_parameters.limit

    get_comment_replies_interactor = GetRepliesForCommentInteractor(
        storage, presenter
    )
    comment_replies_response = \
        get_comment_replies_interactor.get_replies_for_comment(
            comment_id=comment_id, offset=offset, limit=limit)

    import json
    response_data = json.dumps(comment_replies_response)

    return HttpResponse(response_data, status=200)
