# pylint: disable=wrong-import-position

APP_NAME = "fb_post_v2"
OPERATION_NAME = "add_post"
REQUEST_METHOD = "post"
URL_SUFFIX = "post/v1/"

from .test_case_01 import TestCase01AddPostAPITestCase

__all__ = [
    "TestCase01AddPostAPITestCase"
]
