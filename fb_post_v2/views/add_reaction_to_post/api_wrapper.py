from django.http import HttpResponse
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post_v2.interactors.react_to_post_interactor import \
    ReactToPostInteractor
from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    storage = SQLStorage()
    presenter = DataPresenter()

    post_id = kwargs['id']
    reaction_type = kwargs['request_data']['reaction_type']
    user_id = kwargs['user'].id

    react_to_post_interactor = ReactToPostInteractor(
        storage, presenter
    )
    react_to_post_interactor.react_to_post(
        user_id=user_id, post_id=post_id, reaction_type=reaction_type
    )

    return HttpResponse(status=202)
