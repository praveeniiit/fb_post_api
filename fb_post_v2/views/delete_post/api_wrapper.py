from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post_v2.interactors.delete_post_interactor import DeletePostInteractor
from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    post_id = kwargs['id']

    storage = SQLStorage()
    presenter = DataPresenter()

    delete_post_interactor = DeletePostInteractor(
        storage, presenter
    )
    delete_post_interactor.delete_post(post_id=post_id)

