# pylint: disable=wrong-import-position

APP_NAME = "fb_post_v2"
OPERATION_NAME = "add_comment"
REQUEST_METHOD = "post"
URL_SUFFIX = "post/{id}/comment/v1/"

from .test_case_01 import TestCase01AddCommentAPITestCase

__all__ = [
    "TestCase01AddCommentAPITestCase"
]
