from django.http import HttpResponse
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post_v2.interactors.add_comment_interactor import AddCommentInteractor
from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    user_id = kwargs['user'].id
    post_id = kwargs['id']
    comment_description = kwargs['request_data']['comment_description']

    storage = SQLStorage()
    presenter = DataPresenter()

    add_comment_interactor = AddCommentInteractor(storage, presenter)
    add_comment_and_get_id = add_comment_interactor.add_comment(
        user_id=user_id, post_id=post_id, description=comment_description
    )

    import json
    response_data = json.dumps(add_comment_and_get_id)
    return HttpResponse(response_data, status=202)
