from django.http import HttpResponse
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post_v2.interactors.get_user_posts_interactor import \
    GetUserPostsInteractor
from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    storage = SQLStorage()
    presenter = DataPresenter()

    user_id = kwargs['id']
    query_parameters = kwargs['request_query_params']
    offset = query_parameters.offset
    limit = query_parameters.limit

    get_user_posts = GetUserPostsInteractor(
        storage, presenter
    )
    get_user_posts_response = get_user_posts.get_user_posts(
        user_id=user_id, offset=offset, limit=limit)

    import json
    response_data = json.dumps(get_user_posts_response)

    return HttpResponse(response_data, status=200)
