from django.http import HttpResponse
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post_v2.interactors.get_total_reaction_count import \
    GetTotalReactionsCount
from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    storage = SQLStorage()
    presenter = DataPresenter()

    get_total_reactions_count = GetTotalReactionsCount(storage, presenter)
    total_reaction_count = get_total_reactions_count.get_total_reaction_count()

    import json
    response_data = json.dumps(total_reaction_count)

    return HttpResponse(response_data, status=200)
