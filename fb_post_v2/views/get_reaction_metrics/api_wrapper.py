from django.http import HttpResponse
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post_v2.interactors.get_reaction_metrics_interactor import \
    GetReactionMetricsInteractor
from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    storage = SQLStorage()
    presenter = DataPresenter()

    post_id = kwargs['id']

    get_reaction_metrics_for_post = GetReactionMetricsInteractor(
        storage, presenter
    )
    reaction_metrics_response = \
        get_reaction_metrics_for_post.get_reaction_metrics(post_id)

    import json
    response_data = json.dumps(reaction_metrics_response)

    return HttpResponse(response_data, status=200)
