from unittest.mock import Mock, create_autospec

from fb_post_v2.interactors.create_post_interactor import CreatePostInteractor
from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import Storage, PostIdDTO

NEW_POST = (
    1,
    "New Post from admin."
)


def test_create_post_on_valid_input_get_post_id():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)
    post_id_dto = PostIdDTO(post_id=1)
    post_id_dict = {'post_id': 1}

    storage_mock.create_post_and_get_post_id.return_value = post_id_dto
    presenter_mock.get_create_post_response.return_value = post_id_dict

    create_post_interator = CreatePostInteractor(storage_mock, presenter_mock)
    result = create_post_interator.create_post(*NEW_POST)

    storage_mock.create_post_and_get_post_id.assert_called_once_with(*NEW_POST)
    presenter_mock.get_create_post_response.assert_called_once_with(post_id_dto)
    assert result == post_id_dict
