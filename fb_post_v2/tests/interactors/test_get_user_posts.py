from unittest.mock import create_autospec
from fb_post_v2.interactors.get_user_posts_interactor import \
    GetUserPostsInteractor
from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import Storage, PostDTO, UserDTO, \
    CommentDTO, ReactionDTO, PostCompleteDetailsDTO


def test_get_user_posts_on_invalid_user_id_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    user_id = 2
    storage_mock.is_user_exists.return_value = False

    get_user_posts_interactor = GetUserPostsInteractor(
        storage_mock, presenter_mock
    )
    get_user_posts_interactor.get_user_posts(user_id)

    storage_mock.is_user_exists.assert_called_once_with(user_id)
    presenter_mock.raise_invalid_user_id_exception.assert_called_once()


def test_get_user_posts_on_valid_input_get_response():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    user_id = 2
    storage_mock.is_user_exists.return_value = True
    post_dto = PostDTO(post_id=2)
    user_dto = UserDTO(user_id=1)
    comment_dto = CommentDTO(comment_id=2)
    reaction_dto = ReactionDTO(reaction_type="LIKE")

    user_posts_dto = PostCompleteDetailsDTO(
        post=[post_dto], users=[user_dto], comments=[comment_dto],
        reactions=[reaction_dto])
    user_posts_complete_dict = [{"post_id": 2, "user_id": 1,
                                 "comments": [{"comment_id": 2}],
                                 "reactions": [{"reaction_type": "LIKE"}]
                                 }]

    storage_mock.get_user_posts.return_value = [user_posts_dto]
    presenter_mock.get_user_posts_response.return_value = user_posts_complete_dict

    get_user_posts_interactor = GetUserPostsInteractor(
        storage_mock, presenter_mock)
    result = get_user_posts_interactor.get_user_posts(user_id)

    storage_mock.is_user_exists.assert_called_once_with(user_id)
    storage_mock.get_user_posts.assert_called_once_with(user_id)
    presenter_mock.get_user_posts_response.assert_called_once_with(
        [user_posts_dto])
    assert result == user_posts_complete_dict
