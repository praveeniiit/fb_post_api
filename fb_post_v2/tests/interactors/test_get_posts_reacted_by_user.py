from unittest.mock import create_autospec

from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import Storage, PostDTO, PostIdDTO
from fb_post_v2.interactors.get_posts_reacted_by_user_interactor import \
    GetPostsReactedByUserInteractor


def test_get_posts_reacted_by_user_on_valid_user_id_return_response():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    user_id = 2
    reacted_post_dto = [PostIdDTO(
        post_id=2
    )]
    reacted_post_dict = [
        {
            'post_id': 1
        }
    ]

    storage_mock.is_user_exists.return_value = True
    storage_mock.get_posts_reacted_by_user.return_value = reacted_post_dto
    presenter_mock.get_posts_reacted_by_user.return_value = reacted_post_dict

    get_posts_reacted_by_user_interactor = GetPostsReactedByUserInteractor(
        storage_mock, presenter_mock
    )
    result = get_posts_reacted_by_user_interactor.get_posts_reacted_by_user(
        user_id)

    storage_mock.is_user_exists.assert_called_once_with(user_id)
    storage_mock.get_posts_reacted_by_user.assert_called_once_with(user_id)
    presenter_mock.get_posts_reacted_by_user.assert_called_once_with(
        reacted_post_dto
    )
    assert result is reacted_post_dict


def test_get_posts_reacted_by_user_on_invalid_user_id_raise_exception():
    presenter_mock = create_autospec(Presenter)
    storage_mock = create_autospec(Storage)

    user_id = 2
    storage_mock.is_user_exists.return_value = False

    get_posts_reacted_by_user_interactor = GetPostsReactedByUserInteractor(
        storage_mock, presenter_mock
    )
    get_posts_reacted_by_user_interactor.get_posts_reacted_by_user(user_id)

    storage_mock.is_user_exists.assert_called_once_with(user_id)
    presenter_mock.raise_invalid_user_id_exception.assert_called_once()
