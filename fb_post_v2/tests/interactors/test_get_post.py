from unittest.mock import create_autospec

from fb_post_v2.interactors.get_post_interactor import GetPostInteractor
from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import UserDTO, CommentDTO, \
    ReactionDTO, PostCompleteDetailsDTO, Storage, PostDTO


def test_get_post_on_invalid_post_id_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id = 2
    storage_mock.is_post_exists.return_value = False

    get_post_interactor = GetPostInteractor(storage_mock, presenter_mock)
    get_post_interactor.get_post(post_id=post_id)

    storage_mock.is_post_exists.assert_called_once_with(post_id)
    presenter_mock.raise_invalid_post_id_exception.assert_called_once()


def test_get_post_on_valid_input_return_response():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_dto = PostDTO(post_id=2)
    user_dto = UserDTO(user_id=1)
    comment_dto = CommentDTO(comment_id=2)
    reaction_dto = ReactionDTO(reaction_type="LIKE")

    post_complete_dto = PostCompleteDetailsDTO(
        post=post_dto, users=[user_dto], comments=[comment_dto],
        reactions=[reaction_dto]
    )
    post_complete_dict = {
        "post_id": 2,
        "user_id": 1,
        "comments": [
            {
                "comment_id": 2
            }
        ],
        "reactions": [
            {
                "reaction_type": "LIKE"
            }
        ]
    }

    post_id = 2
    storage_mock.is_post_exists.return_value = True
    storage_mock.get_post.return_value = post_complete_dto
    presenter_mock.get_post_response.return_value = post_complete_dict

    get_post_interactor = GetPostInteractor(storage_mock, presenter_mock)
    result = get_post_interactor.get_post(post_id=post_id)

    storage_mock.is_post_exists.assert_called_once_with(post_id)
    storage_mock.get_post.assert_called_once_with(post_id)
    presenter_mock.get_post_response.assert_called_once_with(post_complete_dto)
    assert result is post_complete_dict
