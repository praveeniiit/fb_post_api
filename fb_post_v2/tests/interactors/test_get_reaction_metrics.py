from unittest.mock import create_autospec

from fb_post_v2.constants.reactions import ReactionType
from fb_post_v2.interactors.get_reaction_metrics_interactor import \
    GetReactionMetricsInteractor
from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import Storage, ReactionMetricDTO


def test_get_reaction_metrics_on_invalid_post_id_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id = 2
    storage_mock.is_post_exists.return_value = False

    get_reaction_metrics_interactor = GetReactionMetricsInteractor(
        storage_mock, presenter_mock
    )
    get_reaction_metrics_interactor.get_reaction_metrics(post_id=post_id)

    storage_mock.is_post_exists.assert_called_once_with(post_id)
    presenter_mock.raise_invalid_post_id_exception.assert_called_once()


def test_get_reaction_metrics_on_valid_post_id_return_response():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id = 2
    reaction_metric_dto = ReactionMetricDTO(
        reaction_type=ReactionType.LIKE.value,
        count=3
    )
    reaction_metric_list = {
        "reaction_type": ReactionType.LIKE.value,
        "count": 3
    }

    storage_mock.is_post_exists.return_value = True
    storage_mock.get_reaction_metrics.return_value = reaction_metric_dto
    presenter_mock.get_reaction_metrics_response.return_value = \
        reaction_metric_list

    get_reaction_metrics_interactor = GetReactionMetricsInteractor(
        storage_mock, presenter_mock
    )
    result = get_reaction_metrics_interactor.get_reaction_metrics(
        post_id=post_id)

    storage_mock.is_post_exists.assert_called_once_with(post_id)
    storage_mock.get_reaction_metrics.assert_called_once_with(post_id)
    presenter_mock.get_reaction_metrics_response.assert_called_once_with(
        reaction_metric_dto)
    assert result == reaction_metric_list
