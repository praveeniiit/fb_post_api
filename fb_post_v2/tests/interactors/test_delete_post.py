from unittest.mock import create_autospec

from fb_post_v2.interactors.storages.storage import Storage
from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.delete_post_interactor import DeletePostInteractor


def test_delete_post_on_invalid_post_id_return_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id = 2
    storage_mock.is_post_exists.return_value = False

    delete_post_interactor = DeletePostInteractor(storage_mock, presenter_mock)
    delete_post_interactor.delete_post(post_id=post_id)

    storage_mock.is_post_exists.assert_called_once_with(post_id)
    presenter_mock.raise_invalid_post_id_exception.assert_called_once()


def test_delete_post_on_valid_input_delete_post():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id = 2
    storage_mock.is_post_exists.return_value = True

    delete_post_interactor = DeletePostInteractor(storage_mock, presenter_mock)
    delete_post_interactor.delete_post(post_id=post_id)

    storage_mock.is_post_exists.assert_called_once_with(post_id)
    storage_mock.delete_post.assert_called_once_with(post_id)
