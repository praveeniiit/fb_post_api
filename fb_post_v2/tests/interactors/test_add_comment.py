from unittest.mock import create_autospec

from fb_post_v2.interactors.add_comment_interactor import AddCommentInteractor
from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import Storage, CommentIdDTO

COMMENT_DATA = {
    'user_id': 1,
    'post_id': 1,
    'comment_description': 'This is my comment.'
}
INVALID_POST_ID = {
    "response": "INVALID_POST_ID",
    "status_code": 404,
}


def test_add_comment_on_valid_input_get_comment_id():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    comment_id_dto = CommentIdDTO(comment_id=1)
    comment_id_dict = {'comment_id': 1}

    storage_mock.is_post_exists.return_value = True
    storage_mock.add_comment_and_get_comment_id.return_value = comment_id_dto
    presenter_mock.get_add_comment_response.return_value = comment_id_dict

    add_comment_interactor = AddCommentInteractor(storage_mock, presenter_mock)
    result = add_comment_interactor.add_comment(
        COMMENT_DATA['user_id'], COMMENT_DATA['post_id'],
        COMMENT_DATA['comment_description']
    )

    storage_mock.add_comment_and_get_comment_id.assert_called_once_with(
        COMMENT_DATA['user_id'], COMMENT_DATA['post_id'],
        COMMENT_DATA['comment_description']
    )
    storage_mock.is_post_exists.assert_called_once_with(
        post_id=COMMENT_DATA['post_id']
    )
    presenter_mock.get_add_comment_response.assert_called_once_with(
        comment_id_dto
    )
    assert result == comment_id_dict


def test_add_comment_on_invalid_input_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock.is_post_exists.return_value = False

    add_comment_interactor = AddCommentInteractor(storage_mock, presenter_mock)
    add_comment_interactor.add_comment(
        COMMENT_DATA['user_id'], COMMENT_DATA['post_id'],
        COMMENT_DATA['comment_description']
    )

    storage_mock.is_post_exists.assert_called_once_with(
        post_id=COMMENT_DATA['post_id']
    )
    presenter_mock.raise_invalid_post_id_exception.assert_called_once()
