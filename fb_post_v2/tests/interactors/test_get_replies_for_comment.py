from unittest.mock import create_autospec

from fb_post_v2.interactors.get_replies_for_comment_interactor import GetRepliesForCommentInteractor
from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import Storage, UserDTO, CommentDTO, ReplyDTO


def test_get_replies_for_comment_on_invalid_comment_id_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    comment_id = 2
    storage_mock._is_comment_do_not_exist.return_value = False

    replies_for_comment_interactor = GetRepliesForCommentInteractor(
        storage_mock, presenter_mock
    )
    replies_for_comment_interactor.get_replies_for_comment(comment_id)

    storage_mock._is_comment_do_not_exist.assert_called_once_with(comment_id)
    presenter_mock.raise_invalid_comment_id_exception.assert_called_once_with()


def test_get_replies_for_comment_on_valid_comment_id_get_response():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    comment_id = 2
    commenter_dto = UserDTO(user_id=2)
    comment_dto = CommentDTO(comment_id=3)
    replies_dto = ReplyDTO(comment_id=comment_dto, commenter=commenter_dto)
    replies_dict = [
        {
            'commenter': {
                'commenter_id': 2,
                'name': 'praveen',
            },
            'comment_id': 2,
            'commented_at': '2019-08-29 00:00:01',
            'comment_description': 'New reply for comment.'
        }
    ]

    storage_mock._is_comment_do_not_exist.return_value = True
    storage_mock.get_replies_for_comment.return_value = replies_dto
    presenter_mock.get_replies_for_comment.return_value = replies_dict

    replies_for_comment_interactor = GetRepliesForCommentInteractor(
        storage_mock, presenter_mock
    )
    result = replies_for_comment_interactor.get_replies_for_comment(comment_id)

    storage_mock._is_comment_do_not_exist.assert_called_once_with(comment_id)
    storage_mock.get_replies_for_comment.assert_called_once_with(comment_id)
    presenter_mock.get_replies_for_comment.assert_called_once_with(replies_dto)
    assert result == replies_dict
