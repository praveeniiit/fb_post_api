from mock import create_autospec

from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.reply_to_comment_interactor import ReplyToCommentInteractor
from fb_post_v2.interactors.storages.storage import Storage, CommentIdDTO

NEW_COMMENT_REPLY = {
    'user_id': 4,
    'comment_id': 3,
    'description': 'This is my reply comment.'
}
INVALID_COMMENT_ID = {
    "response": "INVALID_COMMENT_ID",
    "status_code": 404,
}


def test_reply_to_comment_on_valid_comment_id_get_comment_id():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    comment_id_dto = CommentIdDTO(comment_id=10)
    comment_id_dict = {'comment_id': 2}

    storage_mock._is_comment_do_not_exist.return_value = False
    storage_mock.is_comment_have_parent.return_value = False
    storage_mock.reply_to_comment_and_get_reply_id.return_value = comment_id_dto
    presenter_mock.get_reply_to_comment_response.return_value = comment_id_dict

    reply_to_comment_interactor = ReplyToCommentInteractor(
        storage_mock, presenter_mock)
    result = reply_to_comment_interactor.reply_to_comment(
        user_id=NEW_COMMENT_REPLY['user_id'],
        comment_id=NEW_COMMENT_REPLY['comment_id'],
        description=NEW_COMMENT_REPLY['description']
    )

    storage_mock._is_comment_do_not_exist.assert_called_once_with(
        NEW_COMMENT_REPLY['comment_id'])
    storage_mock.is_comment_have_parent.assert_called_once_with(
        NEW_COMMENT_REPLY['comment_id']
    )
    storage_mock.reply_to_comment_and_get_reply_id.assert_called_once_with(
        user_id=NEW_COMMENT_REPLY['user_id'],
        comment_id=NEW_COMMENT_REPLY['comment_id'],
        description=NEW_COMMENT_REPLY['description']
    )
    presenter_mock.get_reply_to_comment_response.assert_called_once_with(
        comment_id_dto)
    assert result == comment_id_dict


def test_reply_to_comment_on_replying_to_reply():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    comment_id_dto = CommentIdDTO(comment_id=2)
    comment_id_dict = {'comment_id': 2}
    comment_parent_id =4

    storage_mock._is_comment_do_not_exist.return_value = False
    storage_mock.is_comment_have_parent.return_value = True
    storage_mock.get_comment_parent_id.return_value = comment_parent_id
    storage_mock.reply_to_comment_and_get_reply_id.return_value = comment_id_dto
    presenter_mock.get_reply_to_comment_response.return_value = comment_id_dict

    reply_to_comment_interactor = ReplyToCommentInteractor(
        storage_mock, presenter_mock)
    result = reply_to_comment_interactor.reply_to_comment(
        user_id=NEW_COMMENT_REPLY['user_id'],
        comment_id=NEW_COMMENT_REPLY['comment_id'],
        description=NEW_COMMENT_REPLY['description']
    )

    storage_mock._is_comment_do_not_exist.assert_called_once_with(
        NEW_COMMENT_REPLY['comment_id'])
    storage_mock.is_comment_have_parent.assert_called_once_with(
        NEW_COMMENT_REPLY['comment_id'])
    storage_mock.get_comment_parent_id.assert_called_once_with\
        (NEW_COMMENT_REPLY['comment_id'])
    storage_mock.reply_to_comment_and_get_reply_id.assert_called_once_with(
        comment_id=comment_parent_id, user_id=NEW_COMMENT_REPLY['user_id'],
        description=NEW_COMMENT_REPLY['description']
    )
    presenter_mock.get_reply_to_comment_response.assert_called_once_with(
        comment_id_dto)
    assert result == comment_id_dict


def test_reply_to_comment_on_invalid_comment_id_raise_error():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock._is_comment_do_not_exist.return_value = False

    reply_to_comment_interactor = ReplyToCommentInteractor(
        storage_mock, presenter_mock)
    result = reply_to_comment_interactor.reply_to_comment(
        user_id=NEW_COMMENT_REPLY['user_id'],
        comment_id=NEW_COMMENT_REPLY['comment_id'],
        description=NEW_COMMENT_REPLY['description']
    )

    storage_mock._is_comment_do_not_exist.assert_called_once_with(
        NEW_COMMENT_REPLY['comment_id'])
    storage_mock.is_comment_have_parent.assert_called_once_with(
        NEW_COMMENT_REPLY['comment_id'])
    presenter_mock.raise_invalid_comment_id_exception.assert_called_once()
