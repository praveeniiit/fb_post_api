from unittest.mock import create_autospec

from fb_post_v2.interactors.get_posts_with_more_positive_reactions_interactor import \
    GetPostsWithMorePositiveReactionsInteractor
from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import Storage, PostIdDTO


def test_get_post_with_more_positive_reaction_than_negative():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    positive_posts_dto = PostIdDTO(post_id=1)
    positive_post_ids = [2, 5, 6]

    storage_mock.get_posts_with_more_positive_reactions.return_value = \
        [positive_posts_dto]
    presenter_mock.get_posts_with_more_positive_reactions_response.return_value = \
        positive_post_ids

    positive_posts_interactor = GetPostsWithMorePositiveReactionsInteractor(
        storage_mock, presenter_mock
    )
    result = positive_posts_interactor.posts_with_more_positive_reactions()

    storage_mock.get_posts_with_more_positive_reactions.assert_called_once()
    presenter_mock.get_posts_with_more_positive_reactions_response.assert_called_once_with(
        [positive_posts_dto]
    )
    assert result is positive_post_ids
