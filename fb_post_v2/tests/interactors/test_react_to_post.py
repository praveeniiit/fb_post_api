from unittest.mock import create_autospec

from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import Storage
from fb_post_v2.interactors.react_to_post_interactor import ReactToPostInteractor

NEW_REACTION_DATA = {
    'user_id': 1,
    'post_id': 2,
    'reaction_type': 'LIKE'
}
INVALID_REACTION_DATA = {
    'user_id': 1,
    'post_id': 2,
    'reaction_type': 'LIKE124'
}
UPDATE_REACTION = "LOVE"
INVALID_POST_EXCEPTION = {
    "response": 'POST_NOT_FOUND',
    'res_status': "Unable to find your post"
}
INVALID_REACTION_EXCEPTION = {
    "reaction": "REACTION_NOT_ALLOWED",
    "res_status": "Unable to store your reaction."
}


def test_react_to_post_on_valid_input():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock.is_post_exists.return_value = True
    storage_mock.is_post_reaction_exists.return_value = False

    react_to_post_interactor = ReactToPostInteractor(
        storage_mock, presenter_mock)
    react_to_post_interactor.react_to_post(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['post_id'],
        NEW_REACTION_DATA['reaction_type']
    )

    storage_mock.is_post_exists.assert_called_once_with(
        NEW_REACTION_DATA['post_id'])
    storage_mock.is_post_reaction_exists.assert_called_once_with(
        user_id=NEW_REACTION_DATA['user_id'], post_id=NEW_REACTION_DATA['post_id']
    )
    storage_mock.react_to_post_on_correct_input_create_reaction.assert_called_once_with(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['post_id'],
        NEW_REACTION_DATA['reaction_type']
    )


def test_react_to_post_on_invalid_post_id_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock.is_post_exists.return_value = False
    storage_mock.is_post_reaction_exists.return_value = False

    react_to_post_interactor = ReactToPostInteractor(
        storage_mock, presenter_mock)
    react_to_post_interactor.react_to_post(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['post_id'],
        NEW_REACTION_DATA['reaction_type']
    )

    storage_mock.is_post_exists.assert_called_once_with(
        NEW_REACTION_DATA['post_id'])
    storage_mock.is_post_reaction_exists.assert_called_once_with(
        user_id=NEW_REACTION_DATA['user_id'],
        post_id=NEW_REACTION_DATA['post_id']
    )
    presenter_mock.raise_invalid_post_id_exception.assert_called_once()


def test_react_to_post_on_invalid_reaction_type_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock.is_post_exists.return_value = True
    storage_mock.is_post_reaction_exists.return_value = False

    react_to_post_interactor = ReactToPostInteractor(
        storage_mock, presenter_mock)
    react_to_post_interactor.react_to_post(
        INVALID_REACTION_DATA['user_id'], INVALID_REACTION_DATA['post_id'],
        INVALID_REACTION_DATA['reaction_type']
    )

    storage_mock.is_post_exists.assert_called_once_with(
        INVALID_REACTION_DATA['post_id'])
    storage_mock.is_post_reaction_exists.assert_called_once_with(
        user_id=INVALID_REACTION_DATA['user_id'],
        post_id=INVALID_REACTION_DATA['post_id']
    )
    presenter_mock.raise_invalid_reaction_exception.assert_called_once()


def test_react_to_post_on_update():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock.is_post_exists.return_value = True
    storage_mock.is_post_reaction_exists.return_value = True
    storage_mock.get_post_reaction.return_value = UPDATE_REACTION

    react_to_post_interactor = ReactToPostInteractor(
        storage_mock, presenter_mock)
    react_to_post_interactor.react_to_post(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['post_id'],
        NEW_REACTION_DATA['reaction_type']
    )

    storage_mock.is_post_exists.assert_called_once_with(
        NEW_REACTION_DATA['post_id'])
    storage_mock.is_post_reaction_exists.assert_called_once_with(
        user_id=NEW_REACTION_DATA['user_id'],
        post_id=NEW_REACTION_DATA['post_id']
    )
    storage_mock.get_post_reaction.assert_called_once_with(
        user_id=NEW_REACTION_DATA['user_id'],
        post_id=NEW_REACTION_DATA['post_id']
    )
    storage_mock.update_post_reaction.assert_called_once_with(
        user_id=NEW_REACTION_DATA['user_id'],
        post_id=NEW_REACTION_DATA['post_id'],
        reaction_type=NEW_REACTION_DATA['reaction_type']
    )


def test_react_to_post_on_delete():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock.is_post_exists.return_value = True
    storage_mock.is_post_reaction_exists.return_value = True
    storage_mock.get_post_reaction.return_value = NEW_REACTION_DATA['reaction_type']

    react_to_post_interactor = ReactToPostInteractor(
        storage_mock, presenter_mock)
    react_to_post_interactor.react_to_post(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['post_id'],
        NEW_REACTION_DATA['reaction_type']
    )

    storage_mock.is_post_exists.assert_called_once_with(
        NEW_REACTION_DATA['post_id'])
    storage_mock.is_post_reaction_exists.assert_called_once_with(
        user_id=NEW_REACTION_DATA['user_id'],
        post_id=NEW_REACTION_DATA['post_id']
    )
    storage_mock.get_post_reaction.assert_called_once_with(
        user_id=NEW_REACTION_DATA['user_id'],
        post_id=NEW_REACTION_DATA['post_id']
    )
    storage_mock.delete_post_reaction.assert_called_once_with(
        user_id=NEW_REACTION_DATA['user_id'], post_id=NEW_REACTION_DATA['post_id']
    )
