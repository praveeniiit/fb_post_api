from mock import create_autospec

from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.react_to_comment_interactor import\
    ReactToCommentInteractor
from fb_post_v2.interactors.storages.storage import Storage

NEW_REACTION_DATA = {
    'user_id': 1, 'comment_id': 2, 'reaction_type': 'LIKE'
}
INVALID_REACTION_DATA = {
    'user_id': 1, 'comment_id': 2, 'reaction_type': 'LIKE143'
}
UPDATE_REACTION = "LOVE"
INVALID_COMMENT_EXCEPTION = {
    "response": "COMMENT_NOT_FOUND",
    'res_status': "Unable to find your comment"
}
INVALID_REACTION_EXCEPTION = {
    "reaction": "REACTION_NOT_ALLOWED",
    "res_status": "Unable to store your reaction."
}


def test_react_to_comment_on_valid_input():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock._is_comment_do_not_exist.return_value = True
    storage_mock.is_comment_reaction_exists.return_value = False

    react_to_comment_interactor = ReactToCommentInteractor(
        storage_mock, presenter_mock
    )
    react_to_comment_interactor.react_to_comment(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['comment_id'],
        NEW_REACTION_DATA['reaction_type']
    )

    storage_mock._is_comment_do_not_exist.assert_called_once_with(
        NEW_REACTION_DATA['comment_id']
    )
    storage_mock.is_comment_reaction_exists.assert_called_once_with(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['comment_id']
    )
    storage_mock.react_to_comment_on_correct_input_create_reaction.\
        assert_called_once_with(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['comment_id'],
        NEW_REACTION_DATA['reaction_type']
    )


def test_react_to_comment_on_invalid_comment_id_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock._is_comment_do_not_exist.return_value = False
    storage_mock.is_comment_reaction_exists.return_value = False

    react_to_comment_interactor = ReactToCommentInteractor(
        storage_mock, presenter_mock
    )
    react_to_comment_interactor.react_to_comment(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['comment_id'],
        NEW_REACTION_DATA['reaction_type']
    )

    storage_mock._is_comment_do_not_exist.assert_called_once_with(
        NEW_REACTION_DATA['comment_id'])
    storage_mock.is_comment_reaction_exists.assert_called_once_with(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['comment_id']
    )
    presenter_mock.raise_invalid_comment_id_exception.assert_called_once()


def test_react_to_comment_on_invalid_reaction_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock._is_comment_do_not_exist.return_value = True
    storage_mock.is_comment_reaction_exists.return_value = False

    react_to_comment_interactor = ReactToCommentInteractor(
        storage_mock, presenter_mock)
    react_to_comment_interactor.react_to_comment(
        INVALID_REACTION_DATA['user_id'], INVALID_REACTION_DATA['comment_id'],
        INVALID_REACTION_DATA['reaction_type']
    )

    storage_mock._is_comment_do_not_exist.assert_called_once_with(
        INVALID_REACTION_DATA['comment_id'])
    storage_mock.is_comment_reaction_exists.assert_called_once_with(
        INVALID_REACTION_DATA['user_id'], INVALID_REACTION_DATA['comment_id']
    )
    presenter_mock.raise_invalid_reaction_exception.assert_called_once()


def test_react_to_comment_on_update():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock._is_comment_do_not_exist.return_value = True
    storage_mock.is_comment_reaction_exists.return_value = True
    storage_mock.get_comment_reaction.return_value = UPDATE_REACTION

    react_to_comment_interactor = ReactToCommentInteractor(
        storage_mock, presenter_mock)
    react_to_comment_interactor.react_to_comment(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['comment_id'],
        NEW_REACTION_DATA['reaction_type']
    )

    storage_mock._is_comment_do_not_exist.assert_called_once_with(
        NEW_REACTION_DATA['comment_id'])
    storage_mock.is_comment_reaction_exists.assert_called_once_with(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['comment_id']
    )
    storage_mock.get_comment_reaction.assert_called_once_with(
        NEW_REACTION_DATA['comment_id'], NEW_REACTION_DATA['user_id']
    )
    storage_mock.update_comment_reaction.assert_called_once_with(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['comment_id'],
        NEW_REACTION_DATA['reaction_type']
    )


def test_react_to_comment_on_delete():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    storage_mock._is_comment_do_not_exist.return_value = True
    storage_mock.is_comment_reaction_exists.return_value = True
    storage_mock.get_comment_reaction.return_value = NEW_REACTION_DATA['reaction_type']

    react_to_comment_interactor = ReactToCommentInteractor(
        storage_mock, presenter_mock)
    react_to_comment_interactor.react_to_comment(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['comment_id'],
        NEW_REACTION_DATA['reaction_type']
    )

    storage_mock._is_comment_do_not_exist.assert_called_once_with(
        NEW_REACTION_DATA['comment_id'])
    storage_mock.is_comment_reaction_exists.assert_called_once_with(
        NEW_REACTION_DATA['user_id'], NEW_REACTION_DATA['comment_id']
    )
    storage_mock.get_comment_reaction.assert_called_once_with(
        NEW_REACTION_DATA['comment_id'], NEW_REACTION_DATA['user_id']
    )
    storage_mock.delete_comment_reaction.assert_called_once_with(
        NEW_REACTION_DATA['comment_id'], NEW_REACTION_DATA['user_id']
    )
