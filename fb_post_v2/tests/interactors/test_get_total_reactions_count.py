from unittest.mock import create_autospec

from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import Storage
from fb_post_v2.interactors.get_total_reaction_count import \
    GetTotalReactionsCount


def test_get_total_reactions_count():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    reaction_count = 40
    total_reaction_dict = {
        'reactions_count': reaction_count
    }

    storage_mock.get_total_reactions_count.return_value = reaction_count
    presenter_mock.get_total_reactions_count_response.return_value = \
        total_reaction_dict

    get_total_reactions_count_interactor = GetTotalReactionsCount(
        storage_mock, presenter_mock)
    result = get_total_reactions_count_interactor.get_total_reaction_count()

    storage_mock.get_total_reactions_count.assert_called_once()
    presenter_mock.get_total_reactions_count_response.assert_called_once_with(
        reaction_count)
    assert result == total_reaction_dict
