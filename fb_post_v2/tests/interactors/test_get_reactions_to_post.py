from unittest.mock import create_autospec

from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import Storage, UserDTO, \
    ReactionDTO, ReactionTypeDTO
from fb_post_v2.interactors.get_reactions_to_post_interactor import \
    GetReactionsToPostInteractor


def test_get_reactions_to_post_on_invalid_post_id_raise_exception():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id = 2
    storage_mock.is_post_exists.return_value = False

    get_reactions_to_post_interactor = GetReactionsToPostInteractor(
        storage_mock, presenter_mock
    )
    get_reactions_to_post_interactor.get_reaction_to_post(post_id)

    storage_mock.is_post_exists.assert_called_once_with(post_id)
    presenter_mock.raise_invalid_post_id_exception.assert_called_once()


def test_get_reactions_to_post_on_valid_post_id_return_response():
    storage_mock = create_autospec(Storage)
    presenter_mock = create_autospec(Presenter)

    post_id = 2
    user_dto = UserDTO(user_id=1)
    reaction_dto = ReactionTypeDTO(reaction_type= "LIKE")
    reactions_to_post_dto = ReactionDTO(
        user=user_dto, reaction=reaction_dto
    )
    reactions_to_post_dict = [
        {"user_id": 1, "name": "iB Cricket", "profile_pic": "",
         "reaction": "LIKE"}
    ]

    storage_mock.is_post_exists.return_value = True
    storage_mock.get_reactions_to_post.return_value = reactions_to_post_dto
    presenter_mock.get_reactions_to_post.return_value = reactions_to_post_dict

    get_reactions_to_post_interactor = GetReactionsToPostInteractor(
        storage_mock, presenter_mock
    )
    result = get_reactions_to_post_interactor.get_reaction_to_post(post_id)

    storage_mock.is_post_exists.assert_called_once_with(post_id)
    storage_mock.get_reactions_to_post.assert_called_once_with(post_id)
    presenter_mock.get_reactions_to_post.assert_called_once_with(
        reactions_to_post_dto)
    assert result == reactions_to_post_dict
