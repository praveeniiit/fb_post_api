import pytest
from django.db.models import Count, Prefetch, F, Q

from fb_post_v2.constants.reactions import ReactionType, POSITIVE_REACTIONS_LIST, \
    NEGATIVE_REACTIONS_LIST
from fb_post_v2.models import User, Post, Comment, Reaction


@pytest.fixture
def user_data():
    user = User.objects.create_user(
        username="Praveen", password="rgukt123", email="prvn@gmail.com"
    )
    return user


@pytest.fixture
def post_data(user_data):
    post = Post.objects.create(description="My new post from testing!",
                               user_id=user_data.id)
    return post


@pytest.fixture
def comment_data(user_data, post_data):
    comment = Comment.objects.create(
        description="Comment from admin in db Testing!", user_id=user_data.id,
        post_id=post_data.id)
    return comment


@pytest.fixture
def post_reaction_data(user_data, post_data):
    reaction = Reaction.objects.create(user=user_data, post=post_data,
                                       reaction_type="WOW")
    return reaction


@pytest.fixture
def comment_reaction_data(user_data, comment_data):
    reaction = Reaction.objects.create(user=user_data, comment=comment_data,
                                       reaction_type="LIKE")
    return reaction


@pytest.fixture
def reaction_metrics(post_reaction_data, post_data):
    data = Reaction.objects.filter(post_id=post_data.id).values(
        "reaction_type").annotate(count=Count('reaction_type'))
    return list(data)


@pytest.fixture
def comment_replies(comment_data, user_data):
    for i in range(10):
        reply_text = "Welcome sample text %d" % i
        Comment.objects.create(
            description=reply_text, parent_id=comment_data.id,
            user_id=user_data.id
        )
    comment_replies = Comment.objects.get(
        id=comment_data.id).replies.select_related("user")
    return comment_replies


@pytest.fixture
def user_posts(user_data):
    for i in range(10):
        post_description = "Welcome sample text %d" % i
        Post.objects.create(user=user_data, description=post_description)
    return Post.objects.filter(user=user_data)


@pytest.fixture
def post_reactions(post_data, user_data):
    for i in range(10):
        user = User.objects.create(
            username="Praveen %d" % i, profile_pic_url="none")
        Reaction.objects.create(reaction_type=ReactionType.LOVE.value,
                                post_id=post_data.id, user=user)
    return Reaction.objects.filter(post_id=post_data.id)


@pytest.fixture
def user_reacted_posts(user_data):
    for i in range(10):
        post = Post.objects.create(
            description="Post number %d" % i, user_id=user_data.id)
        Reaction.objects.create(reaction_type=ReactionType.LOVE.value,
                                post_id=post.id, user_id=user_data.id)
    return Post.objects.filter(reactions__user_id=user_data.id).values('id')


@pytest.fixture
def posts_with_more_positive_reactions(user_data, post_data):
    user = User.objects.create(name="Iron Bruce", profile_pic_url="None")
    Reaction.objects.create(
        user=user, post=post_data, reaction_type=ReactionType.LOVE.value)
    Reaction.objects.create(
        user=user_data, post=post_data, reaction_type=ReactionType.LIKE.value)

    positive_exp = Count(
        'reactions',
        filter=Q(reactions__reaction_type__in=POSITIVE_REACTIONS_LIST))
    negative_exp = Count(
        'reactions',
        filter=Q(reactions__reaction_type__in=NEGATIVE_REACTIONS_LIST))
    posts = Post.objects.annotate(
        positive_count=positive_exp, negative_count=negative_exp).filter(
        positive_count__gt=F('negative_count')).values('id')
    return posts


@pytest.fixture
def get_post_with_all(
        user_data, post_data, comment_data, post_reaction_data,
        comment_reaction_data, comment_replies):
    for reply in comment_replies:
        reply.reactions.create(user=user_data)
    posts = Post.objects.filter(user_id=post_data.id).select_related(
        'user').prefetch_related(
        Prefetch("reactions", to_attr="post_reactions"),
        Prefetch("comments", queryset=Comment.objects.select_related('user'),
                 to_attr="post_comments"),
        Prefetch("post_comments__reactions", to_attr="comment_reactions"),
        Prefetch("post_comments__replies",
            queryset=Comment.objects.select_related("user").prefetch_related(
            Prefetch('reactions', to_attr="reply_reactions")),
            to_attr="comment_replies"),
    )

    return posts[0]


@pytest.fixture
def populate_get_post():

    user = User.objects.create_user(
        username="Praveen", name='Praveen',
        password="123",profile_pic_url="None")
    post = Post.objects.create(
        user_id=user.id, description="This is my first post."
    )
    Reaction.objects.create(
        post_id=post.id, user_id=user.id, reaction_type="LIKE"
    )
    comment = Comment.objects.create(
        description="Waste comment", post_id=post.id, user_id=user.id
    )
    reply = Comment.objects.create(
        description="Reply comment", parent_id=comment.id, user_id=user.id
    )
    Reaction.objects.create(
        comment_id=comment.id, user_id=user.id, reaction_type="LIKE"
    )
    Reaction.objects.create(
        comment_id=reply.id, user_id=user.id, reaction_type="LIKE"
    )
