import pytest

from fb_post_v2.constants.reactions import ReactionType
from fb_post_v2.models import Post, Comment, Reaction
from fb_post_v2.storages.sql_storage import SQLStorage

from fb_post_v2.constants.reactions import (ReactionType,
                                            POSITIVE_REACTIONS_LIST,
                                            NEGATIVE_REACTIONS_LIST)
from fb_post_v2.interactors.storages.storage import (Storage,
                                                     CommentIdDTO,
                                                     ReactionTypeDTO, ReplyDTO,
                                                     ReactionDTO, PostDTO,
                                                     PostIdDTO,
                                                     PostCompleteDetailsDTO,
                                                     ReactionMetricDTO, UserDTO,
                                                     CommentDTO,
                                                     ReactionWithUserDTO)


def populate_get_post_dto():
    post = [
        PostDTO(
            post_id=1, user_id=1, post_content="This is my first post.",
            posted_at="2019-09-08 12:12:23"
        )
    ]
    users = []
    comments = []
    reactions = [
        ReactionWithUserDTO(
            post_id=1, user_id=1, reaction_type="LIKE"
        )
    ]
    user = UserDTO(
        user_id=1, name="Praveen", profile_pic_url="None"
    )
    users.append(user)
    comment = CommentDTO(
        comment_id=1, comment_content="Waste comment",
        commented_at="2019-09-08 12:12:23", user_id=1
    )
    reaction = ReactionWithUserDTO(
        comment_id=1, user_id=1, reaction_type="LIKE"
    )
    reactions.append(reaction)
    comments.append(comment)
    reply = CommentDTO(
        comment_id=2, parent_id=1, comment_content="Reply",
        commented_at="2019-09-08 12:12:23"
    )
    reaction = ReactionWithUserDTO(
        comment_id=2, user_id=1, reaction_type="LIKE"
    )
    reactions.append(reaction)
    comments.append(reply)
    return PostCompleteDetailsDTO(
        post=post, users=users, comments=comments, reactions=reactions
    )


class TestStorage:

    @pytest.mark.django_db
    def test_get_posts_reacted_by_user(self, user_reacted_posts, user_data):
        storage = SQLStorage()
        user_posts_dto_list = storage.get_posts_reacted_by_user(
            user_id=user_data.id)
        assert len(user_posts_dto_list) == len(user_reacted_posts)
        for (post_dto, post_fixture) in zip(
                user_posts_dto_list, user_reacted_posts):
            assert post_dto.post_id == post_fixture ['id']

    @pytest.mark.django_db
    def test_get_posts_with_more_positive_reactions(
            self, posts_with_more_positive_reactions):
        storage = SQLStorage()
        posts_dto_list = storage.get_posts_with_more_positive_reactions()
        assert len(posts_dto_list) == len(posts_with_more_positive_reactions)
        for (post_dto, post_fixture) in zip(
                posts_dto_list, posts_with_more_positive_reactions):
            assert post_dto.post_id == post_fixture ['id']

    @pytest.mark.django_db
    def test_get_reactions_to_post(
            self, post_reactions, post_data):
        storage = SQLStorage()
        reactions_dto_list = storage.get_reactions_to_post(post_id=post_data.id)
        assert len(reactions_dto_list) == len(post_reactions)
        for (reaction_dto, reaction_fixture) in zip(
                reactions_dto_list, post_reactions):
            assert reaction_dto.user.user_id == reaction_fixture.user_id
            assert reaction_dto.user.name == reaction_fixture.user.name
            assert reaction_dto.reaction_type == reaction_fixture.reaction_type

    @pytest.mark.django_db
    def test_get_replies_for_comment(self, comment_replies, comment_data):
        storage = SQLStorage()
        replies_dto_list = storage.get_replies_for_comment(
            comment_id=comment_data.id)
        assert len(replies_dto_list) == len(comment_replies)
        for (reply_dto, reply_fixture) in zip(
                replies_dto_list, comment_replies):
            assert reply_dto.commenter.user_id == reply_fixture.user_id
            assert reply_dto.commenter.name == reply_fixture.user.name
            assert reply_dto.comment.comment_id == reply_fixture.id
            assert reply_dto.comment.comment_content == \
                   reply_fixture.description

    @pytest.mark.django_db
    def test_get_post(self, populate_get_post):
        storage = SQLStorage()
        get_post_dto = storage.get_post(post_id=1)
        custom_dto = populate_get_post_dto()

        for (post_dto, custom_post) in zip(get_post_dto.post, custom_dto.post):
            assert post_dto.post_id == custom_post.post_id
            assert post_dto.post_content == custom_post.post_content
        for (comment_dto, custom_comment) in zip(
                get_post_dto.comments, custom_dto.comments):
            assert comment_dto.comment_id == custom_comment.comment_id
        for (user_dto, custom_user) in zip(
                get_post_dto.users, custom_dto.users):
            assert user_dto.user_id == custom_user.user_id
            assert user_dto.name == custom_user.name
        for (reaction_dto, custom_reaction) in zip(
                get_post_dto.reactions, custom_dto.reactions):
            assert reaction_dto.reaction_type == custom_reaction.reaction_type
            assert reaction_dto.user_id == custom_reaction.user_id

    @pytest.mark.django_db
    def test_get_user_posts(self, populate_get_post):
        storage = SQLStorage()
        get_user_posts_dto = storage.get_user_posts(user_id=1)
        custom_dto = populate_get_post_dto()

        for (post_dto, custom_post) in zip(
                get_user_posts_dto.post, custom_dto.post):
            assert post_dto.post_id == custom_post.post_id
            assert post_dto.post_content == custom_post.post_content
        for (comment_dto, custom_comment) in zip(
                get_user_posts_dto.comments, custom_dto.comments):
            assert comment_dto.comment_id == custom_comment.comment_id
        for (user_dto, custom_user) in zip(
                get_user_posts_dto.users, custom_dto.users):
            assert user_dto.user_id == custom_user.user_id
            assert user_dto.name == custom_user.name
        for (reaction_dto, custom_reaction) in zip(
                get_user_posts_dto.reactions, custom_dto.reactions):
            assert reaction_dto.reaction_type == custom_reaction.reaction_type
            assert reaction_dto.user_id == custom_reaction.user_id

    @pytest.mark.django_db
    def test_create_post_storage(self, user_data):
        storage = SQLStorage()
        create_post = storage.create_post_and_get_post_id(
            user_id=user_data.id, post_content="THis is my first post."
        )
        db_post = Post.objects.get(user_id=user_data.id)
        assert create_post.post_id == db_post.id

    @pytest.mark.django_db
    def test_add_comment_and_get_comment_id_storage(
            storage, user_data, post_data):
        storage = SQLStorage()
        comment = storage.add_comment_and_get_comment_id(
            user_id=user_data.id, post_id=post_data.id,
            comment_description="Comment"
        )
        db_comment = Comment.objects.get(
            user_id=user_data.id, post_id=post_data.id)
        assert db_comment.id == comment.comment_id

    @pytest.mark.django_db
    def test_reply_to_comment_and_get_reply_id_storage(
            self, user_data, comment_data):
        storage = SQLStorage()
        reply = storage.reply_to_comment_and_get_reply_id(
            user_id=user_data.id, comment_id=comment_data.id,
            description="Reply to comment"
        )
        db_reply = Comment.objects.get(
            user_id=user_data.id, parent_id=comment_data.id)
        assert db_reply.id == reply.comment_id

    @pytest.mark.django_db
    def test_react_to_post_on_correct_input_create_reaction(
            self, user_data, post_data):
        storage = SQLStorage()
        storage.react_to_post_on_correct_input_create_reaction(
            user_id=user_data.id, post_id=post_data.id,
            reaction_type=ReactionType.LIKE.value)
        is_reaction_exists_in_db = Reaction.objects.filter(
            user_id=user_data.id, post_id=post_data.id,
            reaction_type=ReactionType.LIKE.value
        ).exists()
        assert is_reaction_exists_in_db

    @pytest.mark.django_db
    def test_react_to_comment_on_correct_input_create_reaction(
            self, user_data, comment_data):
        storage = SQLStorage()
        storage.react_to_comment_on_correct_input_create_reaction(
            user_id=user_data.id, comment_id=comment_data.id,
            reaction_type=ReactionType.LIKE.value)
        is_reaction_exists_in_db = Reaction.objects.filter(
            user_id=user_data.id, comment_id=comment_data.id,
            reaction_type=ReactionType.LIKE.value
        ).exists()
        assert is_reaction_exists_in_db

    @pytest.mark.django_db
    def test_get_total_reactions_count(self, post_reaction_data):
        storage = SQLStorage()
        reaction_count = storage.get_total_reactions_count()
        assert reaction_count == 1

    @pytest.mark.django_db
    def test_get_reaction_metrics(self, reaction_metrics, post_data):
        storage = SQLStorage()
        reaction_metrics_dto_list = storage.get_reaction_metrics(
            post_id=post_data.id)
        for (reaction_dto, reaction_fixture) in zip(
                reaction_metrics_dto_list, reaction_metrics):
            dto_reaction_type = reaction_dto.reaction_type
            assert reaction_dto.count == reaction_fixture ['count']
            assert dto_reaction_type == reaction_fixture ['reaction_type']

    @pytest.mark.django_db
    def test_delete_post(self, post_data):
        storage = SQLStorage()
        storage.delete_post(post_id=post_data.id)
        with pytest.raises(Post.DoesNotExist) as PostNotExist:
            Post.objects.get(id=post_data.id)

    @pytest.mark.django_db
    def test_is_post_exists(self, post_data):
        storage = SQLStorage()
        is_post_exists = storage.is_post_exists(post_id=post_data.id)
        assert is_post_exists

    @pytest.mark.django_db
    def test_is_user_exists(self, user_data):
        storage = SQLStorage()
        is_user_exists = storage.is_user_exists(user_id=user_data.id)
        assert is_user_exists

    @pytest.mark.django_db
    def test_is_comment_exists(self, comment_data):
        storage = SQLStorage()
        is_comment_exists = storage.is_comment_exists(
            comment_id=comment_data.id)
        assert is_comment_exists

    @pytest.mark.django_db
    def test_is_post_reaction_exists(
            self, post_reaction_data, post_data, user_data):
        storage = SQLStorage()
        is_post_reaction_exists = storage.is_post_reaction_exists(
            post_id=post_data.id, user_id=user_data.id)
        assert is_post_reaction_exists

    @pytest.mark.django_db
    def test_is_comment_reaction_exists(
            self, comment_reaction_data, comment_data, user_data):
        storage = SQLStorage()
        is_comment_reaction_exists = storage.is_comment_reaction_exists(
            comment_id=comment_data.id, user_id=user_data.id
        )
        assert is_comment_reaction_exists

    @pytest.mark.django_db
    def test_get_post_reaction(
            self, post_reaction_data, post_data, user_data):
        storage = SQLStorage()
        post_reaction_dto = storage.get_post_reaction(
            post_id=post_data.id, user_id=user_data.id
        )
        assert post_reaction_data.reaction_type == \
               post_reaction_dto.reaction_type

    @pytest.mark.django_db
    def test_get_comment_reaction(
            self, comment_reaction_data, comment_data, user_data):
        storage = SQLStorage()
        comment_reaction_dto = storage.get_comment_reaction(
            user_id=user_data.id, comment_id=comment_data.id
        )
        assert comment_reaction_dto.reaction_type == \
               comment_reaction_data.reaction_type

    @pytest.mark.django_db
    def test_delete_post_reaction(
            self, post_reaction_data, post_data, user_data):
        storage = SQLStorage()
        storage.delete_post_reaction(post_id=post_data.id, user_id=user_data.id)

        with pytest.raises(Reaction.DoesNotExist) as ReactionNotExist:
            Reaction.objects.get(user_id=user_data.id, post_id=post_data.id)

    @pytest.mark.django_db
    def test_delete_comment_reaction(
            self, comment_reaction_data, comment_data, user_data):
        storage = SQLStorage()
        storage.delete_comment_reaction(comment_id=comment_data.id,
                                        user_id=user_data.id, reaction='LIKE')

        with pytest.raises(Reaction.DoesNotExist) as ReactionNotExist:
            Reaction.objects.get(
                user_id=user_data.id, comment_id=comment_data.id)

    @pytest.mark.django_db
    def test_update_post_reaction(
            self, post_reaction_data, post_data, user_data):
        storage = SQLStorage()
        storage.update_post_reaction(
            post_id=post_data.id, user_id=user_data.id,
            reaction_type=ReactionType.HAHA.value
        )
        is_reaction_updated = Reaction.objects.filter(
            post_id=post_data.id, user_id=user_data.id,
            reaction_type=ReactionType.HAHA.value
        ).exists()
        assert is_reaction_updated

    @pytest.mark.django_db
    def test_update_comment_reaction(
            self, comment_reaction_data, comment_data, user_data):
        storage = SQLStorage()
        storage.update_comment_reaction(
            comment_id=comment_data.id, user_id=user_data.id,
            reaction_type=ReactionType.HAHA.value
        )
        is_reaction_updated = Reaction.objects.filter(
            comment_id=comment_data.id, user_id=user_data.id,
            reaction_type=ReactionType.HAHA.value
        ).exists()
        assert is_reaction_updated

    @pytest.mark.django_db
    def test_is_comment_have_parent(self, comment_replies):
        storage = SQLStorage()
        is_comment_parent_exists = storage.is_comment_have_parent(
            comment_id=comment_replies [0].id)
        assert is_comment_parent_exists

    @pytest.mark.django_db
    def test_get_comment_parent_id(self, comment_replies, comment_data):
        storage = SQLStorage()
        parent_id = storage.get_comment_parent_id(
            comment_id=comment_replies [0].id)
        assert comment_data.id == parent_id.comment_id
