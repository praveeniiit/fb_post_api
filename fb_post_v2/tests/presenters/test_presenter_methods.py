import pytest
from django_swagger_utils.drf_server.exceptions import BadRequest, NotFound

from fb_post.constants.reactions import ReactionType
from fb_post_v2.interactors.storages.storage import PostIdDTO, CommentIdDTO, \
    ReactionMetricDTO, PostDTO, UserDTO, ReactionDTO, CommentDTO, ReplyDTO, \
    PostCompleteDetailsDTO, ReactionWithUserDTO
from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.utils.test_utils.post import make_post_dto, POSTS_COMPLETE_DICT


def test_get_create_post_response():
    presenter = DataPresenter()
    post_dto = PostIdDTO(post_id=1)
    post_dict = {
        'post_id': 1
    }

    response = presenter.get_create_post_response(post_id_dto=post_dto)
    assert response == post_dict


def test_get_add_comment_response():
    presenter = DataPresenter()
    comment_dto = CommentIdDTO(comment_id=1)
    comment_dict = {
        'comment_id': 1
    }

    response = presenter.get_add_comment_response(comment_id_dto=comment_dto)
    assert response == comment_dict


def test_get_reply_to_comment_response():
    presenter = DataPresenter()
    comment_dto = CommentIdDTO(comment_id=1)
    reply_dict = {
        'reply_comment_id': 1
    }

    response = presenter.get_reply_to_comment_response(comment_dto)
    assert response == reply_dict


def test_get_total_reactions_count_response():
    presenter = DataPresenter()
    reaction_count = 1
    reaction_count_dict = dict(reactions_count=reaction_count)

    response = presenter.get_total_reactions_count_response(reaction_count)
    assert reaction_count_dict == response


def test_get_reaction_metrics_response():
    presenter = DataPresenter()
    reaction_dto_list = [
        ReactionMetricDTO(
            reaction_type=ReactionType.LIKE.value, count=23),
        ReactionMetricDTO(
            reaction_type=ReactionType.WOW.value, count=2
        )
    ]
    reaction_dict = [
        {
            'reaction_type': reaction.reaction_type,
            'count': reaction.count
        } for reaction in reaction_dto_list
    ]

    response = presenter.get_reaction_metrics_response(
        reaction_metric_dto_list=reaction_dto_list)
    assert response == reaction_dict


def test_get_replies_for_comment():
    presenter = DataPresenter()
    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url='None')
    comment_dto = CommentDTO(
        comment_id=2, comment_content='Reply for the comment.',
        commented_at='2019:08:09 09:33:12'
    )
    reply_dto = ReplyDTO(commenter=user_dto, comment=comment_dto)
    reply_dto_dict = [
        {
            'comment_id': 2,
            'commenter': {
                'user_id': 1, 'name': 'Praveen', 'profile_pic': 'None'
            }
        }
    ]

    response = presenter.get_replies_for_comment([reply_dto])
    assert response == reply_dto_dict


def test_get_post():
    presenter = DataPresenter()
    user_posts_dto = make_post_dto()

    response = presenter.get_post_response(user_posts_dto)
    assert response == POSTS_COMPLETE_DICT[0]


def test_get_user_posts():
    presenter = DataPresenter()
    user_posts_dto = make_post_dto()

    response = presenter.get_response_for_user_posts(user_posts_dto)

    assert response == POSTS_COMPLETE_DICT


def test_get_reactions_to_post():
    presenter = DataPresenter()
    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url='None')
    reaction_dto = ReactionDTO(
        user=user_dto, reaction_type=ReactionType.LIKE.value)
    reaction_dto_dict = [
        {
            'user_id': user_dto.user_id,
            'name': user_dto.name,
            'profile_pic': user_dto.profile_pic_url,
            'reaction': reaction_dto.reaction_type
        }
    ]

    response = presenter.get_reactions_to_post(reactions_list=[reaction_dto])
    assert response == reaction_dto_dict


def test_get_posts_reacted_by_user():
    presenter = DataPresenter()
    reacted_post_dto = [
        PostIdDTO(post_id=2),
        PostIdDTO(post_id=3)
    ]
    reacted_post_dict = [dict(post_id=2), dict(post_id=3)]

    response = presenter.get_posts_reacted_by_user(post_dto=reacted_post_dto)
    assert response == reacted_post_dict


def test_get_posts_with_more_positive_reactions_response():
    presenter = DataPresenter()
    positive_post_dto = [
        PostIdDTO(post_id=4),
        PostIdDTO(post_id=3)
    ]
    positive_post_dict = [dict(post_id=4), dict(post_id=3)]

    response = presenter.get_posts_with_more_positive_reactions_response(
        positive_posts_list=positive_post_dto
    )
    assert positive_post_dict == response


def test_raise_invalid_user_id_exception():
    presenter = DataPresenter()
    with pytest.raises(NotFound) as UserNotFound:
        presenter.raise_invalid_user_id_exception()


def test_raise_invalid_post_id_exception():
    presenter = DataPresenter()
    with pytest.raises(NotFound) as PostNotFound:
        presenter.raise_invalid_post_id_exception()


def test_raise_invalid_comment_id_exception():
    presenter = DataPresenter()
    with pytest.raises(NotFound) as CommentNotFound:
        presenter.raise_invalid_comment_id_exception()


def test_raise_invalid_reaction_exception():
    presenter = DataPresenter()
    with pytest.raises(BadRequest) as ReactionInvalid:
        presenter.raise_invalid_reaction_exception()
