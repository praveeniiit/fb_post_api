import datetime

from fb_post_v2.interactors.storages.storage import PostDTO, UserDTO, \
    CommentDTO, ReactionWithUserDTO, PostCompleteDetailsDTO


POSTS_COMPLETE_DICT = [
    {'post_id': 2, 'post_content': 'Post from admin!', 'posted_by':
        {'name': 'Praveen', 'user_id': 1, 'profile_pic': 'None'},
     'posted_at': '09/03/2019, 17:00:30',
     'comments':
         [{'comment_content': 'Comment from',
           'commented_at': '09/03/2019, 17:00:30',
           'commenter':
               {'name': 'Praveen', 'user_id': 1, 'profile_pic': 'None'},
           'reactions': {'type': ['LOVE'], 'count': 1},
           'replies': [
               {'comment_content': 'Reply to comment',
                'commented_at': '09/03/2019, 17:00:30',
                'commenter':
                    {'name': 'Praveen', 'user_id': 1, 'profile_pic': 'None'},
                'reactions': {'type': ['WOW'], 'count': 1}}]}],
     'reactions':
         {'type': ['LIKE', 'LOVE'], 'count': 2}
     }
]


def make_post_dto():
    post_dto = PostDTO(
        post_id=2, post_content='Post from admin!',
        posted_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988), user_id=1)
    user_dto = UserDTO(user_id=1, name='Praveen', profile_pic_url='None')
    comment_dto = CommentDTO(
        comment_id=2, comment_content="Comment from",
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988),
        post_id=2, user_id=1)
    reply_dto = CommentDTO(
        comment_id=3, comment_content='Reply to comment',
        commented_at=datetime.datetime(2019, 9, 3, 17, 0, 30, 566988),
        parent_id=2, user_id=1)
    reaction_dto = ReactionWithUserDTO(reaction_type="LIKE", user_id=1,
                                       post_id=2)
    reaction_dto2 = ReactionWithUserDTO(reaction_type="LOVE", user_id=1,
                                        post_id=2)
    comment_reaction = ReactionWithUserDTO(reaction_type="LOVE", user_id=1,
                                           comment_id=2)
    reply_reaction = ReactionWithUserDTO(reaction_type="WOW", user_id=1,
                                         comment_id=3)

    user_posts_dto = PostCompleteDetailsDTO(
        post=[post_dto], users=[user_dto], comments=[comment_dto, reply_dto],
        reactions=[reaction_dto, comment_reaction, reply_reaction,
                   reaction_dto2])
    return user_posts_dto
