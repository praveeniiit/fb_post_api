from abc import ABC
from typing import List

from django_swagger_utils.drf_server.exceptions import BadRequest, NotFound

from fb_post_v2.constants.magic_strings import INVALID_POST_ID, \
    INVALID_COMMENT_ID, INVALID_REACTION, INVALID_USER_ID
from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import PostDTO, ReplyDTO, \
    ReactionDTO, PostIdDTO, PostCompleteDetailsDTO, ReactionMetricDTO, \
    CommentIdDTO, ReplyDTOWithCount, PostIdDTOWithTotal, ReactionDTOWithTotal, \
    PostCompleteDetailsDTOWithTotal


def convert_user_dto_to_dict(user_dto):
    return {
        'user_id': user_dto.user_id,
        'name': user_dto.name,
        'profile_pic': user_dto.profile_pic_url,
    }


class DataPresenter(Presenter):
    def get_create_post_response(self, post_id_dto: PostIdDTO):
        response = {
            'post_id': post_id_dto.post_id
        }
        return response

    def get_add_comment_response(self, comment_id_dto: CommentIdDTO):
        return {
            'comment_id': comment_id_dto.comment_id
        }

    def get_reply_to_comment_response(self, comment_id_dto: CommentIdDTO):
        return {
            'reply_comment_id': comment_id_dto.comment_id
        }

    def get_total_reactions_count_response(self, reactions_count: int):
        return {
            'reactions_count': reactions_count
        }

    def get_reaction_metrics_response(
            self, reaction_metric_dto_list: List[ReactionMetricDTO]):
        # reaction_list = [r.reaction_type for r in reaction_metric_dto_list]
        # print('Reaction_list\n\n\n', reaction_list)
        # unique_reaction_list = []
        #
        # for reaction_type in reaction_list:
        #     if reaction_type not in unique_reaction_list:
        #         unique_reaction_list.append(
        #             {
        #                 'type': reaction_type,
        #                 'count': reaction_list.count(reaction_type)
        #             }
        #         )
        # return unique_reaction_list
        return [
            {
                'reaction_type': reaction.reaction_type,
                'count': reaction.count
            } for reaction in reaction_metric_dto_list
        ]

    def get_post_response(self, get_post_dto: PostCompleteDetailsDTO):
        return self.get_response_for_user_posts(get_post_dto)[0]

    def get_posts_with_more_positive_reactions_response(
            self, positive_posts_list_with_total: PostIdDTOWithTotal):
        positive_posts_list = positive_posts_list_with_total.post_id_dtos
        positive_posts_total = positive_posts_list_with_total.total

        return {
            'total': positive_posts_total,
            'result': [post.post_id for post in positive_posts_list]
        }

    def get_reactions_to_post(self,
                              reactions_list_with_total: ReactionDTOWithTotal):
        reactions_list = reactions_list_with_total.reaction_dto_list
        reactions_list_total = reactions_list_with_total.total

        post_reactions = []
        for reaction in reactions_list:
            user_dto_list = convert_user_dto_to_dict(reaction.user)
            user_dto_list.update(
                {'reaction': reaction.reaction_type}
            )
            post_reactions.append(user_dto_list)

        return {
            'total': reactions_list_total,
            'result': post_reactions
        }

    def get_replies_for_comment(self,
                                replies_list_with_total: ReplyDTOWithCount):
        replies_list = replies_list_with_total.replies_dto
        replies = []
        for reply in replies_list:
            user_dto = reply.commenter
            comment = reply.comment
            replies.append(
                {
                    'comment_id': comment.comment_id,
                    'commenter': convert_user_dto_to_dict(user_dto)
                }
            )
        return {
            'total': replies_list_with_total.total,
            'result': replies
        }

    def get_posts_reacted_by_user(self,
                                  post_dtos_with_total: PostIdDTOWithTotal):
        post_dto = post_dtos_with_total.post_id_dtos
        post_total = post_dtos_with_total.total
        return {
            'total': post_total,
            'result': [
                {
                    'post_id': post.post_id
                } for post in post_dto
            ]
        }

    def raise_invalid_user_id_exception(self):
        raise NotFound(*INVALID_USER_ID)

    def raise_invalid_post_id_exception(self):
        raise NotFound(*INVALID_POST_ID)

    def raise_invalid_comment_id_exception(self):
        raise NotFound(*INVALID_COMMENT_ID)

    def raise_invalid_reaction_exception(self):
        raise BadRequest(*INVALID_REACTION)

    def get_response_for_user_posts(
            self, users_post_dtos_with_total: PostCompleteDetailsDTOWithTotal):
        data = users_post_dtos_with_total.user_post_dtos
        posts_count = users_post_dtos_with_total.total

        posts = data.post
        users_dto = data.users
        user_posts_dict = []

        for post in posts:
            user_posts_dict.append(
                convert_post_to_complete_dictionary(post, data, users_dto)
            )

        return {
            'total': posts_count,
            'result': user_posts_dict
        }


def convert_post_to_dict(post, user_dto):
    return {
        'post_id': post.post_id,
        'post_content': post.post_content,
        'posted_by': get_user_dict(post.user_id, user_dto),
        'posted_at': post.posted_at.strftime("%m/%d/%Y, %H:%M:%S"),
        'comments': post.post_id
    }


def convert_comment_dto_to_dict(comment, users_dto):
    return {
        'comment_content': comment.comment_content,
        'commented_at': comment.commented_at.strftime("%m/%d/%Y, %H:%M:%S"),
        'commenter': get_user_dict(comment.user_id, users_dto)
    }


def convert_reactions_to_dict(reaction_dto):
    reactions_types = list({r.reaction_type for r in reaction_dto})
    reactions_types.sort()
    reaction_count = len(reactions_types)

    return {
        'type': reactions_types,
        'count': reaction_count
    }


def get_user_dict(user_id, users_dto):
    users = filter(lambda u: u.user_id == user_id, users_dto)
    user = [user for user in users][0]
    return {
        'name': user.name,
        'user_id': user.user_id,
        'profile_pic': user.profile_pic_url
    }


def get_replies(replies, users_dto, complete_details):
    replies_list = []
    for reply in replies:
        reply_dict = convert_comment_dto_to_dict(reply, users_dto)
        reply_reactions = filter(
            lambda x: x.comment_id == reply.comment_id,
            complete_details.reactions)
        reply_dict['reactions'] = convert_reactions_to_dict(
            reply_reactions
        )
        replies_list.append(reply_dict)
    return replies_list


def get_comments_in_dict_format(post_comments, users_dto, complete_details,
                                comment_replies_map):
    comments_list = []
    for comment in post_comments:
        comment_dict = convert_comment_dto_to_dict(
            comment, users_dto)

        comment_reactions = filter(
            lambda x: x.comment_id == comment.comment_id,
            complete_details.reactions)

        replies = comment_replies_map[comment.comment_id]
        comment_dict['reactions'] = convert_reactions_to_dict(comment_reactions)

        replies_list = get_replies(replies, users_dto, complete_details)

        comment_dict['replies'] = replies_list
        comments_list.append(comment_dict)
    return comments_list


def convert_post_to_complete_dictionary(
        post, complete_details: PostCompleteDetailsDTO, users_dto):
    post_reactions = filter(lambda x: x.post_id == post.post_id,
                            complete_details.reactions)
    post_comments = filter(
        lambda x: x.post_id == post.post_id,
        complete_details.comments)

    post_replies = filter(
        lambda x: x.post_id is None,
        complete_details.comments)

    from collections import defaultdict
    comment_replies_map = defaultdict(lambda: [])

    for reply in post_replies:
        comment_replies_map[reply.parent_id].append(reply)

    post_dict = convert_post_to_dict(post, users_dto)
    post_dict['reactions'] = convert_reactions_to_dict(post_reactions)

    comments_list = get_comments_in_dict_format(
        post_comments, users_dto, complete_details, comment_replies_map)
    post_dict['comments'] = comments_list

    return post_dict
