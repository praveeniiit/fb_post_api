from typing import List

from django.db.models import Count, Q, F, Prefetch

from fb_post_v2.constants.reactions import ReactionType, \
    POSITIVE_REACTIONS_LIST, NEGATIVE_REACTIONS_LIST
from fb_post_v2.interactors.storages.storage import Storage, CommentIdDTO, \
    ReactionTypeDTO, ReplyDTO, ReactionDTO, PostDTO, PostIdDTO, \
    PostCompleteDetailsDTO, ReactionMetricDTO, UserDTO, CommentDTO, \
    ReactionWithUserDTO, ReplyDTOWithCount, PostIdDTOWithTotal, \
    ReactionDTOWithTotal, PostCompleteDetailsDTOWithTotal
from fb_post_v2.models import Post, Comment, Reaction, User


def convert_comment_to_dto(comment) -> CommentDTO:
    return CommentDTO(
        user_id=comment.user_id, post_id=comment.post_id,
        comment_id=comment.id, comment_content=comment.description,
        commented_at=comment.pub_date
    )


def convert_reply_to_dto(reply) -> CommentDTO:
    return CommentDTO(
        user_id=reply.user_id, parent_id=reply.post_id,
        comment_id=reply.id, comment_content=reply.description,
        commented_at=reply.pub_date
    )


def convert_post_reaction_to_dto(reaction) -> ReactionWithUserDTO:
    return ReactionWithUserDTO(
        user_id=reaction.user_id, post_id=reaction.post_id,
        reaction_type=reaction.reaction_type
    )


def convert_comment_reaction_to_dto(reaction) -> ReactionWithUserDTO:
    return ReactionWithUserDTO(
        user_id=reaction.user_id, comment_id=reaction.comment_id,
        reaction_type=reaction.reaction_type
    )


def convert_user_to_dto(user) -> UserDTO:
    return UserDTO(
        user_id=user.id, name=user.name, profile_pic_url=user.profile_pic_url
    )


def convert_full_post_to_dto(posts):
    posts_dto, users_dto = [], []
    reaction_dto_list, comments_dto_list = [], []
    for post in posts:
        post_dto = PostDTO(
            post_id=post.id, post_content=post.description,
            posted_at=post.pub_date, user_id=post.user_id
        )
        posts_dto.append(post_dto)
        users_dto.append(UserDTO(
            user_id=post.user_id, name=post.user.name,
            profile_pic_url=post.user.profile_pic_url
        ))

        for post_reaction in post.reactions.all():
            reaction_dto_list.append(
                convert_post_reaction_to_dto(post_reaction)
            )

        for comment in post.comments.all():
            comments_dto_list.append(convert_comment_to_dto(comment=comment))
            users_dto.append(convert_user_to_dto(user=comment.user))
            for comment_reaction in comment.reactions.all():
                reaction_dto_list.append(
                    convert_comment_reaction_to_dto(comment_reaction)
                )

            replies = comment.replies.all()
            for reply in replies:
                comments_dto_list.append(convert_reply_to_dto(reply=reply))
                users_dto.append(convert_user_to_dto(user=reply.user))
                for comment_reaction in reply.reactions.all():
                    reaction_dto_list.append(
                        convert_comment_reaction_to_dto(comment_reaction)
                    )
    post_complete_dto = PostCompleteDetailsDTO(
        post=posts_dto, users=users_dto, comments=comments_dto_list,
        reactions=reaction_dto_list
    )
    return post_complete_dto


class SQLStorage(Storage):
    def create_post_and_get_post_id(self, user_id, post_content) -> PostIdDTO:
        post = Post.objects.create(
            description=post_content, user_id=user_id
        )
        post_id_dto = PostIdDTO(post_id=post.id)
        return post_id_dto

    def add_comment_and_get_comment_id(self, user_id, post_id,
                                       comment_description) -> CommentIdDTO:
        comment = Comment.objects.create(
            user_id=user_id, post_id=post_id, description=comment_description
        )
        comment_id_dto = CommentIdDTO(comment_id=comment.id)
        return comment_id_dto

    def reply_to_comment_and_get_reply_id(self, comment_id, user_id,
                                          description) -> CommentIdDTO:
        reply = Comment.objects.create(
            user_id=user_id, parent_id=comment_id, description=description
        )
        reply_id_dto = CommentIdDTO(comment_id=reply.id)
        return reply_id_dto

    def react_to_post_on_correct_input_create_reaction(
            self, user_id: int, post_id: int, reaction_type: str) -> None:
        Reaction.objects.create(
            user_id=user_id, post_id=post_id, reaction_type=reaction_type
        )

    def react_to_comment_on_correct_input_create_reaction(
            self, user_id: int, comment_id: int, reaction_type: str) -> None:
        Reaction.objects.create(
            user_id=user_id, comment_id=comment_id, reaction_type=reaction_type
        )

    def get_total_reactions_count(self) -> int:
        return Reaction.objects.count()

    def get_reaction_metrics(self, post_id: int) -> List[ReactionMetricDTO]:
        reaction_metrics = Reaction.objects.filter(post_id=post_id).values(
            "reaction_type").annotate(count=Count('reaction_type'))
        reaction_metrics_dto_list = []
        for reaction in reaction_metrics:
            reaction_dto = ReactionMetricDTO(
                reaction_type=reaction['reaction_type'],
                count=reaction['count'])
            reaction_metrics_dto_list.append(reaction_dto)
        return reaction_metrics_dto_list

    def delete_post(self, post_id: int):
        Post.objects.get(id=post_id).delete()

    def get_post(self, post_id: int) -> PostCompleteDetailsDTO:
        post = Post.objects.filter(id=post_id).select_related(
            'user').prefetch_related(
            Prefetch("reactions", to_attr="post_reactions"),
            Prefetch("comments",
                     queryset=Comment.objects.select_related('user'),
                     to_attr="post_comments"),
            Prefetch("post_comments__reactions", to_attr="comment_reactions"),
            Prefetch("post_comments__replies",
                     queryset=Comment.objects.select_related(
                         "user").prefetch_related(
                         Prefetch('reactions', to_attr="reply_reactions")),
                     to_attr="comment_replies"),
        )[0]
        return convert_full_post_to_dto([post])

    def get_user_posts(self, user_id: int, offset=0,
                       limit=15) -> PostCompleteDetailsDTOWithTotal:
        posts = Post.objects.filter(user_id=user_id).select_related(
            'user').prefetch_related(
            Prefetch("reactions", to_attr="post_reactions"),
            Prefetch("comments",
                     queryset=Comment.objects.select_related('user'),
                     to_attr="post_comments"),
            Prefetch("post_comments__reactions", to_attr="comment_reactions"),
            Prefetch("post_comments__replies",
                     queryset=Comment.objects.select_related(
                         "user").prefetch_related(
                         Prefetch('reactions', to_attr="reply_reactions")),
                     to_attr="comment_replies"),
        )[offset:offset + limit]
        posts_count = Post.objects.filter(user_id=user_id).count()

        return PostCompleteDetailsDTOWithTotal(
            total=posts_count,
            user_post_dtos=convert_full_post_to_dto(posts)
        )

    def get_posts_with_more_positive_reactions(self, offset=0,
                                               limit=15) -> PostIdDTOWithTotal:
        positive_exp = Count(
            'reactions',
            filter=Q(reactions__reaction_type__in=POSITIVE_REACTIONS_LIST))
        negative_exp = Count(
            'reactions',
            filter=Q(reactions__reaction_type__in=NEGATIVE_REACTIONS_LIST))

        positive_posts = Post.objects.annotate(
            positive_count=positive_exp, negative_count=negative_exp).filter(
            positive_count__gt=F('negative_count')).values(
            'id').distinct()[offset:offset + limit]

        positive_posts_count = Post.objects.annotate(
            positive_count=positive_exp, negative_count=negative_exp).filter(
            positive_count__gt=F('negative_count')).values(
            'id').distinct().count()

        positive_post_dto_list = []
        for post in positive_posts:
            post_id_dto = PostIdDTO(post_id=post['id'])
            positive_post_dto_list.append(post_id_dto)

        return PostIdDTOWithTotal(
            total=positive_posts_count,
            post_id_dtos=positive_post_dto_list
        )

    def get_posts_reacted_by_user(self, user_id: int, offset=0,
                                  limit=15) -> PostIdDTOWithTotal:
        user_posts = Post.objects.filter(reactions__user_id=user_id).values(
            'id').distinct()[offset:offset + limit]
        user_posts_count = Post.objects.filter(
            reactions__user_id=user_id).values('id').distinct().count()

        user_posts_dto_list = []
        for post in user_posts:
            post_id_dto = PostIdDTO(post_id=post['id'])
            user_posts_dto_list.append(post_id_dto)

        return PostIdDTOWithTotal(
            total=user_posts_count,
            post_id_dtos=user_posts_dto_list
        )

    def get_reactions_to_post(self, post_id: int, offset: int = 0,
                              limit: int = 15) -> ReactionDTOWithTotal:
        reaction_with_user = Reaction.objects.filter(
            post_id=post_id).select_related('user')[offset:offset + limit]
        reaction_with_user_count = Reaction.objects.filter(
            post_id=post_id).count()

        reaction_with_user_dto_list = []
        for reaction in reaction_with_user:
            reacted_user = UserDTO(
                user_id=reaction.user_id, name=reaction.user.name,
                profile_pic_url=reaction.user.profile_pic_url
            )
            reaction_dto = ReactionDTO(
                user=reacted_user, reaction_type=reaction.reaction_type
            )
            reaction_with_user_dto_list.append(reaction_dto)

        return ReactionDTOWithTotal(
            total=reaction_with_user_count,
            reaction_dto_list=reaction_with_user_dto_list
        )

    def get_replies_for_comment(self, comment_id: int, offset=1,
                                limit=15) -> ReplyDTOWithCount:
        print('UseReplies count', limit, offset)
        replies = Comment.objects.get(id=comment_id).replies.select_related(
            "user")[offset:offset + limit]
        replies_count = Comment.objects.get(id=comment_id).replies.count()
        replies_dto_list = []

        for reply in replies:
            commenter_dto = UserDTO(
                name=reply.user.name, user_id=reply.user_id,
                profile_pic_url=reply.user.profile_pic_url
            )
            comment_dto = CommentDTO(
                comment_id=reply.id, comment_content=reply.description,
                commented_at=reply.pub_date
            )
            replies_dto_list.append(
                ReplyDTO(comment=comment_dto, commenter=commenter_dto)
            )
        return ReplyDTOWithCount(total=replies_count,
                                 replies_dto=replies_dto_list)

    def is_post_exists(self, post_id) -> bool:
        return Post.objects.filter(id=post_id).exists()

    def is_user_exists(self, user_id) -> bool:
        return User.objects.filter(id=user_id).exists()

    def is_comment_exists(self, comment_id) -> bool:
        return Comment.objects.filter(id=comment_id).exists()

    def is_post_reaction_exists(self, post_id: int, user_id: int) -> bool:
        return Reaction.objects.filter(
            post_id=post_id, user_id=user_id).exists()

    def is_comment_reaction_exists(self, user_id: int, comment_id: int) -> bool:
        return Reaction.objects.filter(
            comment_id=comment_id, user_id=user_id).exists()

    def get_post_reaction(self, post_id: int, user_id: int) -> ReactionTypeDTO:
        reaction = Reaction.objects.get(post_id=post_id, user_id=user_id)
        reaction_type_dto = ReactionTypeDTO(
            reaction_type=reaction.reaction_type)
        return reaction_type_dto

    def get_comment_reaction(self, user_id: int,
                             comment_id: int) -> ReactionTypeDTO:
        reaction = Reaction.objects.get(comment_id=comment_id, user_id=user_id)
        reaction_type_dto = ReactionTypeDTO(
            reaction_type=reaction.reaction_type)
        return reaction_type_dto

    def delete_post_reaction(self, post_id: int, user_id: int):
        Reaction.objects.filter(post_id=post_id, user_id=user_id).delete()

    def delete_comment_reaction(self, comment_id: int, user_id: int, reaction):
        Reaction.objects.filter(
            comment_id=comment_id, user_id=user_id,
            reaction_type=reaction).delete()

    def update_post_reaction(self, post_id: int, user_id: int,
                             reaction_type: str):
        post_reaction = Reaction.objects.get(
            post_id=post_id, user_id=user_id
        )
        post_reaction.reaction_type = reaction_type
        post_reaction.save()

    def update_comment_reaction(self, user_id: int, comment_id: int,
                                reaction_type: str):
        post_reaction = Reaction.objects.get(
            comment_id=comment_id, user_id=user_id
        )
        post_reaction.reaction_type = reaction_type
        post_reaction.save()

    def is_comment_have_parent(self, comment_id) -> bool:
        is_parent_exists = Comment.objects.get(id=comment_id).parent_id
        if is_parent_exists:
            return True
        else:
            return False

    def get_comment_parent_id(self, comment_id) -> CommentIdDTO:
        parent_id = Comment.objects.get(id=comment_id).parent_id
        comment_dto = CommentIdDTO(comment_id=parent_id)

        return comment_dto
