import time


def trace_time(func):
    print('After trace_time\n')
    def g(left, right):
        print('After G\n')
        start_time = time.time()
        sum_data = func(left, right)
        end_time = time.time()

        print(f'Request Executed in: {end_time - start_time}')
        return sum_data



    return g
print('In the middle of trace\n')


@trace_time
def sum(left, right):
    print('After Sum\n')
    return left + right

print('After the sum outer')
if __name__ == '__main__':
    # print(sum(3, 4))
    pass