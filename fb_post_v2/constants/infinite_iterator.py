class InfiniteIterator:

    def __iter__(self):
        self.num = 1
        return self

    def __next__(self):
        current_number = self.num
        self.num += 2
        return current_number


if __name__ == '__main__':
    odd_number_iterator = InfiniteIterator()
    odd_iter_object = iter(odd_number_iterator)

    print(next(odd_iter_object))
    print(next(odd_iter_object))
    print(next(odd_iter_object))