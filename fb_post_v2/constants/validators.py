from fb_post_v2.constants.reactions import REACTION_LIST
from fb_post_v2.models.comment import Comment
from fb_post_v2.models.post import Post
from fb_post_v2.models.user import User


class InvalidUserId(Exception):
    pass


class InvalidPostId(Exception):
    pass


class InvalidReaction(Exception):
    pass


class InvalidCommentId(Exception):
    pass


class PostReactionNotFound(Exception):
    pass


class CommentReactionNotFound(Exception):
    pass


def validate_user_id(user_id):
    try:
        user = User.objects.get(pk=user_id)
        return user
    except User.DoesNotExist:
        raise InvalidUserId


def validate_post_id(post_id):
    try:
        post = Post.objects.get(pk=post_id)
        return post
    except Post.DoesNotExist:
        raise InvalidPostId


def validate_reaction_type(reaction_type):
    reaction_is_not_valid = reaction_type not in REACTION_LIST

    if reaction_is_not_valid:
        raise InvalidReaction


def validate_comment_id(comment_id):
    try:
        comment = Comment.objects.get(pk=comment_id)
        return comment
    except Comment.DoesNotExist:
        raise InvalidCommentId