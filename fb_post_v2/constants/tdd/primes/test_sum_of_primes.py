#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from fb_post_v2.constants.tdd.primes.sum_of_primes import sum_of_primes


def test_sum_of_primes_empty_list():
    assert sum_of_primes([]) == 0


def test_sum_of_primes_mixed_list():
    assert sum_of_primes([11, 15, 17, 18, 20, 100]) == 28
