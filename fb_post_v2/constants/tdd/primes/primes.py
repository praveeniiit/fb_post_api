#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
import math


def is_prime(num):
    if num <= 1:
        return False

    for n in range(2, math.floor(math.sqrt(num) + 1)):
        if num % n == 0:
            return False

    return True
