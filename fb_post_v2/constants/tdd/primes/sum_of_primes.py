#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from fb_post_v2.constants.tdd.primes.primes import is_prime


def sum_of_primes(prime_list):
    filtered_prime_list = []
    for number in prime_list:
        is_prime_number = is_prime(number)
        if is_prime_number:
            filtered_prime_list.append(number)

    return sum([x for x in prime_list if is_prime(x)])
