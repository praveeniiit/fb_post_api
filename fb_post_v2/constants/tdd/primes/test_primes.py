#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from fb_post_v2.constants.tdd.primes.primes import is_prime


def test_prime_low_number():
    assert is_prime(1) is False


def test_prime_prime_number():
    assert is_prime(29) is True


def test_prime_composite_number():
    assert is_prime(15) is False
