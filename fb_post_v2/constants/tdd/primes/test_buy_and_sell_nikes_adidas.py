#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from fb_post_v2.constants.tdd.inventory.inventory import Inventory


def test_buy_and_sell_nikes_adidas():
    inventory = Inventory()
    assert inventory.limit == 100
    assert inventory.total_items == 0

    inventory.add_new_stock('Nike', 50.00, 10)
    assert inventory.total_items == 10

    inventory.add_new_stock('Adidas', 70.00, 5)
    assert inventory.total_items == 15

    inventory.remove_stock('Nike', 2)
    assert inventory.total_items == 13

    inventory.remove_stock('Adidas', 1)
    assert inventory.total_items == 12
