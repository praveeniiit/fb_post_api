#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.


class InvalidQuantityException(Exception):
    pass


class Inventory:
    def __init__(self, limit: int = 100):
        self.limit = limit
        self.total_items = 0
        self.stocks = {}

    def add_new_stock(self, name, price, quantity):
        if quantity <= 0:
            raise InvalidQuantityException(
                'Cannot add a quantity of {}. All new stocks must have at '
                'least 1 item'.format(
                    quantity))
        self.stocks [name] = {
            'price': price,
            'quantity': quantity
        }
        self.total_items += quantity
