#  Copyright (c) 2019. This file and code belongs to Praveen. You are free to
#  copy and re-produce, with prior information to the author.
from fb_post_v2.constants.tdd.inventory.inventory import Inventory,\
    InvalidQuantityException

import pytest


@pytest.fixture
def no_stock_inventory():
    return Inventory(10)


def test_inventory_initial_100(no_stock_inventory):
    assert no_stock_inventory.limit == 10
    assert no_stock_inventory.total_items == 0


def test_inventory_custom_limit():
    inventory = Inventory(limit=25)
    assert inventory.limit == 25
    assert inventory.total_items == 0


def test_inventory_add_new_stock_success(no_stock_inventory):
    no_stock_inventory.add_new_stock('Test Jacket', 10.00, 5)
    assert no_stock_inventory.total_items == 5
    assert no_stock_inventory.stocks ['Test Jacket'] ['price'] == 10.00
    assert no_stock_inventory.stocks ['Test Jacket'] ['quantity'] == 5


@pytest.mark.parametrize('name,price,quantity,exception', [
    ('Test Jacket', 10.00, 0, InvalidQuantityException(
        'Cannot add a quantity of 0. All new stocks must have at least 1 item.'
    ))
])
def test_add_new_stock_bad_input(name, price, quantity, exception):
    inventory = Inventory(10)
    try:
        inventory.add_new_stock(name, price, quantity)
    except InvalidQuantityException as inst:
        assert isinstance(inst, type(exception))
        assert inst.args == exception.args
    else:
        pytest.fail('Expected error but found none')
