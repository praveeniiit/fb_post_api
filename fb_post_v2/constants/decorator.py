level = 0


def trace(f):
    def g(*args):
        global level

        prefix = "|  " * level + "|--"
        strargs = ", ".join(repr(a) for a in args)
        print("{} {}({})".format(prefix, f.__name__, strargs))
        # increment the level before calling the function
        # and decrement it after the call
        level += 1
        result = f(*args)
        level -= 1
        return result

    return g


@trace
def square(x):
    return x * x


@trace
def sum_of_squares(x, y):
    return square(x) + square(y)


if __name__ == '__main__':
    print(sum_of_squares(3, 4))
