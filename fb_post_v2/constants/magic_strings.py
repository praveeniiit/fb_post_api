NEW_POST = (
    1,
    "New Post from admin."
)
INVALID_USER_ID = ('Invalid User Id', 'INVALID_USER_ID')
INVALID_POST_ID = ('Invalid Post Id', 'INVALID_POST_ID')
INVALID_COMMENT_ID = ('Invalid Comment Id', 'INVALID_COMMENT_ID')
INVALID_REACTION = ('Invalid Reaction Type', 'INVALID_REACTION_TYPE')