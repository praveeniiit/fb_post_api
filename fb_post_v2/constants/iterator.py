class MyNumbers:
    def __init__(self):
        self.value = 1

    def __iter__(self):
        self.value = 1
        return self

    def __next__(self):
        if self.value >= 10:
            raise StopIteration
        x = self.value
        self.value += 1
        return x


if __name__ == '__main__':
    myclass = MyNumbers()
    myclass_iter = iter(myclass)

    print(next(myclass_iter))
    print(next(myclass_iter))
    print(next(myclass_iter))
    print(next(myclass_iter))
    print(next(myclass_iter))
