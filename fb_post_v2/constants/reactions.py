from enum import Enum

REACTION_LIST = ["LIKE", "LOVE", "HAHA", "WOW", "SAD", "ANGRY"]


class ReactionType(Enum):
    LIKE = "LIKE"
    LOVE = "LOVE"
    HAHA = "HAHA"
    WOW = "WOW"
    SAD = "SAD"
    ANGRY = "ANGRY"


POSITIVE_REACTIONS_LIST = [ReactionType.LIKE.value, ReactionType.LOVE.value,
                           ReactionType.HAHA.value, ReactionType.WOW.value]
NEGATIVE_REACTIONS_LIST = [ReactionType.WOW.value, ReactionType.ANGRY.value]
