# def trace(f):
#     def g(*args):
#         print(f.__name__, *args)
#         return f(*args)
#
#     return g
level = 0


def trace(f):
    def g(*args):
        global level
        prefix = "|  " * level + "|--"
        strargs = ", ".join(repr(a) for a in args)
        print("{} {}({})".format(prefix, f.__name__, strargs))

        level += 1
        result = f(*args)
        level -= 1

        return result

    return g


@trace
def square(x):
    return x * x


@trace
def sum_of_squares(x, y):
    return square(x) + square(y)


print(sum_of_squares(3, 4))


@trace
def fib(n):
    if n == 0 or n == 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


if __name__ == '__main__':
    # print(square(3))
    print('fibo', fib(3))
