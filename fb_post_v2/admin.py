# your django admin
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models.user import User
from .models.post import Post
from .models.reaction import Reaction
from .models.comment import Comment


admin.site.register(User, UserAdmin)
admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Reaction)