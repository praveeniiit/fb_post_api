from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage


class GetUserPostsInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def get_user_posts(self, user_id: int, offset: int, limit: int):
        is_user_not_exists = not self.storage.is_user_exists(user_id=user_id)

        if is_user_not_exists:
            self.presenter.raise_invalid_user_id_exception()
        get_user_posts_dto_with_total = self.storage.get_user_posts(
            user_id=user_id, offset=offset, limit=limit)
        return self.presenter.get_response_for_user_posts(
            get_user_posts_dto_with_total)
