from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage


class GetPostsReactedByUserInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def get_posts_reacted_by_user(self, user_id: int, offset: int, limit: int):
        is_user_not_exists = not self.storage.is_user_exists(user_id=user_id)

        if is_user_not_exists:
            self.presenter.raise_invalid_user_id_exception()
        post_dtos_with_total = self.storage.get_posts_reacted_by_user(
            user_id=user_id, offset=offset, limit=limit)
        return self.presenter.get_posts_reacted_by_user(post_dtos_with_total)
