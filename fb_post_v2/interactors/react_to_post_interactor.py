from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage

REACTION_TYPE = ["LIKE", "LOVE", "WOW", "HAHA", "SAD", "ANGRY"]


class ReactToPostInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def react_to_post(self, user_id: int, post_id: int, reaction_type: str):
        is_invalid_post_id = not self.storage.is_post_exists(post_id=post_id)

        if is_invalid_post_id:
            self.presenter.raise_invalid_post_id_exception()

        is_invalid_reaction_type = reaction_type not in REACTION_TYPE
        is_reaction_exists = self.storage.is_post_reaction_exists(
            user_id=user_id, post_id=post_id
        )

        if is_invalid_reaction_type:
            self.presenter.raise_invalid_reaction_exception()
        if is_reaction_exists:
            reaction_type_from_db = self.storage.get_post_reaction(
                post_id, user_id
            )
            undo_action = reaction_type == reaction_type_from_db.reaction_type
            if undo_action:
                self.undo_reaction(post_id, user_id)
            else:
                self.update_reaction(post_id, user_id, reaction_type)

        else:
            self.storage.react_to_post_on_correct_input_create_reaction(
                user_id=user_id, post_id=post_id, reaction_type=reaction_type
            )

    def undo_reaction(self, post_id, user_id):
        self.storage.delete_post_reaction(post_id=post_id, user_id=user_id)

    def update_reaction(self, post_id, user_id, reaction_type):
        self.storage.update_post_reaction(
            post_id=post_id, user_id=user_id, reaction_type=reaction_type
        )
