from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage


class GetReactionsToPostInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def get_reaction_to_post(self, post_id: int, offset: int, limit: int):
        is_invalid_post_id = not self.storage.is_post_exists(post_id=post_id)

        if is_invalid_post_id:
            self.presenter.raise_invalid_post_id_exception()
        reaction_list_dto_with_total = self.storage.get_reactions_to_post(
            post_id=post_id, offset=offset, limit=limit)
        return self.presenter.get_reactions_to_post(reaction_list_dto_with_total)
