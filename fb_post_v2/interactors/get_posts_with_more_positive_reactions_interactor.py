from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage


class GetPostsWithMorePositiveReactionsInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def posts_with_more_positive_reactions(self, offset: int, limit: int):
        positive_post_ids_dto_with_total = \
            self.storage.get_posts_with_more_positive_reactions(
                offset=offset, limit=limit
            )
        return self.presenter.get_posts_with_more_positive_reactions_response(
            positive_post_ids_dto_with_total
        )