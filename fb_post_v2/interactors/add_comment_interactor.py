from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import Storage


class AddCommentInteractor:
    def __init__(self, storage: Storage, presenter: Presenter):
        self.storage = storage
        self.presenter = presenter

    def add_comment(self, user_id: int, post_id: int, description: str):
        is_invalid_post_id = not self.storage.is_post_exists(post_id=post_id)

        if is_invalid_post_id:
            self.presenter.raise_invalid_post_id_exception()

        comment_id_dto = self.storage.add_comment_and_get_comment_id(
            user_id=user_id, post_id=post_id, comment_description= description)
        return self.presenter.get_add_comment_response(comment_id_dto)
