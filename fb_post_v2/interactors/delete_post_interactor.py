from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage


class DeletePostInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def delete_post(self, post_id: int):
        is_post_not_exists = not self.storage.is_post_exists(post_id=post_id)

        if is_post_not_exists:
            self.presenter.raise_invalid_post_id_exception()
        self.storage.delete_post(post_id=post_id)
