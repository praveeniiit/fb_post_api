from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage


class GetTotalReactionsCount:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def get_total_reaction_count(self):
        reactions_count = self.storage.get_total_reactions_count()
        return self.presenter.get_total_reactions_count_response(
            reactions_count
        )
