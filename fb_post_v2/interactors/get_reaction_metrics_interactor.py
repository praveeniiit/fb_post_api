from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage


class GetReactionMetricsInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def get_reaction_metrics(self, post_id: int):
        is_invalid_post_id = not self.storage.is_post_exists(post_id=post_id)

        if is_invalid_post_id:
            self.presenter.raise_invalid_post_id_exception()
        reaction_metric_dto_list = self.storage.get_reaction_metrics(
            post_id=post_id
        )
        return self.presenter.get_reaction_metrics_response(
            reaction_metric_dto_list
        )
