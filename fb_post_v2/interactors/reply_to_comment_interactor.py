from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage


class ReplyToCommentInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def reply_to_comment(self, comment_id, user_id, description):
        is_comment_not_exist = not self.storage.is_comment_exists(comment_id)

        if is_comment_not_exist:
            self.presenter.raise_invalid_comment_id_exception()

        is_comment_have_parent = self.storage.is_comment_have_parent(comment_id)
        parent_comment_id_for_reply = comment_id

        if is_comment_have_parent:
            parent_comment_id_for_reply = self.storage.get_comment_parent_id(
                comment_id=comment_id
            )

        comment_id_dto = self.storage.reply_to_comment_and_get_reply_id(
            comment_id=parent_comment_id_for_reply, user_id=user_id,
            description=description
        )
        return self.presenter.get_reply_to_comment_response(comment_id_dto)