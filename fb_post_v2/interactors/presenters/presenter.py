from abc import ABCMeta
from abc import abstractmethod
from fb_post_v2.interactors.storages.storage import PostIdDTO, CommentIdDTO, \
    ReactionMetricDTO, PostCompleteDetailsDTO, ReactionDTO, ReplyDTO, PostDTO
from typing import List


class Presenter:
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_create_post_response(self, post_id_dto: PostIdDTO):
        pass

    @abstractmethod
    def get_add_comment_response(self, comment_id_dto: CommentIdDTO):
        pass

    @abstractmethod
    def get_reply_to_comment_response(self, comment_id_dto: CommentIdDTO):
        pass

    @abstractmethod
    def get_total_reactions_count_response(self, reactions_count: int):
        pass

    @abstractmethod
    def get_reaction_metrics_response(
            self, reaction_metric_dto_list: List[ReactionMetricDTO]):
        pass

    @abstractmethod
    def get_post_response(self, get_post_dto: PostCompleteDetailsDTO):
        pass

    @abstractmethod
    def get_user_posts_response(
            self, get_user_posts_dto: List[PostCompleteDetailsDTO]):
        pass

    @abstractmethod
    def get_posts_with_more_positive_reactions_response(
            self, positive_posts_list: List[PostIdDTO]):
        pass

    @abstractmethod
    def get_reactions_to_post(self, reactions_list: List[ReactionDTO]):
        pass

    @abstractmethod
    def get_replies_for_comment(self, replies_list: List[ReplyDTO]):
        pass

    @abstractmethod
    def get_posts_reacted_by_user(self, post_dto: List[PostIdDTO]):
        pass

    @abstractmethod
    def raise_invalid_user_id_exception(self):
        pass

    @abstractmethod
    def raise_invalid_post_id_exception(self):
        pass

    @abstractmethod
    def raise_invalid_comment_id_exception(self):
        pass

    @abstractmethod
    def raise_invalid_reaction_exception(self):
        pass