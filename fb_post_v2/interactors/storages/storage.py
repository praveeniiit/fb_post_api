from abc import ABCMeta
from abc import abstractmethod
from dataclasses import dataclass
from typing import List


@dataclass
class PostIdDTO:
    post_id: int


@dataclass
class PostIdDTOWithTotal:
    total: int
    post_id_dtos: List[PostIdDTO]


@dataclass
class CommentIdDTO:
    comment_id: int


@dataclass
class ReactionTypeDTO:
    reaction_type: str


@dataclass
class ReactionWithUserDTO:
    reaction_type: str
    user_id: int = None
    comment_id: int = None
    post_id: int = None


@dataclass
class ReactionMetricDTO:
    reaction_type: str
    count: int


@dataclass
class CommentDTO:
    comment_id: int
    comment_content: str
    commented_at: str
    post_id: int = None
    user_id: int = None
    parent_id: int = None


@dataclass
class UserDTO:
    user_id: int
    name: str
    profile_pic_url: str


@dataclass
class PostDTO:
    post_id: int
    posted_at: str
    post_content: str
    user_id: int = None


@dataclass
class ReplyDTO:
    comment: CommentDTO
    commenter: UserDTO


@dataclass
class ReplyDTOWithCount:
    replies_dto: List[ReplyDTO]
    total: int


@dataclass
class ReactionDTO:
    user: UserDTO
    reaction_type: str


@dataclass
class ReactionDTOWithTotal:
    total: int
    reaction_dto_list: List[ReactionDTO]


@dataclass
class PostCompleteDetailsDTO:
    post: List[PostDTO]
    users: List[UserDTO]
    comments: List[CommentDTO]
    reactions: List[ReactionWithUserDTO] = None


@dataclass
class PostCompleteDetailsDTOWithTotal:
    total: int
    user_post_dtos: List[PostCompleteDetailsDTO]


class Storage:
    __metaclass__ = ABCMeta

    @abstractmethod
    def create_post_and_get_post_id(self, user_id, post_content) -> PostIdDTO:
        pass

    @abstractmethod
    def add_comment_and_get_comment_id(
            self, user_id, post_id, comment_description) -> CommentIdDTO:
        pass

    @abstractmethod
    def reply_to_comment_and_get_reply_id(
            self, comment_id, user_id, comment_description) -> CommentIdDTO:
        pass

    @abstractmethod
    def react_to_post_on_correct_input_create_reaction(
            self, user_id: int, post_id: int, reaction_type: str) -> None:
        pass

    @abstractmethod
    def react_to_comment_on_correct_input_create_reaction(
            self, user_id: int, comment_id: int, reaction_type: str) -> None:
        pass

    @abstractmethod
    def get_total_reactions_count(self) -> int:
        pass

    @abstractmethod
    def get_reaction_metrics(self, post_id: int) -> ReactionMetricDTO:
        pass

    @abstractmethod
    def delete_post(self, post_id: int):
        pass

    @abstractmethod
    def get_post(self, post_id: int) -> PostCompleteDetailsDTO:
        pass

    @abstractmethod
    def get_user_posts(self, user_id: int, offset=0, limit=15) -> List[PostCompleteDetailsDTO]:
        pass

    @abstractmethod
    def get_posts_with_more_positive_reactions(self, offset=0, limit=15) -> List[PostIdDTO]:
        pass

    @abstractmethod
    def get_posts_reacted_by_user(self, user_id: int, offset=0, limit=15) -> List[PostDTO]:
        pass

    @abstractmethod
    def get_reactions_to_post(self, post_id: int) -> List[ReactionDTO]:
        pass

    @abstractmethod
    def get_replies_for_comment(self, comment_id: int, offset=0, limit=15) -> List[ReplyDTO]:
        pass

    @abstractmethod
    def is_post_exists(self, post_id) -> bool:
        pass

    @abstractmethod
    def is_user_exists(self, user_id) -> bool:
        pass

    @abstractmethod
    def is_comment_exists(self, comment_id) -> bool:
        pass

    @abstractmethod
    def is_post_reaction_exists(self, post_id: int, user_id: int) -> bool:
        pass

    @abstractmethod
    def is_comment_reaction_exists(self, user_id: int, comment_id: int) -> bool:
        pass

    @abstractmethod
    def get_post_reaction(self, post_id: int, user_id: int) -> ReactionTypeDTO:
        pass

    @abstractmethod
    def get_comment_reaction(
            self, user_id: int, comment_id: int) -> ReactionTypeDTO:
        pass

    @abstractmethod
    def delete_post_reaction(self, post_id: int, user_id: int):
        pass

    @abstractmethod
    def delete_comment_reaction(self, comment_id: int, user_id: int, reaction):
        pass

    @abstractmethod
    def update_post_reaction(
            self, post_id: int, user_id: int, reaction_type: str):
        pass

    @abstractmethod
    def update_comment_reaction(
            self, user_id: int, comment_id: int, reaction_type: str):
        pass

    @abstractmethod
    def is_comment_have_parent(self, comment_id) -> bool:
        pass

    @abstractmethod
    def get_comment_parent_id(self, comment_id) -> CommentIdDTO:
        pass
