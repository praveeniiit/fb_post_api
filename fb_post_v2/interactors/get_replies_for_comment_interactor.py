from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage


class GetRepliesForCommentInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def get_replies_for_comment(self, comment_id: int, offset: int, limit: int):
        is_comment_not_exist = not self.storage.is_comment_exists(comment_id)

        if is_comment_not_exist:
            self.presenter.raise_invalid_comment_id_exception()
        replies_dto_list = self.storage.get_replies_for_comment(
            comment_id=comment_id, offset=offset, limit=limit)

        return self.presenter.get_replies_for_comment(replies_dto_list)
