from fb_post_v2.interactors.presenters.presenter import Presenter
from fb_post_v2.interactors.storages.storage import Storage
from fb_post_v2.presenters.json_presenter import DataPresenter
from fb_post_v2.storages.sql_storage import SQLStorage


class CreatePostInteractor:
    def __init__(self, storage: SQLStorage, presenter: DataPresenter):
        self.storage = storage
        self.presenter = presenter

    def create_post(self, user_id: int, post_content: str):
        post_id_dto = self.storage.create_post_and_get_post_id(
            user_id=user_id, post_content=post_content)
        return self.presenter.get_create_post_response(post_id_dto)
