# pylint: disable=wrong-import-position

APP_NAME = "fb_post"
OPERATION_NAME = "get_reaction_metrics"
REQUEST_METHOD = "get"
URL_SUFFIX = "post/{id}/react/metrics/v1/"

from .test_case_01 import TestCase01GetReactionMetricsAPITestCase
from .test_case_02 import TestCase02GetReactionMetricsAPITestCase
from .test_case_03 import TestCase03GetReactionMetricsAPITestCase
from .test_case_04 import TestCase04GetReactionMetricsAPITestCase

__all__ = [
    "TestCase01GetReactionMetricsAPITestCase",
    "TestCase02GetReactionMetricsAPITestCase",
    "TestCase03GetReactionMetricsAPITestCase",
    "TestCase04GetReactionMetricsAPITestCase"
]
