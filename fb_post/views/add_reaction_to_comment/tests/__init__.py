# pylint: disable=wrong-import-position

APP_NAME = "fb_post"
OPERATION_NAME = "add_reaction_to_comment"
REQUEST_METHOD = "post"
URL_SUFFIX = "comment/{id}/react/v1/"

from .test_case_01 import TestCase01AddReactionToCommentAPITestCase
from .test_case_02 import TestCase02AddReactionToCommentAPITestCase
from .test_case_03 import TestCase03AddReactionToCommentAPITestCase
from .test_case_04 import TestCase04AddReactionToCommentAPITestCase
from .test_case_05 import TestCase05AddReactionToCommentAPITestCase
from .test_case_06 import TestCase06AddReactionToCommentAPITestCase
from .test_case_07 import TestCase07AddReactionToCommentAPITestCase
from .test_case_08 import TestCase08AddReactionToCommentAPITestCase
from .test_case_09 import TestCase09AddReactionToCommentAPITestCase
from .test_case_10 import TestCase10AddReactionToCommentAPITestCase

__all__ = [
    "TestCase01AddReactionToCommentAPITestCase",
    "TestCase02AddReactionToCommentAPITestCase",
    "TestCase03AddReactionToCommentAPITestCase",
    "TestCase04AddReactionToCommentAPITestCase",
    "TestCase05AddReactionToCommentAPITestCase",
    "TestCase06AddReactionToCommentAPITestCase",
    "TestCase07AddReactionToCommentAPITestCase",
    "TestCase08AddReactionToCommentAPITestCase",
    "TestCase09AddReactionToCommentAPITestCase",
    "TestCase10AddReactionToCommentAPITestCase"
]
