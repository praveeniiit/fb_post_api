from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post.utils.methods import get_posts_with_more_positive_reactions
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    query_parameters = kwargs['request_query_params']
    offset = query_parameters.offset
    limit = query_parameters.limit

    positive_reaction_posts = get_posts_with_more_positive_reactions(offset, limit)

    import json
    from django.http import HttpResponse

    response_data = json.dumps(positive_reaction_posts)
    return HttpResponse(response_data, status=200)