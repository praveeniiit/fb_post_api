# pylint: disable=wrong-import-position

APP_NAME = "fb_post"
OPERATION_NAME = "get_posts_with_more_positive_reactions"
REQUEST_METHOD = "get"
URL_SUFFIX = "post/positive/reactions/v1/"

from .test_case_01 import TestCase01GetPostsWithMorePositiveReactionsAPITestCase
from .test_case_02 import TestCase02GetPostsWithMorePositiveReactionsAPITestCase
from .test_case_03 import TestCase03GetPostsWithMorePositiveReactionsAPITestCase
from .test_case_04 import TestCase04GetPostsWithMorePositiveReactionsAPITestCase
from .test_case_05 import TestCase05GetPostsWithMorePositiveReactionsAPITestCase

__all__ = [
    "TestCase01GetPostsWithMorePositiveReactionsAPITestCase",
    "TestCase02GetPostsWithMorePositiveReactionsAPITestCase",
    "TestCase03GetPostsWithMorePositiveReactionsAPITestCase",
    "TestCase04GetPostsWithMorePositiveReactionsAPITestCase",
    "TestCase05GetPostsWithMorePositiveReactionsAPITestCase"
]
