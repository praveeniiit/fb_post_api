"""
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking the test with valid input
                * Success
                    * Check Response Code ✔
"""
import json

from fb_post.constants.exception_messsages import OK
from fb_post.utils.test_utils.custom_test_util import TestFixtures
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """

"""

TEST_CASE = {
    "request": {
        "path_params": {},
        "query_params": {},
        "header_params": {},
        "securities": {
            "oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password",
                      "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase01GetPostsWithMorePositiveReactionsAPITestCase(TestFixtures):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase01GetPostsWithMorePositiveReactionsAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        self.create_post()
        self.create_post_reaction()

    def test_case(self):
        response = self.default_test_case()
        response_code = response.status_code
        api_response_posts = json.loads(response.content)

        positive_reaction_posts = self.POSTS_WITH_MORE_POSITIVE_REACTIONS

        self.assertEqual(response_code, OK)
        self.assertEqual(len(api_response_posts), len(positive_reaction_posts))
        self.assertListEqual(api_response_posts, positive_reaction_posts)
