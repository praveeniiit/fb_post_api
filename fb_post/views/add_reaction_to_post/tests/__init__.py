# pylint: disable=wrong-import-position

APP_NAME = "fb_post"
OPERATION_NAME = "add_reaction_to_post"
REQUEST_METHOD = "post"
URL_SUFFIX = "post/{id}/react/v1/"

from .test_case_01 import TestCase01AddReactionToPostAPITestCase
from .test_case_02 import TestCase02AddReactionToPostAPITestCase
from .test_case_03 import TestCase03AddReactionToPostAPITestCase
from .test_case_04 import TestCase04AddReactionToPostAPITestCase
from .test_case_05 import TestCase05AddReactionToPostAPITestCase
from .test_case_06 import TestCase06AddReactionToPostAPITestCase
from .test_case_07 import TestCase07AddReactionToPostAPITestCase
from .test_case_08 import TestCase08AddReactionToPostAPITestCase
from .test_case_09 import TestCase09AddReactionToPostAPITestCase
from .test_case_10 import TestCase10AddReactionToPostAPITestCase

__all__ = [
    "TestCase01AddReactionToPostAPITestCase",
    "TestCase02AddReactionToPostAPITestCase",
    "TestCase03AddReactionToPostAPITestCase",
    "TestCase04AddReactionToPostAPITestCase",
    "TestCase05AddReactionToPostAPITestCase",
    "TestCase06AddReactionToPostAPITestCase",
    "TestCase07AddReactionToPostAPITestCase",
    "TestCase08AddReactionToPostAPITestCase",
    "TestCase09AddReactionToPostAPITestCase",
    "TestCase10AddReactionToPostAPITestCase"
]
