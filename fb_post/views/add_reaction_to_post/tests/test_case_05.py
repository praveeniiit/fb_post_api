"""
# TODO: Complete all three cases for this one
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking the test with valid input and
                reaction is updating or not.

"""

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import ACCEPTED
from fb_post.models import Post, Reaction
from fb_post.utils.test_utils.custom_test_util import TestFixtures
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """
{
    "reaction_type": "LOVE"
}
"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1},
        "query_params": {},
        "header_params": {},
        "securities": {
            "oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password",
                      "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase05AddReactionToPostAPITestCase(TestFixtures):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase05AddReactionToPostAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        post = Post.objects.create(user_id=self.foo_user.id, description="Nothing to post here.")
        Reaction.objects.create(user_id=self.foo_user.id,
                                post_id=post.id, reaction_type="LOVE")

    def test_case(self):
        response = self.default_test_case()
        response_code = response.status_code

        with self.assertRaises(Reaction.DoesNotExist) as ReactionNotExist:
            Reaction.objects.get(id=1)

        self.assertEqual(response_code, ACCEPTED)