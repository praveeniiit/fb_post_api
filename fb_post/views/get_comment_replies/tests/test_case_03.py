"""
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking the test with valid input
                * Success
                    * Check Response Code
                    * Check Comment id
                    * Check Replies Count
                    * Check Each Reply Id
"""
import json
from freezegun import freeze_time

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import OK
from fb_post.models import Post, Comment
from fb_post.utils.test_utils.custom_test_util import TestFixtures
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """

"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1},
        "query_params": {
            "offset": 0,
            "limit": 3
        },
        "header_params": {},
        "securities": {
            "oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password",
                      "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase03GetCommentRepliesAPITestCase(TestFixtures):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE
    freeze = freeze_time("2012-01-14 12:00:01")

    def setupUser(self, username, password):
        super(TestCase03GetCommentRepliesAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        self.freeze.start()
        Post.objects.create(user_id=self.foo_user.id, description="Nothing to post here.")
        comment = Comment.objects.create(user_id=self.foo_user.id,
                                         post_id=1, description="Comment to post.")
        for i in range(5):
            Comment.objects.create(user_id=self.foo_user.id, parent=comment,
                                   description="Reply to the Comment1 no%d" % i)

    def test_case(self):
        self.default_test_case()
