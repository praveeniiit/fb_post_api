from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post.utils.methods import get_total_reaction_count
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    reaction_count = get_total_reaction_count()

    import json
    from django.http.response import HttpResponse
    response_data = json.dumps(
        {
            "reactions_count": reaction_count
        }
    )
    return HttpResponse(response_data, status=200)