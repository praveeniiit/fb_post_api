# pylint: disable=wrong-import-position

APP_NAME = "fb_post"
OPERATION_NAME = "get_total_reactions_count"
REQUEST_METHOD = "get"
URL_SUFFIX = "reaction/count/v1/"

from .test_case_01 import TestCase01GetTotalReactionsCountAPITestCase
from .test_case_02 import TestCase02GetTotalReactionsCountAPITestCase

__all__ = [
    "TestCase01GetTotalReactionsCountAPITestCase",
    "TestCase02GetTotalReactionsCountAPITestCase"
]
