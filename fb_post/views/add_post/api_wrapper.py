"""post
400 - BadRequest
404 - NotFound
403 - Forbidden
"""

from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator
from .validator_class import ValidatorClass
from fb_post.utils.methods import create_post


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    user_id = kwargs["user"].id
    post_content = kwargs["request_data"]["post_content"]

    post = create_post(user_id, post_content)
    import json
    from django.http.response import HttpResponse
    response_data = json.dumps({
        "post_id": post
    })
    return HttpResponse(response_data, status=202)

