"""
# TODO: Update test case description
# DESCRIPTION: In this test case, checking the post existence if post is not there
                raise 404 error with response POST_NOT_EXIST
"""
import json

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import INVALID_POST_ID, NOT_FOUND
from fb_post.models import Post
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """
{
    "comment_description": "string"
}
"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1231},
        "query_params": {},
        "header_params": {},
        "securities": {"oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password", "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase04AddCommentAPITestCase(CustomAPITestCase):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase04AddCommentAPITestCase, self).setupUser(
            username=username,
            password=password
        )

    def test_case(self):
        self.default_test_case()
