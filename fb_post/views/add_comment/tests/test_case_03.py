"""
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking whether new comment
                added or not and success response for the valid input.
                *Success
                    * Check Response code
                    * Check Description of the comment
"""

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import ACCEPTED
from fb_post.models import Post, Comment
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """
{
    "comment_description": "Comment for post"
}
"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1},
        "query_params": {},
        "header_params": {},
        "securities": {"oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password", "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase03AddCommentAPITestCase(CustomAPITestCase):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase03AddCommentAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        Post.objects.create(user_id=self.foo_user.id,
                            description="My First post from the admin.")

    def test_case(self):
        import json
        response = self.default_test_case()

        # response
        # status_code
        # created comment description
        # created comment user_id
        # created comment post_id

        comment_id_from_response = json.loads(response.content)['comment_id']
        comment = Comment.objects.get(id=comment_id_from_response)

        self.assert_match_snapshot(
            value=comment.description,
            name='comment_description'
        )

        self.assert_match_snapshot(
            value=comment.user_id,
            name='comment_user_id'
        )

        self.assert_match_snapshot(
            value=comment.post_id,
            name='post_id'
        )
