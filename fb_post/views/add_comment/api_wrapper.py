from django_swagger_utils.drf_server.exceptions import BadRequest, NotFound
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post.constants.exception_messsages import INVALID_POST_ID_RESPONSE, \
    INVALID_POST_ID
from fb_post.utils.methods import add_comment, InvalidPostId
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    user_id = kwargs['user'].id
    post_id = kwargs['id']
    comment_description = kwargs['request_data']['comment_description']

    import json
    from django.http.response import HttpResponse
    try:
        comment = add_comment(post_id, user_id, comment_description)
    except InvalidPostId:
        raise NotFound(*INVALID_POST_ID)

    response_data = json.dumps({
        "comment_id": comment
    })
    return HttpResponse(response_data, status=202)