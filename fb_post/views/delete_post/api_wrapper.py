from django_swagger_utils.drf_server.exceptions import BadRequest
from django_swagger_utils.drf_server.utils.decorator.interface_decorator \
    import validate_decorator

from fb_post.constants.exception_messsages import INVALID_POST_ID, \
    INVALID_POST_ID_RESPONSE
from fb_post.utils.methods import validate_post_id, delete_post, InvalidPostId
from .validator_class import ValidatorClass


@validate_decorator(validator_class=ValidatorClass)
def api_wrapper(*args, **kwargs):
    post_id = kwargs['id']



    import json
    from django.http.response import HttpResponse
    try:
        delete = delete_post(post_id)
    except InvalidPostId:
        return HttpResponse(INVALID_POST_ID_RESPONSE, status=404)

    response_data = json.dumps(
        {
            "post_id": post_id
        }
    )
    return HttpResponse(response_data, status=202)