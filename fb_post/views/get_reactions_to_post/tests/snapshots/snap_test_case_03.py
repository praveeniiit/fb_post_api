# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['TestCase03GetReactionsToPostAPITestCase::test_case status'] = 200

snapshots['TestCase03GetReactionsToPostAPITestCase::test_case body'] = {
    'result': [
        {
            'name': '',
            'profile_pic_url': '',
            'reaction': 'LIKE',
            'user_id': 1
        },
        {
            'name': 'Praveen',
            'profile_pic_url': 'None',
            'reaction': 'LIKE',
            'user_id': 2
        }
    ],
    'total': 6
}

snapshots['TestCase03GetReactionsToPostAPITestCase::test_case header_params'] = {
    'content-language': [
        'Content-Language',
        'en'
    ],
    'content-length': [
        '177',
        'Content-Length'
    ],
    'content-type': [
        'Content-Type',
        'text/html; charset=utf-8'
    ],
    'vary': [
        'Accept-Language, Origin, Cookie',
        'Vary'
    ],
    'x-frame-options': [
        'SAMEORIGIN',
        'X-Frame-Options'
    ]
}
