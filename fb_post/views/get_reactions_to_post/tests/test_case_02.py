"""
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking the test with valid input
                * Failure ✔
                    * On Invalid Post Id ✔
                        * Check Response Code ✔
                        * Check Response Text ✔
"""
import json

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import get_random_reaction, NOT_FOUND, \
    INVALID_POST_ID
from fb_post.models import Post, Reaction, Comment
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """

"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1123},
        "query_params": {},
        "header_params": {},
        "securities": {"oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password", "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase02GetReactionsToPostAPITestCase(CustomAPITestCase):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase02GetReactionsToPostAPITestCase, self).setupUser(
            username=username,
            password=password
        )

        for inc in range(5):
            post = Post.objects.create(user_id=self.foo_user.id,
                                       description="Sample post from user 1")
            Reaction.objects.create(
                user_id=self.foo_user.id, post_id=post.id,
                reaction_type=get_random_reaction()
            )

    def test_case(self):
        response = self.default_test_case()
        response_code = response.status_code
        response_text = json.loads(response.content)

        self.assertEqual(response_code, NOT_FOUND)
        self.assertEqual(response_text['response'], INVALID_POST_ID[0])
        self.assertEqual(response_text['res_status'], INVALID_POST_ID[1])
