"""
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking the test with valid input
                * Success ✔
                    * On Valid Post Id ✔
                        * Check Response Code ✔
                        * Check Reacted User Id ✔
                        * Check Reaction Count ✔
"""
import json

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import get_random_reaction, OK
from fb_post.models import Post, Reaction, Comment
from fb_post.utils.test_utils.custom_test_util import TestFixtures
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """

"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1},
        "query_params": {},
        "header_params": {},
        "securities": {"oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password", "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase01GetReactionsToPostAPITestCase(TestFixtures):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase01GetReactionsToPostAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        self.create_post()
        self.create_post_reaction()

    def test_case(self):
        response = self.default_test_case()
        response_code = response.status_code

        api_reactions = json.loads(response.content)
        db_reaction_with_user = self.POST_REACTIONS_WITH_USER

        self.assertEqual(response_code, OK)
        self.assertEqual(len(api_reactions), len(db_reaction_with_user))

        for (reaction, db_reaction) in zip(api_reactions, db_reaction_with_user):
            self.assertEqual(reaction['user_id'], db_reaction['user_id'])
            self.assertEqual(reaction['name'], db_reaction['name'])
            self.assertEqual(reaction['reaction'], db_reaction['reaction'])
