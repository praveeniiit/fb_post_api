"""
# TODO: Update test case description
# DESCRIPTION: In this test case we are checking the test with valid input
        * Success ✔
            * Check Response Code ✔
            * Check Post Count ✔

"""
import json

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import get_random_reaction, OK
from fb_post.models import Post, Reaction, Comment
from fb_post.utils.test_utils.custom_test_util import TestFixtures
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """

"""

TEST_CASE = {
    "request": {
        "path_params": {},
        "query_params": {
            "offset": 0,
            "limit": 3
        },
        "header_params": {},
        "securities": {"oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password", "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase01GetPostsReactedByUserAPITestCase(TestFixtures):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase01GetPostsReactedByUserAPITestCase, self).setupUser(
            username=username,
            password=password
        )
        self.create_post()
        self.create_post_reaction()

    def test_case(self):
        response = self.default_test_case()
        response_code = response.status_code
        api_response_posts = json.loads(response.content)

        user_reacted_posts = self.POSTS_REACTED_BY_USER

        self.assertEqual(response_code, OK)
        self.assertEqual(len(api_response_posts), len(user_reacted_posts))
        self.assertListEqual(api_response_posts, user_reacted_posts)