# pylint: disable=wrong-import-position

APP_NAME = "fb_post"
OPERATION_NAME = "get_posts_reacted_by_user"
REQUEST_METHOD = "get"
URL_SUFFIX = "user/reacted/posts/v1/"

from .test_case_01 import TestCase01GetPostsReactedByUserAPITestCase
from .test_case_02 import TestCase02GetPostsReactedByUserAPITestCase
from .test_case_03 import TestCase03GetPostsReactedByUserAPITestCase
from .test_case_04 import TestCase04GetPostsReactedByUserAPITestCase
from .test_case_05 import TestCase05GetPostsReactedByUserAPITestCase

__all__ = [
    "TestCase01GetPostsReactedByUserAPITestCase",
    "TestCase02GetPostsReactedByUserAPITestCase",
    "TestCase03GetPostsReactedByUserAPITestCase",
    "TestCase04GetPostsReactedByUserAPITestCase",
    "TestCase05GetPostsReactedByUserAPITestCase"
]
