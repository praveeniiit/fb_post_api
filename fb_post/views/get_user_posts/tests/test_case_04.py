"""
# TODO: Update test case description
# DESCRIPTION: On giving valid input , it returns dictionary
"""
import json

from django_swagger_utils.utils.test import CustomAPITestCase

from fb_post.constants.exception_messsages import get_random_reaction, NOT_FOUND, \
    INVALID_USER_ID
from fb_post.models import Post, Comment, Reaction
from . import APP_NAME, OPERATION_NAME, REQUEST_METHOD, URL_SUFFIX

REQUEST_BODY = """

"""

TEST_CASE = {
    "request": {
        "path_params": {"id": 1234},
        "query_params": {
            "offset": 0,
            "limit": 1
        },
        "header_params": {},
        "securities": {"oauth": {"tokenUrl": "http://auth.ibtspl.com/oauth2/", "flow": "password", "scopes": ["superuser"], "type": "oauth2"}},
        "body": REQUEST_BODY,
    },
}


class TestCase04GetUserPostsAPITestCase(CustomAPITestCase):
    app_name = APP_NAME
    operation_name = OPERATION_NAME
    request_method = REQUEST_METHOD
    url_suffix = URL_SUFFIX
    test_case_dict = TEST_CASE

    def setupUser(self, username, password):
        super(TestCase04GetUserPostsAPITestCase, self).setupUser(
            username=username,
            password=password
        )

        for inc in range(5):
            post = Post.objects.create(user_id=self.foo_user.id,
                                       description="Sample post from user 1")
            Reaction.objects.create(
                user_id=self.foo_user.id, post_id=post.id,
                reaction_type=get_random_reaction()
            )
            for comment_inc in range(3):
                comment = Comment.objects.create(
                    post_id=post.id, user_id=self.foo_user.id,
                    description="Sample comment for the post%d"%inc)
                Reaction.objects.create(
                    user_id=self.foo_user.id, comment_id=comment.id,
                    reaction_type=get_random_reaction())
                for reply_inc in range(2):
                    reply = Comment.objects.create(
                        parent_id=comment.id, user_id=self.foo_user.id,
                        description="Sample comment for the post%d" % inc)
                    Reaction.objects.create(
                        user_id=self.foo_user.id, comment_id=reply.id,
                        reaction_type=get_random_reaction())

    def test_case(self):
        self.default_test_case()
