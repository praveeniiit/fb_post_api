from django.db.models import Q, F, Count, Prefetch
from fb_post.constants.reactions import ReactionType, REACTION_LIST
from fb_post.models.user import User
from fb_post.models.post import Post
from fb_post.models.comment import Comment
from fb_post.models.reaction import Reaction


class InvalidUserId(Exception):
    pass


class InvalidPostId(Exception):
    pass


class InvalidReaction(Exception):
    pass


class InvalidCommentId(Exception):
    pass


class PostReactionNotFound(Exception):
    pass


class CommentReactionNotFound(Exception):
    pass


def validate_user_id(user_id):
    try:
        user = User.objects.get(pk=user_id)
        return user
    except User.DoesNotExist:
        raise InvalidUserId


def validate_post_id(post_id):
    try:
        post = Post.objects.get(pk=post_id)
        return post
    except Post.DoesNotExist:
        raise InvalidPostId


def validate_reaction_type(reaction_type):
    reaction_is_not_valid = reaction_type not in REACTION_LIST

    if reaction_is_not_valid:
        raise InvalidReaction


def validate_comment_id(comment_id):
    try:
        comment = Comment.objects.get(pk=comment_id)
        return comment
    except Comment.DoesNotExist:
        raise InvalidCommentId


def get_post_reaction(user_id, post_id):
    try:
        reaction = Reaction.objects.get(user_id=user_id, post_id=post_id)

        return reaction
    except Reaction.DoesNotExist:
        raise PostReactionNotFound


def get_comment_reaction(user_id, comment_id):
    try:
        reaction = Reaction.objects.get(user_id=user_id, comment_id=comment_id)
        return reaction
    except Reaction.DoesNotExist:
        raise CommentReactionNotFound


def get_user_in_dictionary_form(user):
    return {
        "name": user.name,
        "user_id": user.id,
        "profile_pic_url": user.profile_pic_url
    }


def get_reaction_in_dictionary_form(reaction):
    reactions_types = list({r.reaction_type for r in reaction})
    reactions_types.sort()
    if reactions_types is None:
        reactions_types = []
    return {
        "count": len(reactions_types),
        "type": reactions_types
    }


def get_replies_in_dictionary_form(replies):
    return [
        {
            "comment_id": reply.id,
            "commenter": get_user_in_dictionary_form(reply.user),
            "commented_at": str(reply.pub_date),
            "comment_content": reply.description,
            "reactions": get_reaction_in_dictionary_form(reply.reply_reactions)
        } for reply in replies
    ]


def get_post(post_id):
    validate_post_id(post_id)
    posts = Post.objects.filter(id=post_id).select_related(
        'user').prefetch_related(
        Prefetch("reactions", to_attr="post_reactions"),
        Prefetch("comments", queryset=Comment.objects.select_related('user'),
                 to_attr="post_comments"),
        Prefetch("post_comments__reactions", to_attr="comment_reactions"),
        Prefetch("post_comments__replies",
                 queryset=Comment.objects.select_related(
                     "user").prefetch_related(
                     Prefetch('reactions', to_attr="reply_reactions")),
                 to_attr="comment_replies"),
    )
    return convert_post_object_to_dictionary(posts[0])


def create_post(user_id, post_content):
    validate_user_id(user_id)
    post = Post.objects.create(description=post_content, user_id=user_id)
    return post.id


def add_comment(post_id, comment_user_id, comment_text):
    validate_user_id(comment_user_id)
    validate_post_id(post_id)
    comment = Comment.objects.create(description=comment_text,
                                     post_id=post_id, user_id=comment_user_id)
    return comment.id


def reply_to_comment(comment_id, reply_user_id, reply_text):
    validate_user_id(reply_user_id)
    comment = validate_comment_id(comment_id)

    if comment.parent is not None:
        comment_id = comment.parent.id
    reply = Comment.objects.create(description=reply_text,
                                   parent_id=comment_id, user_id=reply_user_id)
    return reply.id


def undo_or_update_reaction(reaction, reaction_type):
    undo_action = reaction.reaction_type == reaction_type

    if undo_action:
        reaction.delete()
    else:
        reaction.reaction_type = reaction_type
        reaction.save()
        return reaction


def react_to_post(user_id, post_id, reaction_type):
    validate_user_id(user_id)
    validate_post_id(post_id)
    validate_reaction_type(reaction_type)

    try:
        reaction = get_post_reaction(user_id, post_id)
        return undo_or_update_reaction(reaction, reaction_type)
    except PostReactionNotFound:
        reaction = Reaction.objects.create(reaction_type=reaction_type,
                                           post_id=post_id, user_id=user_id)
        return reaction


def react_to_comment(user_id, comment_id, reaction_type):
    validate_user_id(user_id)
    validate_comment_id(comment_id)
    validate_reaction_type(reaction_type)

    try:
        reaction = get_comment_reaction(user_id, comment_id)
        return undo_or_update_reaction(reaction, reaction_type)
    except CommentReactionNotFound:
        reaction = Reaction.objects.create(reaction_type=reaction_type,
                                           comment_id=comment_id,
                                           user_id=user_id)
        return reaction


def get_comment_with_commenter(comment):
    return {
        "comment_id": comment.id,
        "commenter": get_user_in_dictionary_form(comment.user),
        "commented_at": str(comment.pub_date),
        "comment_content": comment.description
    }


def convert_post_object_to_dictionary(post):
    comment_data = []

    final_data = {
        "post_id": post.id,
        "posted_by": get_user_in_dictionary_form(post.user),
        "posted_at": str(post.pub_date),
        "post_content": post.description,
        "reactions": get_reaction_in_dictionary_form(post.post_reactions)
    }
    for comment in post.post_comments:
        data = get_comment_with_commenter(comment)
        data.update({
            "reactions": get_reaction_in_dictionary_form(
                comment.comment_reactions),
            "replies_count": len(comment.comment_replies),
            "replies": get_replies_in_dictionary_form(comment.comment_replies)
        })
        comment_data.append(data)
    final_data.update(
        {
            "comments": comment_data,
            "comments_count": len(comment_data)
        }
    )
    return final_data


def get_user_posts(user_id, offset=0, limit=15):
    validate_user_id(user_id)
    posts = Post.objects.filter(user_id=user_id).select_related(
        'user').prefetch_related(
        Prefetch("reactions", to_attr="post_reactions"),
        Prefetch("comments", queryset=Comment.objects.select_related('user'),
                 to_attr="post_comments"),
        Prefetch("post_comments__reactions", to_attr="comment_reactions"),
        Prefetch("post_comments__replies",
                 queryset=Comment.objects.select_related(
                     "user").prefetch_related(
                     Prefetch('reactions', to_attr="reply_reactions")),
                 to_attr="comment_replies"),
    )[offset:offset + limit]
    posts_count = Post.objects.filter(user_id=user_id).select_related(
        'user').count()
    return {
        "total": posts_count,
        "result": [convert_post_object_to_dictionary(post) for post in posts]
    }


def get_posts_with_more_positive_reactions(offset=0, limit=15):
    positive_reaction_types = [ReactionType.LIKE.value, ReactionType.LOVE.value,
                               ReactionType.HAHA.value, ReactionType.WOW.value]
    negative_reaction_types = [ReactionType.SAD.value, ReactionType.ANGRY.value]
    positive_exp = Count('reactions',
                         filter=Q(
                             reactions__reaction_type__in=positive_reaction_types))
    negative_exp = Count('reactions',
                         filter=Q(
                             reactions__reaction_type__in=negative_reaction_types))
    posts = Post.objects.annotate(
        positive_count=positive_exp, negative_count=negative_exp).filter(
        positive_count__gt=F('negative_count')).values(
        'id').distinct()[offset:offset + limit]
    posts_count = Post.objects.annotate(
        positive_count=positive_exp, negative_count=negative_exp).filter(
        positive_count__gt=F('negative_count')).values(
        'id').distinct().count()

    return {
        "total": posts_count,
        "result": [post['id'] for post in posts]
    }


def get_posts_reacted_by_user(user_id, offset=0, limit=15):
    validate_user_id(user_id)
    user_posts = Post.objects.filter(reactions__user_id=user_id).values(
        'description').values('id').distinct()[offset:offset + limit]
    user_posts_count = Post.objects.filter(reactions__user_id=user_id).values(
        'description').values('id').distinct().count()
    return {
        "total": user_posts_count,
        "result": [post['id'] for post in user_posts]
    }


def get_reactions_to_post(post_id, offset=0, limit=15):
    validate_post_id(post_id)
    reactions = Reaction.objects.filter(post_id=post_id).select_related(
        'user')[offset:offset + limit]
    react_count = Reaction.objects.filter(post_id=post_id).count()
    user_reactions = []
    for reaction in reactions:
        reacted_user = get_user_in_dictionary_form(reaction.user)
        reacted_user.update({"reaction": reaction.reaction_type})
        user_reactions.append(reacted_user)
    return {
        "total": react_count,
        "result": user_reactions
    }


def get_reaction_metrics(post_id):
    validate_post_id(post_id)
    data = Reaction.objects.filter(post_id=post_id).values(
        "reaction_type").annotate(count=Count('reaction_type'))
    return list(data)


def get_total_reaction_count():
    return Reaction.objects.count()


def get_replies_for_comment(comment_id, offset=0, limit=15):
    comment = validate_comment_id(comment_id)
    replies = comment.replies.select_related('user')[offset:offset + limit]
    replies_count = comment.replies.select_related('user').count()

    return {
        "total": replies_count,
        "result": [get_comment_with_commenter(reply) for reply in replies]
    }


def delete_post(post_id):
    post = validate_post_id(post_id)
    return post.delete()
