from django_swagger_utils.drf_server.exceptions import BadRequest

from fb_post.constants.exception_messsages import OFFSET_LIMIT_NEGATIVE_NUMBER, \
    HIGH_LIMIT_ERROR


class ValidateQuery:

    def validate_limit(self, limit):
        is_limit_negative: int = limit < 0
        is_limit_greaterthan_100: int = limit > 100

        if is_limit_negative:
            raise BadRequest(*OFFSET_LIMIT_NEGATIVE_NUMBER)

        if is_limit_greaterthan_100:
            raise BadRequest(*HIGH_LIMIT_ERROR)

    def validate_offset(self, offset):
        is_offset_negative: int = offset < 0

        if is_offset_negative:
            raise BadRequest(*OFFSET_LIMIT_NEGATIVE_NUMBER)
