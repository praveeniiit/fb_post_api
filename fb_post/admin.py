from .models.user import User
from .models.post import Post
from .models.comment import Comment
from .models.reaction import Reaction
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin


admin.site.register(User)
admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Reaction)