from typing import ClassVar, Dict, List


class Foo:
    x: int = 1
    y: ClassVar[str] = 'Class variable'

    def __init__(self) -> None:
        self.i: List[int] = [0]

    def foo(self, a: int, b: str) -> Dict[int, str]:
        return {a: b}


foo = Foo()
foo.x = 123

print(foo.x)
print(foo.i)
print(Foo.y)
print(foo.foo(1, "abc"))